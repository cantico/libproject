<?php
// -------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
// -------------------------------------------------------------------------
/**
 *
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
namespace Ovidentia\LibProject;

/**
 *
 * Fields are named accordiing to the MSPDI format
 *
 * @property \ORM_PkField $uuid
 *
 * @property \ORM_StringField $Name
 * @property \ORM_StringField $Title
 * @property \ORM_StringField $Subject
 * @property \ORM_StringField $Category
 * @property \ORM_StringField $Company
 * @property \ORM_StringField $Manager
 * @property \ORM_StringField $Author
 * @property \ORM_StringField $CreationDate
 * @property \ORM_IntField $Revision
 * @property \ORM_DateTime $LastSaved
 * @property \ORM_BoolField $ScheduleFromStart
 * @property \ORM_DateTimeField $StartDate
 * @property \ORM_DateTimeField $FinishDate
 * @property \ORM_TimeField $DefaultStartTime
 * @property \ORM_TimeField $DefaultFinishTime
 * @method \Ovidentia\LibProject\Project		newRecord
 * @method \Ovidentia\LibProject\Project		get
 */
class ProjectSet extends RecordSet
{

    /**
     * Field initialization
     */
    public function __construct()
    {
        parent::__construct();
        
        $this->addFields(
            ORM_PkStringField('uuid'), // for ovidentia API
            ORM_StringField('Name'),
            ORM_StringField('Title')->index(), // displayed title of project, gantproject use the title
            ORM_TextField('Subject'),
            ORM_StringField('Category'),
            ORM_StringField('Company'),
            ORM_StringField('Manager'),
            ORM_StringField('Author'),
            ORM_DateTimeField('CreationDate'),
            ORM_IntField('Revision'),
            ORM_DateTimeField('LastSaved'),
            ORM_BoolField('ScheduleFromStart')
                ->setDescription('Whether the project is scheduled from the start date or finish date.'),
            ORM_DateTimeField('StartDate')
                ->setDescription('The start date of the project. Required if ScheduleFromStart is true.'),
            ORM_DateTimeField('FinishDate')
                ->setDescription('The finish date of the project. Required if ScheduleFromStart is false.'),
            ORM_TimeField('DefaultStartTime')
                ->setDescription('The default start time of new tasks.'),
            ORM_TimeField('DefaultFinishTime')
                ->setDescription('The default finish time of new tasks.')
        );
        
        $this->setPrimaryKey('uuid');
    }

    /**
     * (non-PHPdoc)
     * @see Ovidentia\LibProject\RecordSet::save()
     */
    public function save(Project $record)
    {
        if (empty($record->Title)) {
            throw new MandatoryFieldException('Title is mandatory');
        }
        
        if (empty($record->uuid)) {
            $record->uuid = uuid();
            $record->CreationDate = date('Y-m-d H:i:s');
        }
        
        $record->LastSaved = date('Y-m-d H:i:s');
        
        if (! isset($record->Revision)) {
            $record->Revision = 0;
        }
        
        $record->Revision ++;
        
        return parent::save($record);
    }

    /**
     * (non-PHPdoc)
     * @see ORM_RecordSet::delete()
     */
    public function delete(\ORM_Criteria $oCriteria = null)
    {
        $res = $this->select($oCriteria);
        foreach ($res as $project) {
            /*@var $project Project */
            
            $project->deleteAll();
        }
        
        return parent::delete($oCriteria);
    }

    /**
     * Select project with assignements to a user
     * 
     * @param int $user            
     *
     * @return ORM_Iterator
     */
    public function selectAssignedTo($user)
    {
        $storage = Storage();
        $AssignmentSet = $storage->AssignmentSet();
        $storage->includeResourceSet();
        $AssignmentSet->ResourceUID();
        
        $criteria = $AssignmentSet->ResourceUID->user->is($user);
        $res = $this->select($this->uuid->in($criteria, 'project'));
        
        return $res;
    }
    
    
    /**
     * Test if a modification in a project record fields must update all tasks
     * 
     * @param Project $oldProject The project
     * @param Project $newProject The modified project (new revision)
     * 
     * @return boolean
     */
    public function isTaskUpdateRequired(Project $oldProject, Project $newProject)
    {
        if ((int) $oldProject->ScheduleFromStart !== (int) $newProject->ScheduleFromStart) {
            return true;
        }
        
        if ($newProject->ScheduleFromStart) {
            
            if ($oldProject->StartDate !== $newProject->StartDate) {
                return true;
            }
        } else {
            
            if ($oldProject->FinishDate !== $newProject->FinishDate) {
                return true;
            }
        }
        
        return false;
    }
}
