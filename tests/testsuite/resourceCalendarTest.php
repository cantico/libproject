<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 *  
 */
namespace Ovidentia\LibProject;



class ResourceCalendarTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @beforeClass
     */
    public static function loadMockObjects()
    {
        require_once dirname(__FILE__).'/../mockObjects.php';
    }
    
    
    /**
     * Get a resource calendar
     * @return \Ovidentia\LibProject\Calendar
     */
    protected function resourceCalendar()
    {
        $mock = new MockCalendar();
        return $mock->getResourceCalendar();
    }
    
    
    public function testResourceCalendarInstance()
    {
        $calendar = $this->resourceCalendar();
    
        $this->assertInstanceOf('Ovidentia\LibProject\Calendar', $calendar);
    }
    
    
    public function testMainCalendarInstance()
    {
        $resourceCalendar = $this->resourceCalendar();
        $baseCalendar = $resourceCalendar->getBaseCalendar();
        
        $this->assertNotEmpty($resourceCalendar->BaseCalendarUID);
        $this->assertInstanceOf('Ovidentia\LibProject\Calendar', $baseCalendar);
    }
    
    
    public function testResourceCalendarWeekDaysFrombaseCalendar()
    {
        $calendar = $this->resourceCalendar();
        $Iterator = $calendar->selectWeekDays();
        $this->assertEquals(7, $Iterator->count());
    }
    
    public function testResourceCalendarMondayFromBaseCalendar()
    {
        $calendar = $this->resourceCalendar();
        $monday = $calendar->getWeekday(WeekDay::MONDAY);
    
        $this->assertInstanceOf('Ovidentia\LibProject\WeekDay', $monday);
        $this->assertTrue($monday->isWorking());
    }
    
    
    public function testResourceCalendarWorkingDaysFromBaseCalendar()
    {
        $calendar = $this->resourceCalendar();
    
        $this->assertTrue($calendar->getWeekday(WeekDay::MONDAY)->isWorking());
        $this->assertTrue($calendar->getWeekday(WeekDay::TUESDAY)->isWorking());
        $this->assertTrue($calendar->getWeekday(WeekDay::WEDNESDAY)->isWorking());
        $this->assertTrue($calendar->getWeekday(WeekDay::THURSDAY)->isWorking());
        $this->assertTrue($calendar->getWeekday(WeekDay::FRIDAY)->isWorking());
        $this->assertFalse($calendar->getWeekday(WeekDay::SATURDAY)->isWorking());
        $this->assertFalse($calendar->getWeekday(WeekDay::SUNDAY)->isWorking());
    }
    
    
    public function testResourceCalendarMondayWorkingTimes()
    {
        $calendar = $this->resourceCalendar();
        $monday = $calendar->getWeekday(WeekDay::MONDAY);
        $workingTimes = $monday->selectWorkingTimes();
    
        $this->assertEquals(2, $workingTimes->count());
    
        $mondayHours = (int) ($monday->getWorkingTimeDuration() / 3600);
        $this->assertEquals(8, $mondayHours);
    }
    
    
    public function testNonWorkingDayWithWorkingTimesFromBaseCalendar()
    {
        $calendar = $this->resourceCalendar();
    
        $arr = $calendar->getExceptionDays();
    
        $this->assertTrue(isset($arr['2014-12-25']));
        $this->assertEquals(1, count($arr['2014-12-25']));
    }
    
    
    public function testAddWorkingTimesOnDayOffFromBaseCalendar()
    {
        $calendar = $this->resourceCalendar();
    
        $add1hour = $calendar->date()->addWorkingTimes(new \DateTime('2014-12-24 17:30:00'), 3600);
    
        $this->assertEquals(
            '2014-12-25 09:30:00',
            $add1hour->format('Y-m-d H:i:s'),
            'the addWorkingTimes ignore exception periods from base calendar'
        );
    }
    
    
    public function testAddTimeOnDayOffFromResourceCalendar()
    {
        $calendar = $this->resourceCalendar();
        $set = $calendar->getParentSet();
        
        $add1hour = $calendar->date()->add(new \DateTime('2014-08-15 17:30:00'), $set->getInterval('PT1H'));
    
        $this->assertEquals('2014-09-01 09:30:00', $add1hour->format('Y-m-d H:i:s'));
    }
}
