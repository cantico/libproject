<?php
// -------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
// -------------------------------------------------------------------------
/**
 *
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
namespace Ovidentia\LibProject;


/**
 * Methods to manage a human resource calendar
 *
 */
class CalendarUser
{
    /**
     * @var Calendar
     */
    private $calendar;
    
    
    /**
     * 
     * @param Calendar $calendar
     */
    public function __construct(Calendar $calendar)
    {
        $this->calendar = $calendar;
    }
    
    
    
    /**
     * Insert working times in calendar from the user caldendar
     * 
     * @param OvidentiaCalendar $ovical
     */
    public function insertWorkingTimes(OvidentiaCalendar $ovical)
    {
        $weekdaySet = $this->calendar->WeekDaySet();
        $workingtimeSet = Storage()->WorkingTimeSet();
    
        $res = $ovical->selectWorkingPeriods();
    
        $weekday = array();
    
        foreach ($res as $period) {
            // periodes de travail
    
            $daytype = getDayType(date('w', $period->ts_begin));
    
            if (! isset($weekday[$daytype])) {
                $weekdayRecord = $weekdaySet->newRecord();
                /* @var $weekdayRecord WeekDay */
    
                $weekdayRecord->DayType = $daytype;
                $weekdayRecord->DayWorking = 1;
                $weekdayRecord->calendar = $this->calendar->UID;
                $weekdayRecord->save();
                $weekday[$daytype] = $weekdayRecord->id;
            }
    
            $workingtime = $workingtimeSet->newRecord();
            /*@var $workingtime WorkingTime */
    
            $workingtime->FromTime = date('H:i:s', $period->ts_begin);
            $workingtime->ToTime = date('H:i:s', $period->ts_end);
            $workingtime->weekday = $weekday[$daytype];
            $workingtime->save();
        }
    }
    
    /**
     * Insert non working days into calendar (jours feries)
     *
     * @param OvidentiaCalendar $ovical
     */
    public function insertNonWorkingDays(OvidentiaCalendar $ovical)
    {
        $weekdaySet = $this->calendar->WeekDaySet();
        $timeperiodSet = Storage()->TimePeriodSet();
    
        $res = $ovical->selectNonWorkingDays($this->calendar->project());
    
        foreach ($res as $period) {
            $weekdayRecord = $weekdaySet->newRecord();
            /* @var $weekdayRecord WeekDay */
    
            $weekdayRecord->DayType = WeekDay::EXCEPTION;
            $weekdayRecord->DayWorking = 0;
            $weekdayRecord->calendar = $this->calendar->UID;
            $weekdayRecord->save();
    
            $timeperiod = $timeperiodSet->newRecord();
            /* @var $timeperiod TimePeriod */
    
            $timeperiod->FromDate = date('Y-m-d H:i:s', $period->ts_begin);
            $timeperiod->ToDate = date('Y-m-d H:i:s', $period->ts_end);
            $timeperiod->weekday = $weekdayRecord->id;
            $timeperiod->save();
        }
    }
    
    /**
     * Insert non vacation periods into calendar (jours de conges)
     *
     * @param OvidentiaCalendar $ovical
     */
    public function insertVacationPeriods(OvidentiaCalendar $ovical)
    {
        $weekdaySet = $this->calendar->WeekDaySet();
        $timeperiodSet = Storage()->TimePeriodSet();
    
        $res = $ovical->selectVacationPeriods($this->calendar->project());
    
        foreach ($res as $period) {
            $weekdayRecord = $weekdaySet->newRecord();
            /* @var $weekdayRecord WeekDay */
    
            $weekdayRecord->DayType = WeekDay::EXCEPTION;
            $weekdayRecord->DayWorking = 0;
            $weekdayRecord->calendar = $this->calendar->UID;
            $weekdayRecord->save();
    
            $timeperiod = $timeperiodSet->newRecord();
            /* @var $timeperiod TimePeriod */
    
            $timeperiod->FromDate = date('Y-m-d H:i:s', $period->ts_begin);
            $timeperiod->ToDate = date('Y-m-d H:i:s', $period->ts_end);
            $timeperiod->weekday = $weekdayRecord->id;
            $timeperiod->save();
        }
    }
}
