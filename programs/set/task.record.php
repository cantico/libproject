<?php
// -------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
// -------------------------------------------------------------------------
/**
 *
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
namespace Ovidentia\LibProject;

/**
 * Task
 *
 *
 * @property int $UID
 *
 * @property int $ID identifier of the task within the list of tasks.
 * @property string $Name
 * @property int $Type
 * @property string $WBS breakdown structure code of the task.
 * @property string $WBSLevel
 * @property int $OutlineNumber
 * @property int $OutlineLevel
 * @property int $Priority of the task from 0 to 1000.
 * @property string $Start start date of the task.
 * @property string $Finish finish date of the task.
 * @property string $Duration The planned duration of the task.
 * @property string $Work duration of scheduled work for the task.
 * @property string $ActualWork actual duration of work
 * @property string $Stop date that the task was stopped.
 * @property string $Resume date that the task resumed.
 * @property int $ResumeValid task can be resumed.
 * @property int $EffortDriven
 * @property int $Recurring
 * @property int $Estimated
 * @property int $Milestone
 * @property int $Summary task is a summary task.
 * @property int $Critical
 * @property string $EarlyStart
 * @property string $EarlyFinish
 * @property string $LateStart
 * @property string $LateFinish
 * @property int $PercentComplete of the task duration completed.
 * @property int $PercentWorkComplete of the task work completed.
 * @property string $OvertimeWork       The amount of overtime work scheduled for the task.
 *                                      (calculated)
 *                                      When a task is first created, the Overtime Work 
 *                                      field shows "0 hrs." When you enter scheduled overtime 
 *                                      work in the sheet portion of the Task Usage or 
 *                                      Resource Usage view for the assignment, 
 *                                      Microsoft Office Project rolls up these overtime 
 *                                      work hours and enters the total overtime work for 
 *                                      the task into this field.
 * @property string $ActualStart
 * @property string $ActualFinish
 * @property string $ActualDuration The actual duration of the task.
 * @property string $ActualOvertimeWork The actual overtime work for the task. (calculated)
 *                                      When you first create a task, the Actual Overtime Work 
 *                                      field contains zero. As the assigned resources for the 
 *                                      task report actual overtime work, it is entered into 
 *                                      the Actual Overtime Work field.
 *
 * @property string $RegularWork The amount of non-overtime work scheduled for the task.
 * @property string $RemainingDuration The amount of time required to complete
 *           the unfinished portion of the task.
 * @property string $RemainingWork The remaining work scheduled to complete the task.
 * @property int $ConstraintType
 * @property string $ConstraintDate argument for the task constraint type.
 * @property string $Deadline for the task to be completed.
 * @property string $Hyperlink of the hyperlink associated with the task.
 * @property string $HyperlinkAddress associated with the task.
 * @property int $IgnoreResourceCalendar
 * @property string $Notes
 * @property int $Active
 * 
 * @property Project $project
 * @method Project project()
 * 
 * @method TaskSet getParentSet()
 * 
 */
class Task extends Record
{
    /**
     * Task type, fixed resource units
     * 
     * Duration = Work ÷ Resource Units
     * 
     * Resource assignment units in Project are expressed 
     * either in percentages or decimals. 
     * At 100% units, a resource is working full-time; 
     * at 50% units, the resource is working half-time, and so on.
     * 
     * The default setting in Project scheduling is Fixed Units, 
     * meaning that whatever resource units you allocate, 
     * that value will remain fixed and the other two elements of 
     * the scheduling formula will be affected by changes.
     * 
     * @var int
     */
    const FIXED_UNITS = 0;
    
    
    /**
     * Task type, fixed duration
     * 
     * Duration = Work ÷ Resource Units 
     * 
     * Duration is the length of working time between 
     * the start and finish of a task. When you choose to make 
     * a task Fixed Duration, the task's duration remains 
     * at whatever value you enter and Project recalculates 
     * the resource units as you change assignments.
     * 
     * Note : A summary task always has a Fixed-duration task type. 
     * A summary task is based on the earliest start date and 
     * the latest finish date of its subtasks, and the calculation 
     * for a summary task's duration is based on its subtasks. 
     * So, you cannot directly change duration for a summary task.
     * 
     *  With effort-driven scheduling turned on, 
     *  adding more resources decreases the time units each 
     *  resource will have to work on the task.
     * 
     * @var int
     */
    const FIXED_DURATION = 1;
    
    /**
     * Task type, fixed work
     * 
     * Set the task to Fixed Work when you want the amount 
     * of work to remain constant, regardless of any change 
     * in duration or resource units. 
     * 
     * Remember, in Project, work is measured in time units, 
     * such as hours, and it is the amount of effort that a resource 
     * needs to complete a task. The total work for a task is the sum 
     * of all those time units, no matter how many 
     * resources are assigned to the task.
     * 
     * @var int
     */
    const FIXED_WORK = 2;
    
    
    /**
     * As Soon As Possible
     * With this constraint, schedules the task as early as it can,
     * given other scheduling parameters. No additional date 
     * restrictions are put on the task. This is the default 
     * constraint for newly created tasks in projects scheduled 
     * from the start date.
     */
    const ASAP = 0;
    
    /**
     * As Late As Possible
     * With this constraint, schedules the task as late as it can, 
     * given other scheduling parameters. No additional date 
     * restrictions are put on the task. This is the default 
     * constraint for newly created tasks in projects scheduled 
     * from the finish date.
     */
    const ALAP = 1;
    
    /**
     * Must Start On
     * This constraint indicates the exact date on which a task 
     * must be scheduled to begin. Other scheduling parameters 
     * such as task dependencies, lead or lag time, 
     * resource leveling, and delay can't affect scheduling 
     * the task unless this requirement is met.
     */
    const START_ON = 2;
    
    /**
     * Must Finish On
     * This constraint indicates the exact date on which a task 
     * must be scheduled to be completed. Other scheduling 
     * parameters such as task dependencies, lead or lag time, 
     * resource leveling, and delay can't affect scheduling 
     * the task unless this requirement is met.
     */
    const FINISH_ON = 3;
    
    /**
     * Start No Earlier Than
     * This constraint indicates the earliest possible date that 
     * you want this task to begin. The task cannot be scheduled 
     * to start any time before the specified date. For projects 
     * scheduled from the start date, this constraint is applied 
     * when you enter a start date for a task.
     */
    const START_NO_EARLIER_THAN = 4;
    
    /**
     * Start No Later Than
     * This constraint indicates the latest possible date that 
     * you want this task to begin. The task can be scheduled to 
     * start on or before the specified date. A predecessor won't 
     * be able to push a successor task with an SNLT constraint 
     * past the constraint date. For projects scheduled from the 
     * finish date, this constraint is applied when you enter a 
     * start date for a task.
     */
    const START_NO_LATER_THAN = 5;
    
    /**
     * Finish No Earlier Than
     * This constraint indicates the earliest possible date that 
     * you want this task to be completed. The task cannot be 
     * scheduled to finish any time before the specified date. 
     * For projects scheduled from the start date, this constraint 
     * is applied when you enter a finish date for a task.
     */
    const FINISH_NO_EARLIER_THAN = 6;
    
    /**
     * Finish No Later Than
     * This constraint indicates the latest possible date that you 
     * want this task to be completed. It can be scheduled to finish 
     * on or before the specified date. A predecessor won't be able 
     * to push a successor task with an FNLT constraint past the 
     * constraint date. For projects scheduled from the finish date, 
     * this constraint is applied when you enter a finish date for a task.
     */
    const FINISH_NO_LATER_THAN = 7;
    
    
    
    
    
    /**
     * Information type
     * 
     * Scheduled information is the current, most up-to-date task 
     * information. Fields containing scheduled information are Duration, 
     * Work, Start, Finish, and Cost. The scheduled Duration, Work, 
     * and Cost fields represent the total amount for that task.
     * 
     * When you first begin your project, the scheduled information is 
     * similar, if not identical, to your baseline planned information. 
     * However, as tasks are completed, you make adjustments and enter 
     * actual information. You find that one task needs three more days 
     * than first expected. Another task was able to start a day early. 
     * Another task incurred an unexpected cost. You find you need to 
     * change a constraint on one task and add a task dependency on 
     * another. With these adjustments, the scheduled information is 
     * recalculated to provide you with the most up-to-date picture 
     * of your project.
     * 
     * When you start entering actual information on in-progress tasks, 
     * scheduled information takes that into account and recalculates 
     * accordingly. For example, scheduled work is calculated as 
     * Actual Work + Remaining Work. For completed tasks, scheduled 
     * information is the same as actual information.
     * 
     */
    const SCHEDULED = 1;
    
    /**
     * Information type
     * 
     * Actual information reflects how the task was finally accomplished. 
     * You started with the planned projection of duration, work, cost, 
     * and start and finish dates. You enter progress information, or 
     * actuals, for tasks, and end up with the real picture of the 
     * completion of the task. Actuals tell you how much the task really 
     * cost, how many days of work it really took, the actual duration, 
     * and the real start and finish dates.
     */
    const ACTUAL = 2;
    
    
    /**
     * Date type
     */
    const EARLY = 3;
    
    /**
     * Date type
     */
    const LATE = 4;
    
    
    /**
     * Information type, planned or baseline
     *
     * Planned information is also known as baseline information.
     * Examples of fields containing baseline information include
     * Baseline Work, Baseline Start, Baseline Cost, and so on.
     *
     * When you build and refine your project to the point where you're
     * confident you can start the project, you have a good beginning
     * point, or baseline. If you save baseline task information at
     * that point, at later points throughout the project you can
     * compare your current progress against your initial plan.
     * Saving the baseline is essential for meaningful project
     * tracking and analysis.
     *
     *
     * @todo not supported, baseline information need a specific SET
     */
    const PLANNED = 5;
    const BASELINE = 5;
    
    

    /**
     * Set used to query links
     * 
     * @var PredecessorLinkSet
     */
    private $predecessorLinkSet = null;
    
    
    /**
     * path cache
     * @var Path | boolean
     */
    private $path = false;
    
    /**
     * path cache
     * @var Path | boolean
     */
    private $upperPath = false;
    
    /**
     * path cache
     * @var Path | boolean
     */
    private $lowerPath = false;
    

    /**
     * Test if the task is overallocated
     * field for MSPDI export: OverAllocated
     * 
     * @return bool
     */
    public function overAllocated()
    {
        return ('' !== $this->ActualOvertimeWork) || ('' !== $this->OvertimeWork);
    }

    /**
     * Get the resources assignements on tasks
     *
     * @return Assignment[]
     */
    public function selectAssignments()
    {
        $set = Storage()->AssignmentSet();
        Storage()->includeResourceSet();
        $set->ResourceUID();
        $res = $set->select($set->TaskUID->is($this->UID));
        $res->orderAsc($set->ResourceUID->Name);
        
        return $res;
    }
    
    
    /**
     * Get one assignment by resource UID
     * or null if there is no assignment with this resource
     * 
     * @param string $ResourceUID
     * 
     * @return Assignment
     */
    public function getAssignment($ResourceUID)
    {
        $set = Storage()->AssignmentSet();
        Storage()->includeResourceSet();
        $set->ResourceUID();
        return $set->get($set->TaskUID->is($this->UID)->_AND_($set->ResourceUID->UID->is($ResourceUID)));
    }
    
    

    /**
     * Set or get the predecessor link set used to query the linked tasks
     * 
     * @param PredecessorLinkSet $set            
     * @return PredecessorLinkSet
     */
    public function predecessorLinkSet($set = null)
    {
        if (isset($set)) {
            $this->predecessorLinkSet = $set;
        }
        
        if (! isset($this->predecessorLinkSet)) {
            $this->predecessorLinkSet = Storage()->PredecessorLinkSet();
            
            Storage()->includeTaskSet();
            $this->predecessorLinkSet->task();
            $this->predecessorLinkSet->PredecessorUID();
        }
        
        return $this->predecessorLinkSet;
    }

    /**
     * A predecessor is a task whose start or finish date determines the start or finish date of its successor task.
     *
     * the predecessor task is available in PredecessorLink::PredecessorUID
     * 
     * @return PredecessorLink[]
     */
    public function selectPredecessors()
    {
        $set = $this->predecessorLinkSet();
        $res = $set->select($set->task->UID->is($this->UID));
        
        $res->orderAsc($set->task->ID);
        
        return $res;
    }

    /**
     * A successor is a task whose start or finish date is driven by its predecessor task.
     * the success task is available in PredecessorLink::task
     * 
     * @return PredecessorLink[]
     */
    public function selectSuccessors()
    {
        $set = $this->predecessorLinkSet();
        $res = $set->select($set->PredecessorUID->UID->is($this->UID));
        
        $res->orderAsc($set->task->ID);
        
        return $res;
    }

    /**
     * Get calendar used for dates manipulations on tasks
     * do not use the ressources calendar to get the same behaviour as GanttProject, instead we use the default calendar
     *
     * @return Calendar
     */
    public function getCalendar()
    {
        $set = Storage()->CalendarSet();
        return $set->get(
            $set->project->is($this->project)
            ->_AND_(
                $set->IsBaseCalendar->is(true)
            )
        );
    }

    /**
     * Get start date by type
     * 
     * @param int $date_type
     *            Task::ACTUAL | Task::EARLY | Task::LATE
     *            
     * @return string
     */
    public function getStartDate($date_type)
    {
        switch ($date_type) {
            case Task::SCHEDULED:
                return $this->Start;
            case Task::ACTUAL:
                return $this->ActualStart;
            case Task::EARLY:
                return $this->EarlyStart;
            case Task::LATE:
                return $this->LateStart;
        }
    }

    /**
     * Get finish date by type
     * 
     * @param int $date_type
     *            Task::ACTUAL | Task::EARLY | Task::LATE
     *            
     * @return string
     */
    public function getFinishDate($date_type)
    {
        switch ($date_type) {
            case Task::SCHEDULED:
                return $this->Finish;
            case Task::ACTUAL:
                return $this->ActualFinish;
            case Task::EARLY:
                return $this->EarlyFinish;
            case Task::LATE:
                return $this->LateFinish;
        }
    }
    
    /**
     * Get constraint date if valid
     * @throws MandatoryFieldException
     * 
     * @return string
     */
    public function getConstraintDate()
    {
        $ConstraintType = (int) $this->ConstraintType;
        
        if (self::ASAP === $ConstraintType || self::ALAP === $ConstraintType) {
            return null;
        }
        
        if (! isset($this->ConstraintDate) || '0000-00-00 00:00:00' === $this->ConstraintDate) {
            throw new MandatoryFieldException('The ConstraintDate field is mandatory for this ConstraintType value');
        }
        
        return $this->ConstraintDate;
    }
    

    /**
     * Test the constraint
     * always return true if constraint does not apply on a date or if dates are not set
     *
     * @throws MandatoryFieldException
     * @throws Exception
     *
     * @param int $date_type
     *            Task::ACTUAL | Task::EARLY | Task::LATE
     *            
     * @return bool
     */
    public function isConstraintSatisfied($date_type)
    {
        $ConstraintType = (int) $this->ConstraintType;
        
        if (self::ASAP === $ConstraintType || self::ALAP === $ConstraintType) {
            return true;
        }
        
        $Start = $this->getStartDate($date_type);
        $Finish = $this->getFinishDate($date_type);
        
        $noStart = (! isset($Start) || '0000-00-00 00:00:00' === $Start);
        $noFinish = (! isset($Finish) || '0000-00-00 00:00:00' === $Finish);
        
        if ($noStart || $noFinish) {
            return true;
        }
        
        return $this->testConstraint($Start, $Finish, $this->getConstraintDate());
    }
    
    /**
     * Test constraint with date parameters
     * 
     * @param string $Start
     * @param string $Finish
     * @param string $ConstraintDate
     * 
     * @return bool
     */
    private function testConstraint($Start, $Finish, $ConstraintDate)
    {
        $ConstraintType = (int) $this->ConstraintType;
        
        switch ($ConstraintType) {
            case Task::ASAP:
            case Task::ALAP:
                return true;
            case Task::START_ON:
                return ($Start === $ConstraintDate);
            case Task::FINISH_ON:
                return ($Finish === $ConstraintDate);
            case Task::START_NO_EARLIER_THAN:
                return ($Start >= $ConstraintDate);
            case Task::START_NO_LATER_THAN:
                return ($Start <= $ConstraintDate);
            case Task::FINISH_NO_EARLIER_THAN:
                return ($Finish >= $ConstraintDate);
            case Task::FINISH_NO_LATER_THAN:
                return ($Finish <= $ConstraintDate);
        }
        
        throw new Exception('Unsuported constraint type');
    }
    
    
    /**
     * Test if the task has an actual finish date expired or completed at 100%
     * frozen task should not be modified
     * @return bool
     */
    public function isFrozen()
    {
        if (100 <= (int) $this->PercentComplete) {
            return true;
        }
        
        if ($this->ActualFinish < date('Y-m-d H:i:s')) {
            return true;
        }
        
        return false;
    }

    
    /**
     * Set 3 durations from dates and percentComplete
     * This method do not modify task scheduling (duration is set according to dates)
     * The setDurations method should be used after modification of the dates
     *
     * - Duration (planned duration)
     * - ActualDuration
     * - RemainingDuration, computed from the actual dates
     * 
     * @return bool
     */
    public function setDurations()
    {

        if ($interval = getInterval($this->Start, $this->Finish)) {
            $this->Duration = storage()->utilit()->getIntervalSpec($interval);
        }
        
        if ($interval = getInterval($this->ActualStart, $this->ActualFinish)) {
            $this->ActualDuration = storage()->utilit()->getIntervalSpec($interval);
            $actualDays = (int) $interval->format('%a');
            $percentcomplete = (int) $this->PercentComplete;
            $percentRemaining = (100 - $percentcomplete);
            $remainingDays = round($actualDays*$percentRemaining / 100);
            
            $this->RemainingDuration = 'P'.$remainingDays.'D';
        } else {
            $this->RemainingDuration = '';
        }
        
        return true;
    }
    
    
    /**
     * Get interval beetween actual dates
     * @return \DateInterval
     */
    public function getActualInterval()
    {
        $actualStart = getDateTime($this->ActualStart);
        $actualFinish = getDateTime($this->ActualFinish);
        
        if (isset($actualStart) && isset($actualFinish)) {
            $interval = $actualStart->diff($actualFinish);
        } elseif (!empty($this->Duration)) {
            $interval = new \DateInterval($this->Duration);
        } else {
            throw new MandatoryFieldException('Missing actual dates or duration');
        }
        
        return $interval;
    }
    
    
    /**
     * Set actual dates if allowed by constraint
     * or the nearest position alloweb by constraint
     * return true if actual dates has been modified
     * 
     * warning, this method does not adjust duration, period has to be the same size
     * 
     * @param \DateTime     $start
     * @param \DateTime     $finish
     * @param \DateInterval $interval   Duration beetween the two dates
     * 
     * @return boolean
     */
    private function setActualDateIfPossible(\DateTime $start, \DateTime $finish, \DateInterval $interval)
    {
        $actualstart = $start->format('Y-m-d H:i:s');
        $actualfinish = $finish->format('Y-m-d H:i:s');
        
        if (!$this->testConstraint($actualstart, $actualfinish, $this->getConstraintDate())) {
        
            $ConstraintType = (int) $this->ConstraintType;
        
            switch ($ConstraintType) {
                case Task::ALAP:
                    // end on project end date
                    // or before the start of a linked task
                    break;
                case Task::START_NO_EARLIER_THAN:
                case Task::START_NO_LATER_THAN:
                    $start = new \DateTime($this->ConstraintDate);
                    $finish = clone $start;
                    $finish->add($interval);
                    break;
                case Task::FINISH_NO_EARLIER_THAN:
                case Task::FINISH_NO_LATER_THAN:
                    $finish = new \DateTime($this->ConstraintDate);
                    $start = clone $finish;
                    $start->sub($interval);
                    break;
                default:
                    // Do not modify because of the constraint
                    return false;
            }
        
            $actualstart = $start->format('Y-m-d H:i:s');
            $actualfinish = $finish->format('Y-m-d H:i:s');
        }
        
        
        if ($this->ActualStart !== $actualstart || $this->ActualFinish !== $actualfinish) {
            $this->ActualStart = $actualstart;
            $this->ActualFinish = $actualfinish;
            return true;
        }
        
        return false;
    }
    
    
    /**
     * Update actual dates, start after a required datetime
     * respect task constraints
     * This method is called if the project boundaries are modified
     * to try to fit the task after the project start date
     * or if a predecessor/successor force modification of the task start
     *  
     * @param \DateTime $start
     *  
     * @return bool
     */
    public function setActualStartAfter(\DateTime $start)
    {
        $interval = $this->getActualInterval();
        
        $finish = clone $start;
        $finish->add($interval);
        
        return $this->setActualDateIfPossible($start, $finish, $interval);
    }
    
     
    /**
     * Update actual dates, start before a required datetime
     * respect task constraints
     * This method is called if the project boundaries are modified
     * to try to fit the task before the project finish date
     * or if a predecessor/successor force modification of the task finish date
     * 
     * @param \DateTime $finish
     * @return bool
     */
    public function setActualFinishBefore(\DateTime $finish)
    {
        $interval = $this->getActualInterval();
        
        $start = clone $finish;
        $start->sub($interval);
        
        return $this->setActualDateIfPossible($start, $finish, $interval);
    }
    
    
    /**
     * Get path of the task or null
     * if the task is orphan
     * 
     * @todo cache one path per path instead of one path per task
     * 
     * @return Path | null
     */
    public function getPath()
    {
        if (false === $this->path) {
            $path = new Path();
            if ($path->loadFromTask($this)) {
                $this->path = $path;
            } else {
                $this->path = null;
            }
        }
        return $this->path;
    }
    
    
    /**
     * Get the upper path of the task or null
     * if the task is orphan
     * the upper path is all task related (by prdecessor or successor) 
     * to the predecessors of the task
     *
     * @return Path | null
     */
    public function getUpperPath()
    {
        if (false === $this->upperPath) {
            $path = new Path();
            if ($path->loadUpperFromTask($this)) {
                $this->upperPath = $path;
            } else {
                $this->upperPath = null;
            }
        }
        return $this->upperPath;
    }
    
    /**
     * Get the lower path of the task or null
     * if the task is orphan
     * the lower path is all task related (by prdecessor or successor) 
     * to the sucessors of the task
     *
     * @return Path | null
     */
    public function getLowerPath()
    {
        if (false === $this->upperPath) {
            $path = new Path();
            if ($path->loadLowerFromTask($this)) {
                $this->upperPath = $path;
            } else {
                $this->upperPath = null;
            }
        }
        return $this->upperPath;
    }
    
    
    
    /**
     * Get actual dates period boundaries from the task constraint
     * return null if the constraint do not set any boundaries on dates
     * 
     * @return PeriodBoundaries
     */
    protected function getConstraintBoundaries()
    {
        $ConstraintType = (int) $this->ConstraintType;
        
        if (self::ASAP === $ConstraintType || self::ALAP === $ConstraintType) {
            return null;
        }
        
        $constraintDate = $this->getConstraintDate();
        $boundaries = new PeriodBoundaries();
        
        switch($ConstraintType) {
            case Task::START_ON:
                $boundaries->setStartMin($constraintDate);
                $boundaries->setStartMax($constraintDate);
                break;
            case Task::FINISH_ON:
                $boundaries->setFinishMin($constraintDate);
                $boundaries->setFinishMax($constraintDate);
                break;
            case Task::START_NO_EARLIER_THAN:
                $boundaries->setStartMin($constraintDate);
                break;
            case Task::START_NO_LATER_THAN:
                $boundaries->setStartMax($constraintDate);
                break;
            case Task::FINISH_NO_EARLIER_THAN:
                $boundaries->setFinishMin($constraintDate);
                break;
            case Task::FINISH_NO_LATER_THAN:
                $boundaries->setFinishMax($constraintDate);
                break;
        }
        
        return $boundaries;
    }
    
    
    /**
     * Get boundaries for actual dates
     * @return PeriodBoundaries
     */
    public function getDatesBoundaries()
    {
        if ($path = $this->getPath()) {
            $boundaries = $path->getDatesBoundaries($this);
        } else {
            $boundaries = new PeriodBoundaries();
        }
        
        // add contraints to boundaries
        if ($constraints = $this->getConstraintBoundaries()) {
            $boundaries->combine($constraints);
        }
        
        $project = $this->project();
        
        // add project start date or project end date to boundaries
        if (isset($project) && $projectBoundaries = $project->getTaskBoundaries()) {
            $boundaries->combine($projectBoundaries);
        }
        
        return $boundaries;
    }
    
    
    
    /**
     * Update predecessors recusively
     * to match the links constraints in all upper tasks if possible.
     * If task constraints prevent modification, adjust modification
     * to fullfill task constraint also
     */
    public function updatePredecessors()
    {
        foreach ($this->selectPredecessors() as $link) {
            /*@var $link PredecessorLink */
            $task = $link->PredecessorUID;
            
            $task->updatePredecessors();
        }
    }
    
    /**
     * Update successors recusively
     * to match the links constraints in all upper tasks if possible.
     * If task constraints prevent modification, adjust modification
     * to fullfill task constraint also
     */
    public function updateSuccessors()
    {
        foreach ($this->selectSuccessors() as $link) {
            /*@var $link PredecessorLink */
            $task = $link->task;
            
            $task->updateSuccessors();
        }
    }
    
    
    /**
     * Set default values for a new task
     * @return Task
     */
    public function setNewTaskDefaultValues()
    {
        return $this;
    }
    
    

    /**
     * Append record to XML under the $node
     * 
     * @param \DOMDocument $xml            
     * @param \DOMElement $node            
     */
    protected function appendXml(\DOMDocument $xml, \DOMElement $node)
    {
        parent::appendXml($xml, $node);
        
        // append PredecessorLinks
        $this->appendXmlIterator($xml, $node, $this->selectPredecessors());
    }

    /**
     * Receive the parent record
     * use this method to update links in record
     */
    public function updateParentByXml(Record $parent)
    {
        $this->project = $parent->uuid;
    }
    
    
    /**
     * Save imported task
     * initialize task name if not set
     * 
     * @return bool
     */
    public function importSave()
    {
        if (!$this->Name) {
            $this->Name = 'Task';
        }
        
        return parent::save();
    }
}
