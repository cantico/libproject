<?php 


require_once $GLOBALS['babInstallPath'].'utilit/session.class.php';
require_once $GLOBALS['babInstallPath'].'utilit/uuid.php';

require_once dirname(__FILE__).'/functions.php';
require_once dirname(__FILE__).'/utilit/import.class.php';


$storage = Ovidentia\LibProject\storage();
$storage->loadOrm();
$set = $storage->ProjectSet();

// create an empty project

$project = $set->get('a1518036-10ba-4494-a1d7-a9deade986e4');
if (isset($project)) {
    $project->deleteAll();
} else {
    $project = $set->newRecord();
    $project->uuid = 'a1518036-10ba-4494-a1d7-a9deade986e4';
}

$mspdi = new \DOMDocument();
$mspdi->preserveWhiteSpace = false;
$mspdi->load('/home/paul/Dropbox/Cantico/Gestion des projets/Test cantico export MSPDI.xml');


$import = new Ovidentia\LibProject\MspdiImport($mspdi, $project);
$import->importProject();

$newmspdi = $project->getDOMDocument();
$newmspdi->formatOutput = true;
echo $newmspdi->saveXML();
