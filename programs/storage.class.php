<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */


// @codingStandardsIgnoreStart

/**
 * Projects storage functionality
 *
 */
class Func_ProjectStorage extends bab_functionality
{
    
    // @codingStandardsIgnoreEnd
    
    /**
     * (non-PHPdoc)
     * @see bab_functionality::getDescription()
     */
    public function getDescription()
    {
        return 'projects storage fonctionality';
        //return translate('projects storage fonctionality');
    }
    
    
    /**
     * Initialize mysql ORM backend.
     */
    public function loadOrm()
    {
        if (!class_exists('ORM_MySqlRecordSet')) {
            $Orm = \bab_functionality::get('LibOrm');
            /*@var $Orm Func_LibOrm */
            $Orm->initMySql();
    
            $mysqlbackend = new \ORM_MySqlBackend($GLOBALS['babDB']);
            \ORM_MySqlRecordSet::setBackend($mysqlbackend);
        }
        
        require_once dirname(__FILE__).'/functions.php';
        $this->requireFiles();
    }
    
    /**
     * Used for test, oherwise it is allready loaded by loadOrm method
     */
    public function requireFiles()
    {
        require_once dirname(__FILE__).'/utilit/base.class.php';
        require_once dirname(__FILE__).'/utilit/base.record.php';
        
        require_once dirname(__FILE__).'/utilit/workingdays.iterator.php';
        require_once dirname(__FILE__).'/utilit/workingtimes.iterator.php';
        require_once dirname(__FILE__).'/utilit/schedulableperiods.iterator.php';
        require_once dirname(__FILE__).'/utilit/timeperiodday.class.php';
        
        require_once dirname(__FILE__).'/utilit/xml.php';
        require_once dirname(__FILE__).'/utilit/date.php';
        require_once dirname(__FILE__).'/utilit/exception.class.php';
        
        require_once dirname(__FILE__).'/utilit/workingtimes.iterator.php';
        require_once dirname(__FILE__).'/utilit/schedulableperiod.class.php';
        require_once dirname(__FILE__).'/utilit/workingtimedate.class.php';
        
        require_once dirname(__FILE__).'/utilit/periodboundaries.class.php';
        require_once dirname(__FILE__).'/utilit/path.class.php';
    }
    
    
    
    /**
     * Include calendar
     */
    public function includeCalendarSet()
    {
        require_once dirname(__FILE__).'/set/calendar.set.php';
        require_once dirname(__FILE__).'/set/calendar.record.php';
    }
    
    
    /**
     * @return \Ovidentia\LibProject\CalendarSet
     */
    public function calendarSet()
    {
        $this->includeCalendarSet();
        return new Ovidentia\LibProject\CalendarSet();
    }
    
    /**
     * Include week day
     */
    public function includeWeekDaySet()
    {
        require_once dirname(__FILE__).'/set/calendar_weekday.set.php';
        require_once dirname(__FILE__).'/set/calendar_weekday.record.php';
    }
    
    /**
     * @return \Ovidentia\LibProject\WeekDaySet
     */
    public function weekDaySet()
    {
        $this->includeWeekDaySet();
        return new Ovidentia\LibProject\WeekDaySet();
    }
    
    /**
     * Include Working time
     */
    public function includeWorkingTimeSet()
    {
        require_once dirname(__FILE__).'/set/calendar_workingtime.set.php';
        require_once dirname(__FILE__).'/set/calendar_workingtime.record.php';
    }
    
    /**
     * @return \Ovidentia\LibProject\WorkingTimeSet
     */
    public function workingTimeSet()
    {
        $this->includeWorkingTimeSet();
        return new Ovidentia\LibProject\WorkingTimeSet();
    }
    
    
    /**
     * Include time period
     */
    public function includeTimePeriodSet()
    {
        require_once dirname(__FILE__).'/set/calendar_timeperiod.set.php';
        require_once dirname(__FILE__).'/set/calendar_timeperiod.record.php';
    }
    
    /**
     * @return \Ovidentia\LibProject\TimePeriodSet
     */
    public function timePeriodSet()
    {
        $this->includeTimePeriodSet();
        return new Ovidentia\LibProject\TimePeriodSet();
    }
    
    
    
    /**
     * Include Project
     */
    public function includeProjectSet()
    {
        require_once dirname(__FILE__).'/set/project.set.php';
        require_once dirname(__FILE__).'/set/project.record.php';
    }
    
    
    /**
     * @return \Ovidentia\LibProject\ProjectSet
     */
    public function projectSet()
    {
        $this->includeProjectSet();
        return new Ovidentia\LibProject\ProjectSet();
    }
    
    
    /**
     * Include Resource
     */
    public function includeResourceSet()
    {
        require_once dirname(__FILE__).'/set/resource.set.php';
        require_once dirname(__FILE__).'/set/resource.record.php';
    }
    
    /**
     * @return \Ovidentia\LibProject\ResourceSet
     */
    public function resourceSet()
    {
        $this->includeResourceSet();
        return new Ovidentia\LibProject\ResourceSet();
    }
    
    
    /**
     * Include Availablity Period
     */
    public function includeAvailabilityPeriodSet()
    {
        require_once dirname(__FILE__).'/set/resource_availability.set.php';
        require_once dirname(__FILE__).'/set/resource_availability.record.php';
    }
    
    /**
     * @return Ovidentia\LibProject\AvailabilityPeriodSet
     */
    public function availabilityPeriodSet()
    {
        $this->includeAvailabilityPeriodSet();
        return new Ovidentia\LibProject\AvailabilityPeriodSet();
    }
    
    /**
     * Include Task
     */
    public function includeTaskSet()
    {
        require_once dirname(__FILE__).'/set/task.set.php';
        require_once dirname(__FILE__).'/set/task.record.php';
    }
    
    
    /**
     * @return \Ovidentia\LibProject\TaskSet
     */
    public function taskSet()
    {
        $this->includeTaskSet();
        return new Ovidentia\LibProject\TaskSet();
    }
    
    /**
     * Include Predecessor link
     */
    public function includePredecessorLinkSet()
    {
        require_once dirname(__FILE__).'/set/task_predecessorlink.set.php';
        require_once dirname(__FILE__).'/set/task_predecessorlink.record.php';
    }
    
    
    /**
     * @return \Ovidentia\LibProject\PredecessorLinkSet
     */
    public function predecessorLinkSet()
    {
        $this->includePredecessorLinkSet();
        return new Ovidentia\LibProject\PredecessorLinkSet();
    }
    
    /**
     * Include Assignment
     */
    public function includeAssignmentSet()
    {
        require_once dirname(__FILE__).'/set/assignment.set.php';
        require_once dirname(__FILE__).'/set/assignment.record.php';
    }
    
    /**
     * @return \Ovidentia\LibProject\AssignmentSet
     */
    public function assignmentSet()
    {
        $this->includeAssignmentSet();
        return new Ovidentia\LibProject\AssignmentSet();
    }
    
    
    
    public function mspdiImport(\DOMDocument $xml, \Ovidentia\LibProject\Project $projectRecord)
    {
        require_once dirname(__FILE__).'/utilit/import.class.php';
        return new Ovidentia\LibProject\MspdiImport($xml, $projectRecord);
    }
    
    /**
     * @return Utilit
     */
    public function utilit()
    {
        require_once dirname(__FILE__).'/utilit/utlit.class.php';
        return new Utilit();
    }
}
