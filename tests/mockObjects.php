<?php 
namespace Ovidentia\LibProject;
require_once dirname(__FILE__).'/mock/MockOvidentia.php';
require_once dirname(__FILE__).'/mock/MockDatabase.php';
require_once dirname(__FILE__).'/mock/MockCalendar.php';
require_once dirname(__FILE__).'/mock/MockProject.php';
require_once dirname(__FILE__).'/../programs/storage.class.php';




function translate($str) {
    return $str;
}



/**
 * Generate a pseudo-random UUID according to RFC 4122
 *
 * @return string the new UUID
 */
function uuid()
{
    return sprintf( '%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
        mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0xffff),
        mt_rand(0, 0x0fff) | 0x4000,
        mt_rand(0, 0x3fff) | 0x8000,
        mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0xffff)
    );
}




/**
 * @return Func_ProjectStorage
 */
function storage()
{
    $storage = new \Func_ProjectStorage;
    // ORM allready loaded for tests
    $storage->requireFiles();
    return $storage;
}








/**
 * Return a set of two associated tasks
 * @return array
 */
function MockTwoRelatedTasks()
{
    $storage = Storage();
    $taskSet = $storage->TaskSet();
    $predSet = $storage->PredecessorLinkSet();
    $predSet->task();
    $predSet->PredecessorUID();
    
    $mock = new MockProject();
    $project = $mock->getProject();
    $projectSet = $project->getParentSet();
    
    $taskSet->project->setForeignSet($projectSet);

    /*@var $backend ORM_MySqlMockBackend */
    $backend = $taskSet->getBackend();
    
    $task1 = $taskSet->newRecord();
    $task1->predecessorLinkSet($predSet);
    $task1->Name = 'Predecessor';
    $task1->ID = '1';
    $task1->ActualStart = '2014-01-01 00:00:00';
    $task1->ActualFinish = '2014-01-02 00:00:00';
    $task1->Duration = 'P1D';
    $task1->Work = 'PT8H';
    $task1->RegularWork = 'PT8H';
    $task1->OvertimeWork = 'PT0H';
    $task1->ActualOvertimeWork = 'PT0H';
    $task1->ActualDuration = 'P1D';
    $task1->RemainingDuration = 'PT1H';
    $task1->PercentComplete = '70';
    $task1->RemainingWork = 'PT1H';
    $task1->project = bab_uuid();
    $task1->Active = 1;
    $task1->project = $project->uuid;
    $task1->save(); // create the UID
    
    $task2 = $taskSet->newRecord();
    $task2->predecessorLinkSet($predSet);
    $task2->Name = 'Successor';
    $task2->ID = '2';
    $task2->ActualStart = '2014-01-02 00:00:00';
    $task2->ActualFinish = '2014-01-03 00:00:00';
    $task2->Duration = 'P1DT4H';
    $task2->Work = 'PT12H';
    $task2->RegularWork = 'PT8H';
    $task2->OvertimeWork = 'PT4H';
    $task2->ActualOvertimeWork = 'PT4H';
    $task2->ActualDuration = 'P1D';
    $task2->RemainingDuration = 'PT6H';
    $task2->PercentComplete = '40';
    $task2->RemainingWork = 'PT7H';
    $task2->project = bab_uuid();
    $task2->Active = 1;
    $task2->project = $project->uuid;
    $task2->save(); // create the UID
    
    // response for a call to $task2->selectPredecessors()
    $predecessor = $predSet->newRecord();
    $predecessor->save(); // set default values on record
    $predecessor->task = $task2;
    $predecessor->PredecessorUID = $task1;
    
    
    $backend->setSelectReturn($predSet, $predSet->task->UID->is($task2->UID),  array($predecessor));
    
    // response for a call to $task1->selectSuccessors()
    $successor = $predSet->newRecord();
    $successor->save(); // set default values on record
    $successor->task = $task2;
    $successor->PredecessorUID = $task1;
    
    
    $backend->setSelectReturn($predSet, $predSet->PredecessorUID->UID->is($task1->UID),  array($successor));
    
    // response for $task1->project()
    // response for $task2->project()
    $backend->setSelectReturn($projectSet, $projectSet->uuid->is($task1->project),  array($project));

    
    return array($task1, $task2);
}
