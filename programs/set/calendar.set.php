<?php
// -------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
// -------------------------------------------------------------------------
/**
 *
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
namespace Ovidentia\LibProject;

/**
 *
 * Fields are named according to the MSPDI format
 *
 * @property \ORM_PkField $UID
 * @property \ORM_StringField $Name
 * @property \ORM_BoolField $IsBaseCalendar
 * @property ProjectSet $project
 * @property CalendarSet $basecalendar
 * @method Calendar	newRecord()
 * @method Calendar	get()
 */
class CalendarSet extends RecordSet
{

    /**
     * Fields initialisation
     */
    public function __construct()
    {
        parent::__construct();
        
        $this->addFields(
            ORM_PkField('UID')->setAutoIncrement(false),
            ORM_StringField('Name'),
            ORM_BoolField('IsBaseCalendar')
        );
        
        $this->setPrimaryKey('UID');
        
        // parent in XML
        $this->hasOne('project', '\Ovidentia\LibProject\ProjectSet', 'ORM_StringField');
        
        // the base calendar on which this calendar depends.
        // Only applicable if the calendar is not a base calendar.
        $this->hasOne('BaseCalendarUID', '\Ovidentia\LibProject\CalendarSet');
    }

    /**
     * (non-PHPdoc)
     * 
     * @see Ovidentia\LibProject.RecordSet::save()
     *
     * @return bool
     */
    public function save(Calendar $record)
    {
        if (empty($record->Name)) {
            throw new MandatoryFieldException('Name is mandatory');
        }
        
        if (empty($record->project)) {
            throw new MandatoryFieldException('project is mandatory');
        }
        
        if ($record->IsBaseCalendar) {
            // ensure the is only one base calendar
            $res = $this->select(
                $this->IsBaseCalendar->is(true)
                ->_AND_(
                    $this->UID->is($record->UID)
                    ->_NOT()
                )
            );
            
            foreach ($res as $oldBaseCalendar) {
                /*@var $oldBaseCalendar Calendar */
                $oldBaseCalendar->IsBaseCalendar = false;
                $oldBaseCalendar->save();
            }
        }
        
        return parent::save($record);
    }

    /**
     * Delete calendar with weekdays
     * 
     * @param \ORM_Criteria $oCriteria
     * @see ORM_RecordSet::delete()
     */
    public function delete(\ORM_Criteria $oCriteria = null)
    {
        $res = $this->select($oCriteria);
        
        foreach ($res as $calendar) {
            $calendar->deleteWeekDays();
        }
        
        return parent::delete($oCriteria);
    }
}
