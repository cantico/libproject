<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 *  
 */
namespace Ovidentia\LibProject;



class CalendarTimePeriodTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @beforeClass
     */
    public static function loadMockObjects()
    {
        require_once dirname(__FILE__).'/../mockObjects.php';
    }
    
    
    /**
     * Get a base calendar, basic tests included
     * 
     * @param bool $workingTimes       Include morking times or not
     */
    protected function baseCalendar($workingTimes)
    {
        $mock = new MockCalendar();
        return $mock->getBaseCalendar($workingTimes);

    }
    
    
    
    public function testTimePeriodDay()
    {
        $calendar = $this->baseCalendar(true);
        
        $arr = $calendar->getExceptionDays();
        $christmas = $arr['2014-12-25'];
        
        /*@var $christmas TimePeriodDay */
        
        $this->assertInstanceOf('\Ovidentia\LibProject\TimePeriodDay', $christmas);
    }
    
    public function testTimePeriodDayUncoveredDayOff()
    {
        
        $calendar = $this->baseCalendar(true);
        
        $arr = $calendar->getExceptionDays();
        $christmas = $arr['2014-12-25'];
        /*@var $christmas TimePeriodDay */
        
        $this->assertCount(0, $christmas->getUncoveredPeriods());
    }
    
    
    public function testTimePeriodDayUncoveredAbsence1H30()
    {
    
        $calendar = $this->baseCalendar(true);
    
        $arr = $calendar->getExceptionDays();
        $dec24 = $arr['2014-12-24'];
        /*@var $dec24 TimePeriodDay */
        
        $periods = $dec24->getUncoveredPeriods();
    
        $this->assertCount(2, $periods);
        
        $this->assertEquals('2014-12-24 00:00:00', $periods[0][0]->format('Y-m-d H:i:s'));
        $this->assertEquals('2014-12-24 15:00:00', $periods[0][1]->format('Y-m-d H:i:s'));
        $this->assertEquals('2014-12-24 16:30:00', $periods[1][0]->format('Y-m-d H:i:s'));
        $this->assertEquals('2014-12-25 00:00:00', $periods[1][1]->format('Y-m-d H:i:s'));
    }
    
    
    public function testTimePeriodDayDayOffNotWorked()
    {
        $calendar = $this->baseCalendar(true);
        
        $arr = $calendar->getExceptionDays();
        $christmas = $arr['2014-12-25'];
        /*@var $christmas TimePeriodDay */
        
        $this->assertFalse($christmas->isWorked());
    }
    
    public function testTimePeriodDayAbsenceWorked()
    {
        $calendar = $this->baseCalendar(true);
        $arr = $calendar->getExceptionDays();
        $dec24 = $arr['2014-12-24'];
        /*@var $dec24 TimePeriodDay */
    
        $this->assertTrue($dec24->isWorked());
    }
    
    
    public function testTimePeriodDayAbsenceSchedulePeriods()
    {
        $calendar = $this->baseCalendar(true);
        $arr = $calendar->getExceptionDays();
        $dec24 = $arr['2014-12-24'];
        /*@var $dec24 TimePeriodDay */
    
        $periods = $dec24->getSchedulablePeriods();
        
        $this->assertCount(3, $periods);
        
        $morning = $periods[0];
        $beforeAbsence = $periods[1];
        $afterAbsence = $periods[2];

        $this->assertInstanceOf('\Ovidentia\LibProject\SchedulablePeriod', $morning);
        $this->assertInstanceOf('\Ovidentia\LibProject\SchedulablePeriod', $beforeAbsence);
        $this->assertInstanceOf('\Ovidentia\LibProject\SchedulablePeriod', $afterAbsence);
        
        $this->assertEquals('2014-12-24 09:00:00', $morning->fromDate->format('Y-m-d H:i:s'));
        $this->assertEquals('2014-12-24 12:00:00', $morning->toDate->format('Y-m-d H:i:s'));
        
        $this->assertEquals('2014-12-24 13:00:00', $beforeAbsence->fromDate->format('Y-m-d H:i:s'));
        $this->assertEquals('2014-12-24 15:00:00', $beforeAbsence->toDate->format('Y-m-d H:i:s'));
        
        $this->assertEquals('2014-12-24 16:30:00', $afterAbsence->fromDate->format('Y-m-d H:i:s'));
        $this->assertEquals('2014-12-24 18:00:00', $afterAbsence->toDate->format('Y-m-d H:i:s'));
    }
}
