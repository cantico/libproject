<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 *  
 */
namespace Ovidentia\LibProject;



class CalendarTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @beforeClass
     */
    public static function loadMockObjects()
    {
        require_once dirname(__FILE__).'/../mockObjects.php';
    }
    
    
    /**
     * Get a base calendar, basic tests included
     * 
     * @param bool $workingTimes       Include morking times or not
     */
    protected function baseCalendar($workingTimes)
    {
        $mock = new MockCalendar();
        return $mock->getBaseCalendar($workingTimes);

    }
    
    
    
    /**
     * calendar->save must throw Exception because project is not set
     * @expectedException \Ovidentia\LibProject\MandatoryFieldException
     */
    public function testCalendarMandatoryProject()
    {
        $storage = Storage();
        $calendar = $storage->CalendarSet()->newRecord();
        $calendar->Name = 'test';
        $calendar->save();
    }
    
    
    /**
     *
     * @expectedException \Ovidentia\LibProject\MandatoryFieldException
     */
    public function testCalendarMandatoryName()
    {
        $storage = Storage();
        $calendar = $storage->CalendarSet()->newRecord();
        $calendar->Name = '';
        $calendar->project = bab_uuid();
        $calendar->save();
    }
    
    
    
    public function testMainCalendarInstance()
    {
        $calendar = $this->baseCalendar(false);
        
        $this->assertInstanceOf('Ovidentia\LibProject\Calendar', $calendar);
    }
    
    
    public function testMainCalendarWorkingDays()
    {
        $calendar = $this->baseCalendar(false);
        
        $Iterator = $calendar->selectWeekDays();
        
        $this->assertEquals(7, $Iterator->count());
        
        $this->assertTrue($calendar->getWeekday(WeekDay::MONDAY)->isWorking());
        $this->assertTrue($calendar->getWeekday(WeekDay::TUESDAY)->isWorking());
        $this->assertTrue($calendar->getWeekday(WeekDay::WEDNESDAY)->isWorking());
        $this->assertTrue($calendar->getWeekday(WeekDay::THURSDAY)->isWorking());
        $this->assertTrue($calendar->getWeekday(WeekDay::FRIDAY)->isWorking());
        $this->assertFalse($calendar->getWeekday(WeekDay::SATURDAY)->isWorking());
        $this->assertFalse($calendar->getWeekday(WeekDay::SUNDAY)->isWorking());
    }
    
    
    
    public function testMainCalendarWithoutWorkingTimes()
    {
        $calendar = $this->baseCalendar(false);
        
        $monday = $calendar->getWeekday(WeekDay::MONDAY);
        $workingTimes = $monday->selectWorkingTimes();
        
        $this->assertEquals(0, $workingTimes->count());
    }
    
    public function testMainCalendarWithWorkingTimes()
    {
        $calendar = $this->baseCalendar(true);
        
        $monday = $calendar->getWeekday(WeekDay::MONDAY);
        $workingTimes = $monday->selectWorkingTimes();
        
        $this->assertEquals(2, $workingTimes->count());
        
        $mondayHours = (int) ($monday->getWorkingTimeDuration() / 3600);
        $this->assertEquals(8, $mondayHours);
    }
    
    
    
    
    
    public function testNonWorkingDayIgnoreWorkingTimes()
    {
        $calendar = $this->baseCalendar(false);
        
        $arr = $calendar->getExceptionDays();
        
        $this->assertTrue(isset($arr['2014-12-25']));
        $this->assertEquals(1, count($arr['2014-12-25']));
    }
    
    public function testNonWorkingDayWithWorkingTimes()
    {
        $calendar = $this->baseCalendar(true);
        
        $arr = $calendar->getExceptionDays();
        
        $this->assertTrue(isset($arr['2014-12-25']));
        $this->assertEquals(1, count($arr['2014-12-25']));
    }
    
    
    
    
    public function testWorkingTimesIterator()
    {
        $calendar = $this->baseCalendar(true);
        $iterator = new WorkingTimes($calendar, new \DateTime('2014-12-20'));
        
        $limit = 50;
        
        $iterator->rewind();
        
        while ($iterator->valid()) {
            
            $iterator->current();
            
            if ($limit <= 0) {
                break;
            }
            
            $limit--;
            
            $iterator->next();
        }
        
        $this->assertEquals(0, $limit, 'the iterator must not stop by itself');
    }

    
    
    
    public function testWorkingTimesIteratorInverted()
    {
        $calendar = $this->baseCalendar(true);
        
        $Iterator = new WorkingTimes($calendar, new \DateTime('2014-09-11 10:00:00'), true);
        // we shoud get the 2014-09-11 09:00 10:00 period on the first call
        
        foreach ($Iterator as $WTDate) {
            /* @var $WTDate WorkingTimeDate */
            break;
        }
        
        $this->assertEquals('2014-09-11 09:00:00', $WTDate->dateTime->format('Y-m-d H:i:s'));
    }
}
