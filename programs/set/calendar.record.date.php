<?php
// -------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
// -------------------------------------------------------------------------
/**
 *
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
namespace Ovidentia\LibProject;

/**
 * Methods to work on dates
 *
 */
class CalendarDate
{
    /**
     * @var Calendar
     */
    private $calendar;
    
    
    /**
     * 
     * @param Calendar $calendar
     */
    public function __construct(Calendar $calendar)
    {
        $this->calendar = $calendar;
    }
    
    
    /**
     * Add or remove working days to a date
     *
     * @param \DateTime $startDate
     * @param int $days
     *
     * @return \DateTime
     */
    public function addWorkingDays(\DateTime $startDate, $days)
    {
        $workingDays = new WorkingDays($this->calendar, $startDate, $days < 0);
        $count = 0;
    
        foreach ($workingDays as $workedDate) {
            if ($count >= abs($days)) {
                return $workedDate;
            }
    
            $count ++;
        }
    
        // if no working days set, add the days
        $startDate->add(new \DateInterval('P' . $days . 'D'));
        
        return $startDate;
    }
    
    /**
     * Add or remove seconds to a date only in working times
     *
     * @throws MissingWorkingTimes
     *
     * @param \DateTime $startDate
     * @param int $seconds
     *
     * @return \DateTime
     */
    public function addWorkingTimes(\DateTime $startDate, $seconds)
    {
        $workingTimes = new WorkingTimes($this->calendar, $startDate, $seconds < 0);
        $total_seconds = 0;
        

        foreach ($workingTimes as $workingTimeDate) {
            /*@var $workingTimeDate WorkingTimeDate */
            
            if ($seconds < 0) {
                $validPeriodDuration = $workingTimeDate->getDurationTo($startDate);
            } else {
                $validPeriodDuration = $workingTimeDate->getDurationFrom($startDate);
            }

            $total_seconds += $validPeriodDuration;
    
            $validWTStartDate = $this->adjustInPeriod(
                $workingTimeDate->dateTime,
                $total_seconds,
                $seconds,
                $validPeriodDuration
            );
    
            if (isset($validWTStartDate)) {
                return $validWTStartDate;
            }
        }
    
        throw new MissingWorkingTimes('Missing working times in calendar');
    }
    
    
    
    /**
     * Add or remove seconds to a date only in schedulables periods
     *
     * @throws MissingWorkingTimes
     *
     * @param \DateTime $startDate
     * @param int $seconds
     *
     * @return \DateTime
     */
    public function addSchedulablePeriods(\DateTime $startDate, $seconds)
    {
        $schedulablePeriods = new SchedulablePeriods($this->calendar, $startDate, $seconds < 0);
        $total_seconds = 0;
    
        foreach ($schedulablePeriods as $schedulablePeriod) {
            /*@var $schedulablePeriod SchedulablePeriod */
            $validPeriodDuration = $schedulablePeriod->getDuration();
            $total_seconds += $validPeriodDuration;

            $validWTStartDate = $this->adjustInPeriod(
                $schedulablePeriod->fromDate,
                $total_seconds,
                $seconds,
                $validPeriodDuration
            );
    
            if (isset($validWTStartDate)) {
                return $validWTStartDate;
            }
        }
    
        throw new MissingWorkingTimes('Missing working times in calendar');
    }
    
    
    /**
     *
     * @param \DateTime $periodStart
     *            start date of the last period
     * @param int $total_seconds
     *            Total seconds with the last period included
     * @param int $seconds
     *            seconds requested
     * @param int $validPeriodDuration
     *            duration of the last period
     * @return \DateTime
     */
    private function adjustInPeriod(\DateTime $periodStart, $total_seconds, $seconds, $validPeriodDuration)
    {
        if ($total_seconds >= abs($seconds)) {
            if ($seconds < 0) {
                $innerDuration = $total_seconds - abs($seconds);
            } else {
                $innerDuration = $validPeriodDuration - ($total_seconds - abs($seconds));
            }
    
            $validWTStartDate = clone $periodStart;
            if ($innerDuration > 0) {
    
                $validWTStartDate->add(new \DateInterval('PT' . $innerDuration . 'S'));
            }
    
            return $validWTStartDate;
        }
    
        return null;
    }
    
    
    /**
     * Get a period end date taking into account the calendar working days
     *
     * @throws MissingWorkingTimes
     *
     * @param \DateTime $startDate
     *            Period sart date
     * @param \DateInterval $interval
     *            Duration to add
     * @param bool $ignoreWorkingTimes
     * @return \DateTime
     */
    public function add(\DateTime $startDate, \DateInterval $interval, $ignoreWorkingTimes = false)
    {
        $startFrom = clone $startDate;
    
        // process duration over the day unit
    
        if ($interval->y) {
            $y_inter = new \DateInterval('P' . $interval->y . 'Y');
            $y_inter->invert = $interval->invert;
            $startFrom->add($y_inter);
            $interval->y = 0;
        }
    
        if ($interval->m) {
            $m_inter = new \DateInterval('P' . $interval->m . 'M');
            $m_inter->invert = $interval->invert;
            $startFrom->add($m_inter);
            $interval->m = 0;
        }
    
        // process duration in seconds
    
        $days = $interval->invert ? - 1 * $interval->d : $interval->d;
    
        $seconds = $interval->h * 3600 + $interval->i * 60 + $interval->s;
        if ($interval->invert) {
            $seconds *= - 1;
        }
    
        if ($ignoreWorkingTimes) {
            $days += floor($seconds / 86400);
            $seconds = 0;
        }
    
        $dayDate = $this->addWorkingDays($startFrom, $days);
    
        if ($seconds === 0) {
            $next = $this->nextWorked($dayDate, $interval->invert);
    
            return $next;
        }
    
        // var_dump($dayDate->format('Y-m-d H:i:s').' seconds : '.$seconds);
    
        return $this->addSchedulablePeriods($dayDate, $seconds);
    }
    
    /**
     * Get the next working period start date, return the same date if the date is in a working period
     *
     * @param \DateTime $startDate
     * @param integer $invert
     *
     * @return \DateTime
     */
    protected function nextWorked(\DateTime $startDate, $invert)
    {
        $schedulablePeriods = new SchedulablePeriods($this->calendar, $startDate, (bool) $invert);
        foreach ($schedulablePeriods as $nextPeriod) {
            /*@var $nextPeriod SchedulablePeriod */
    
            if ($invert) {
                return $nextPeriod->toDate;
            } else {
    
                return $nextPeriod->fromDate;
            }
        }
    
        return $startDate;
    }
}
