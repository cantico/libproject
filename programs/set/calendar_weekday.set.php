<?php
// -------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
// -------------------------------------------------------------------------
/**
 *
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
namespace Ovidentia\LibProject;

/**
 * List of week days in one calendar and group of exception periods
 * Fields are named according to the MSPDI format
 *
 * @property \ORM_PkField $id
 * @property \ORM_IntField $DayType type of day. Values are:
 *           0=Exception, 1=Monday, 2=Tuesday, 3=Wednesday,
 *           4=Thursday, 5=Friday, 6=Saturday, 7=Sunday
 * @property \ORM_BoolField $DayWorking
 * @property \Ovidentia\LibProject\CalendarSet $calendar
 * @method \Ovidentia\LibProject\WeekDay newRecord()
 */
class WeekDaySet extends RecordSet
{

    /**
     * Field initialization
     */
    public function __construct()
    {
        parent::__construct();
        
        $this->setPrimaryKey('id');
        
        $this->addFields(
            ORM_EnumField('DayType', $this->getDayTypes()), // used only for workperiod declarations
            ORM_BoolField('DayWorking')->setDescription('0: unavailability period, 1: worked period')
        );
        
        $this->hasOne('calendar', '\Ovidentia\LibProject\CalendarSet'); // parent in XML
        $this->hasMany('WorkingTimes', '\Ovidentia\LibProject\WorkingTimeSet', 'weekday');
    }

    /**
     * Day types
     * @return array
     */
    public function getDayTypes()
    {
        return array(
            WeekDay::EXCEPTION => translate('Day off'), // the weekday should contain TimePeriod records
            WeekDay::MONDAY => translate('Monday'),
            WeekDay::TUESDAY => translate('Tuesday'),
            WeekDay::WEDNESDAY => translate('Wednesday'),
            WeekDay::THURSDAY => translate('Thursday'),
            WeekDay::FRIDAY => translate('Friday'),
            WeekDay::SATURDAY => translate('Saturday'),
            WeekDay::SUNDAY => translate('Sunday')
        );
    }

    /**
     * (non-PHPdoc)
     * @see ORM_RecordSet::delete()
     */
    public function delete(\ORM_Criteria $oCriteria = null)
    {
        $res = $this->select($oCriteria);
        
        foreach ($res as $weekday) {
            $weekday->deleteWorkingTimes();
            $weekday->deleteTimePeriods();
        }
        
        return parent::delete($oCriteria);
    }
}
