<?php
// -------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
// -------------------------------------------------------------------------
/**
 *
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
namespace Ovidentia\LibProject;

/**
 * Resource, personal or material
 * Fields are named according to the MSPDI format
 *
 * @property \ORM_PkField $UID
 * @property \ORM_IntField $ID
 * @property \ORM_StringField $Name
 * @property \ORM_EnumField $Type
 * @property \ORM_BoolField $IsNull
 * @property \ORM_StringField $Initials
 * @property \ORM_StringField $MaterialLabel
 * @property \ORM_StringField $Group
 * @property \ORM_EnumField $WorkGroup
 * @property \ORM_StringField $EmailAddress
 * @property \ORM_TextField $Notes
 * @property \ORM_DatetimeField $CreationDate
 * @property \ORM_UserField $user
 * @property ProjectSet $project
 * @property CalendarSet $calendar
 * 
 * @method Resource get()
 * @method Resource newRecord()
 */
class ResourceSet extends RecordSet
{
    
    /**
     * field initialization
     */
    public function __construct()
    {
        parent::__construct();
        
        $this->addFields(
            ORM_PkField('UID')->setAutoIncrement(false),
            ORM_IntField('ID')
                ->index()
                ->setDescription('The position identifier of the resource within the list of resources.'),
            ORM_StringField('Name'),
            ORM_EnumField('Type', $this->getTypes()),
            ORM_BoolField('IsNull'),
            ORM_StringField('Initials'),
            ORM_StringField('MaterialLabel')
                ->setDescription('The unit of measure for the material resource.'),
            ORM_StringField('Group')
                ->setDescription('The group to which the resource belongs.'),
            ORM_EnumField('WorkGroup', $this->getWorkGroups())
                ->setDescription('The type of workgroup to which the resource belongs.'),
            ORM_StringField('EmailAddress'),
            ORM_TextField('Notes'),
            ORM_DatetimeField('CreationDate'),
            ORM_UserField('user')->index()
        );
        
        $this->setPrimaryKey('UID');
        $this->hasOne('project', '\Ovidentia\LibProject\ProjectSet', 'ORM_StringField')->index();
        $this->hasOne('CalendarUID', '\Ovidentia\LibProject\CalendarSet')->index();
    }

    /**
     * Resource types
     * @return array
     */
    public function getTypes()
    {
        return array(
            Resource::MATERIAL => translate('Material'),
            Resource::WORK => translate('Work')
        );
    }

    /**
     * resource workgroups
     * @return array
     */
    public function getWorkGroups()
    {
        return array(
            Resource::WG_DEFAULT => translate('Default'),
            Resource::WG_NONE => translate('None'),
            Resource::WG_EMAIL => translate('Email'),
            Resource::WG_WEB => translate('Web')
        );
    }

    /**
     * verify mandatory fields
     * @return bool
     */
    public function save(Resource $record)
    {
        if (empty($record->Name)) {
            throw new MandatoryFieldException('Name is mandatory');
        }
        
        if (empty($record->project)) {
            throw new MandatoryFieldException('project is mandatory');
        }
        
        return parent::save($record);
    }
}
