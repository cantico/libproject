<?php
// -------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
// -------------------------------------------------------------------------
/**
 *
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
namespace Ovidentia\LibProject;

/**
 * Link beetween 2 tasks
 * 
 *
 * @property int $id
 * @property int $Type
 * @property int $LinkLag
 * @property \Ovidentia\LibProject\Task $task
 * @property \Ovidentia\LibProject\Task $PredecessorUID
 */
class PredecessorLink extends Record
{

    /**
     * Finish-to-finish
     * 
     * The dependent task cannot be completed until the task 
     * that it depends on  is completed. 
     * The dependent task can be completed anytime after the 
     * task that it depends on is completed. The FF link type 
     * does not require that both tasks be completed simultaneously.
     * 
     * For example, if you have two tasks, "Add wiring" and 
     * "Inspect electrical," the "Inspect electrical" task cannot 
     * be completed until the "Add wiring" task is completed.
     */
    const FF = 0;

    
    /**
     * Finish-to-start
     * 
     * The dependent task cannot begin until the task that it 
     * depends on is complete. 
     * For example, if you have two tasks, "Dig foundation" 
     * and "Pour concrete," the "Pour concrete" task cannot 
     * begin until the "Dig foundation" task is complete.
     */
    const FS = 1;

    
    /**
     * Start-to-finish
     * 
     * The dependent task cannot be completed until the task 
     * that it depends on begins.
     * 
     * The dependent task can be completed anytime after the 
     * task that it depends on begins. The SF link type does not 
     * require that the dependent task be completed concurrent 
     * with the beginning of the task on which it depends.
     * 
     * For example, the roof trusses for your construction project 
     * are built offsite. Two of the tasks in your project are 
     * "Truss delivery" and "Assemble roof." The "Assemble roof" 
     * task cannot be completed until the "Truss delivery" 
     * task begins.
     */
    const SF = 2;

    
    /**
     * Start-to-start
     * 
     * The dependent task cannot begin until the task that 
     * it depends on begins.
     * 
     * The dependent task can begin anytime after the task 
     * that it depends on begins. The SS link type does not require 
     * that both tasks begin simultaneously.
     * 
     * For example, if you have two tasks, "Pour concrete" and 
     * "Level concrete," the "Level concrete" task cannot 
     * begin until the "Pour concrete" task begins.
     */
    const SS = 3;

    
    /**
     * Get predecessor actual dates boundaries
     * according to the sucessor dates boundaries AND the 
     * link type
     * 
     * @return PeriodBoundaries
     */
    public function getPredecessorBoundaries()
    {
        $boundaries = $this->task->getDatesBoundaries();
        
        //TODO
    }
    
    /**
     * Get sucessor actual dates boundaries
     * according to the prodecessor dates boundaries AND the
     * link type
     *
     * @return PeriodBoundaries
     */
    public function getSucessorBoundaries()
    {
        $boundaries = $this->PredecessorUID->getDatesBoundaries();
        
        //TODO
    }
    
    
    /**
     * Receive the parent record
     * use this method to update links in record
     */
    public function updateParentByXml(Record $parent)
    {
        $this->task = $parent->UID;
    }
}
