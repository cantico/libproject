<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
namespace Ovidentia\LibProject;


class ProjectTest extends \PHPUnit_Framework_TestCase
{
    
    /**
     * @beforeClass
     */
    public static function loadMockObjects()
    {
        require_once dirname(__FILE__).'/../mockObjects.php';
    }
    
    

    public function testSetCreation()
    {
        $storage = Storage();
        
        
        $this->assertInstanceOf('Ovidentia\LibProject\AssignmentSet', $storage->AssignmentSet());
        $this->assertInstanceOf('Ovidentia\LibProject\AvailabilityPeriodSet', $storage->AvailabilityPeriodSet());
        $this->assertInstanceOf('Ovidentia\LibProject\CalendarSet', $storage->CalendarSet());
        $this->assertInstanceOf('Ovidentia\LibProject\PredecessorLinkSet', $storage->PredecessorLinkSet());
        $this->assertInstanceOf('Ovidentia\LibProject\ProjectSet', $storage->ProjectSet());
        $this->assertInstanceOf('Ovidentia\LibProject\ResourceSet', $storage->ResourceSet());
        $this->assertInstanceOf('Ovidentia\LibProject\TaskSet', $storage->TaskSet());
        $this->assertInstanceOf('Ovidentia\LibProject\TimePeriodSet', $storage->TimePeriodSet());
        $this->assertInstanceOf('Ovidentia\LibProject\WeekDaySet', $storage->WeekDaySet());
        $this->assertInstanceOf('Ovidentia\LibProject\WorkingTimeSet', $storage->WorkingTimeSet());
    }

    
    public function testProjectDates()
    {
        $storage = Storage();
        $project = $storage->ProjectSet()->newRecord();
        $project->Title = 'Test project';
        $project->save();
        
        $this->assertRegExp('/\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2}/', (string) $project->CreationDate);
        $this->assertRegExp('/\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2}/', (string) $project->LastSaved);
    }
    
    public function testProjectRevision()
    {
        $storage = Storage();
        $project = $storage->ProjectSet()->newRecord();
        $project->Title = 'Test project';
        $project->Revision = 1;
        $project->save();
    
        $this->assertEquals($project->Revision, '2');
    }
    
    
    public function testProjectUid()
    {
        $storage = Storage();
        $project = $storage->ProjectSet()->newRecord();
        $project->Title = 'Test project';
        $project->save();
    
        $this->assertStringMatchesFormat('%s', (string) $project->uuid);
    }
    
    
    /**
     * very that an untitled project is not allowed
     * @expectedException \Ovidentia\LibProject\MandatoryFieldException
     */
    public function testProjectMandatoryTitle()
    {
        $storage = Storage();
        $project = $storage->ProjectSet()->newRecord();
        $project->save();
    }
    
    
    
    public function testAssigmentDates()
    {
        $storage = Storage();
        $assigment = $storage->AssignmentSet()->newRecord();
        $assigment->project = bab_uuid();
        $assigment->save();
        
        $this->assertRegExp('/\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2}/', $assigment->CreationDate);
    }
    
    
    public function testProjectStartDateModificationOnFrozenTask()
    {
        $mock = new MockProject();
        $project = $mock->getProjectWithTaskConstraint(Task::ASAP);
        
        foreach ($project->selectActiveTasks() as $task) {
            /*@var $task Task */
            $this->assertTrue($task->isFrozen());
            $this->assertEquals($task->ActualStart, '2014-09-01 00:00:00');
        }
        
        $project->StartDate = '2014-09-02 00:00:00';
        $project->updateTasks();
        
        foreach ($project->selectActiveTasks() as $task) {
            /*@var $task Task */
            $this->assertEquals(
                $task->ActualStart,
                '2014-09-01 00:00:00',
                'a frozen task should not be modified automatically'
            );
        }
    }
    
    
    /**
     * @return array
     */
    private function getFutureTaskProject($constraintType, $constraintDate = '0000-00-00 00:00:00')
    {
        $mock = new MockProject();
        $project = $mock->getProjectWithTaskConstraint($constraintType, $constraintDate);
        
        $year = 1+date('Y');
        
        $project->StartDate = $year.'-09-01 00:00:00';
        $project->FinishDate = $year.'-09-14 00:00:00';
        
        foreach ($project->selectActiveTasks() as $task) {
            /*@var $task Task */
        
            $task->ActualStart = $year.'-09-01 00:00:00';
            $task->ActualFinish = $year.'-09-14 00:00:00';
        
            $this->assertFalse($task->isFrozen());
            $this->assertEquals($task->ActualStart, $year.'-09-01 00:00:00');
        }
        
        return array($project, $task);
    }
    
    
    public function testProjectStartDateModificationOnFutureTaskASAP()
    {
        list($project, $task) = $this->getFutureTaskProject(Task::ASAP);
    
        $date = new \DateTime($project->StartDate);
        $date->add(new \DateInterval('P1D'));
        
        $project->StartDate = $date->format('Y-m-d H:i:s');
        $project->updateTasks();

        $this->assertEquals($task->ActualStart, $project->StartDate, 'a future task should be modified automatically');
    }
    
    /*
    public function testProjectStartDateModificationOnFutureTaskALAP()
    {
        list($project, $task) = $this->getFutureTaskProject(Task::ALAP);
        $date = new \DateTime($project->StartDate);
        $date->add(new \DateInterval('P1D'));
    
        $project->StartDate = $date->format('Y-m-d H:i:s');
        $project->updateTasks();
    
        $this->assertEquals(
            $task->ActualFinish,
            $project->FinishDate,
            'a future task should be modified automatically'
        );
    }
    */
    
    
    /**
     * 
     */
    public function testConstraintOnSave()
    {
        $mock = new MockProject();
        $test = false;
        try {
            $project = $mock->getProjectWithTaskConstraint(Task::START_NO_EARLIER_THAN, '2014-09-02 00:00:00');
        } catch (TaskConstraintException $exception) {
            $test = true;
        }
        
        $this->assertTrue($test);
    }
    
    public function testProjectStartDateModificationOnFutureTaskSNET()
    {
        $year = 1+date('Y');
        
        list($project, $task) = $this->getFutureTaskProject(Task::ASAP);
        /*@var $project Project */
        /*@var $task Task */
        
        $task->ConstraintType = Task::START_NO_EARLIER_THAN;
        $task->ConstraintDate = $year.'-09-01 00:00:00';
    
        $date = new \DateTime($project->StartDate);
        $date->sub(new \DateInterval('P1D'));
    
        $project->StartDate = $date->format('Y-m-d H:i:s');
        $project->updateTasks();
    
        $this->assertEquals(
            $task->ActualStart,
            $year.'-09-01 00:00:00',
            'one day after the project start because of the constraint'
        );
    }
}
