<?php 



class bab_functionality
{
    public static function get($path) {
        return false;
    }
}

class bab_Session {
    function getId()
    {
        return '0000000000000000000';
    }
}



/**
 * Returns a singleton of the specified class.
 *
 * @param string $classname
 * @return object
 */
function bab_getInstance($classname) {
    static $instances = NULL;
    if (is_null($instances)) {
        $instances = array();
    }
    if (!array_key_exists($classname, $instances)) {
        $instances[$classname] = new $classname();
    }

    return $instances[$classname];
}



/**
 * Generate a pseudo-random UUID according to RFC 4122
 *
 * @return string the new UUID
 */
function bab_uuid()
{
    return sprintf( '%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
        mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0xffff),
        mt_rand(0, 0x0fff) | 0x4000,
        mt_rand(0, 0x3fff) | 0x8000,
        mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0xffff)
    );
}
