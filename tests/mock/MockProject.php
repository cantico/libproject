<?php 

namespace Ovidentia\LibProject;


class MockProject
{
    
    /**
     * 
     * @var \Ovidentia\LibProject\ProjectSet
     */
    protected $projectSet;
    
    /**
     * 
     * @var \Ovidentia\LibProject\TaskSet
     */
    protected $taskSet;
    
    public function __construct()
    {
        $storage = Storage();
        $this->projectSet = $storage->ProjectSet();
        $this->taskSet = $storage->TaskSet();
    }
    
    public function getProject()
    {
        $project = $this->projectSet->newRecord();
        
        $project->uuid = bab_uuid();
        $project->Author = 'Author name';
        $project->CreationDate = date('Y-m-d H:i:s');
        $project->Name = 'Test project';
        $project->Title = 'This is a mock project';
        
        $project->taskSet($this->taskSet);
        
        return $project;
    }
    
    /**
     * Set the list of tasks to return for one project
     * task must be actives and unplanned
     */
    protected function setProjectTasks(Project $project, Array $tasks)
    {
        $backend = $this->projectSet->getBackend();
    
        // selectTasks()
        $backend->setSelectReturn(
            $this->taskSet,
            $this->taskSet->project->is($project->uuid),
            $tasks
        );
    
        // selectActiveTasks()
        $backend->setSelectReturn(
            $this->taskSet,
            $this->taskSet->project->is($project->uuid)->_AND_($this->taskSet->Active->is(1)),
            $tasks
        );
        
        // selectUnplannedTasks()
        $backend->setSelectReturn(
            $this->taskSet,
            $this->taskSet->project->is($project->uuid)->_AND_(
                $this->taskSet->Finish->is('0000-00-00 00:00:00')->_OR_($this->taskSet->Finish->is(null))
            )->_AND_(
                $this->taskSet->Start->is('0000-00-00 00:00:00')->_OR_($this->taskSet->Start->is(null))
            ),
            $tasks
        );
    }
    
    
    /**
     * Get a project with two active tasks
     * @return Project
     */
    public function getTwoRelatedTasks()
    {
        $project = $this->getProject();
        
        
        $arr = MockTwoRelatedTasks();
        
        foreach ($arr as $task) {
            $task->project = $project->uuid;
        }
        
        $this->setProjectTasks($project, $arr);

        
        return $project;
    }
    
    
    /**
     * Get a project with one task
     * the task will contain a constraint given as parameter
     * @param int $type Constraint type    ex: Task::START_NO_EARLIER_THAN
     * @param string $date Constraint date
     *
     * @return Project
     */
    public function getProjectWithTaskConstraint($type, $date = '0000-00-00 00:00:00')
    {
        $project = $this->getProject();
        $project->Start = '2014-09-01 00:00:00';
        $project->Finish = '2014-09-17 00:00:00';
        $project->ScheduleFromStart = 1;
        
        $arr = MockTwoRelatedTasks();
        
        $task = $this->taskSet->newRecord();
        $task->Name = 'Mock task';
        $task->Type = Task::FIXED_UNITS;
        $task->ActualStart = '2014-09-01 00:00:00';
        $task->ActualFinish = '2014-09-14 00:00:00';
        $task->Duration = 'P14D';
        $task->Work = 'P11D';
        $task->RegularWork = 'P10D';
        $task->ActualOvertimeWork = 'P1D';
        $task->ActualDuration = 'P14D';
        $task->RemainingDuration = 'P14D';
        $task->PercentComplete = '0';
        $task->RemainingWork = 'P11D';
        $task->project = bab_uuid();
        $task->Active = 1;
        $task->ConstraintType = $type;
        $task->ConstraintDate = $date;
        $task->project = $project->uuid;
        $task->save(); // create the UID

        
        $this->setProjectTasks($project, array($task));
        
        return $project;
    }
}
