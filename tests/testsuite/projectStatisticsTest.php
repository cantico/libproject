<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
namespace Ovidentia\LibProject;


class ProjectStatisticsTest extends \PHPUnit_Framework_TestCase
{
    
    /**
     * @beforeClass
     */
    public static function loadMockObjects()
    {
        require_once dirname(__FILE__).'/../mockObjects.php';
    }
    
    /**
     * Get a project with two related tasks
     * @return \Ovidentia\LibProject\Project
     */
    protected function getProject()
    {
        $mock = new MockProject();
        return $mock->getTwoRelatedTasks();
    }
    
    
    public function testProjectInstance()
    {
        $project = $this->getProject();
        $this->assertInstanceOf('Ovidentia\LibProject\Project', $project);
    }
    
    
    public function testProjectStatisticsInstance()
    {
        $statistics = $this->getProject()->statistics();
        $this->assertInstanceOf('Ovidentia\LibProject\ProjectStatistics', $statistics);
    }
    
    public function testNumberOfTasks()
    {
        $project = $this->getProject();
        $tasks = $project->selectActiveTasks();
    
        $this->assertEquals(2, $tasks->count());
    }
    
    /**
     * Get formated interval
     * @param string $method
     * @return string
     */
    protected function interval($method)
    {
        $statistics = $this->getProject()->statistics();
        $duration = $statistics->$method();
        
        $this->assertInstanceOf('\DateInterval', $duration, 'Return a DateInterval');
        
        return $duration->format('%r%d days %h hours');
    }
    
    
    public function testActualDuration()
    {
        $this->assertEquals('2 days 0 hours', $this->interval('getActualDuration'));
    }
    
    public function testDuration()
    {
        $this->assertEquals('2 days 4 hours', $this->interval('getDuration'));
    }
    
    public function testWork()
    {
        $this->assertEquals('0 days 20 hours', $this->interval('getWork'));
    }
    
    public function testActualOvertimeWork()
    {
        $this->assertEquals('0 days 4 hours', $this->interval('getActualOvertimeWork'));
    }
    
    public function testOvertimeWork()
    {
        $this->assertEquals('0 days 4 hours', $this->interval('getOvertimeWork'));
    }
    
    public function testRemainingDuration()
    {
        $this->assertEquals('0 days 7 hours', $this->interval('getRemainingDuration'));
    }
    
    public function testRemainingWork()
    {
        $this->assertEquals('0 days 8 hours', $this->interval('getRemainingWork'));
    }
}
