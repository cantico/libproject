<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
namespace Ovidentia\LibProject;



/**
 * Working time and date
 * Object given by each iteration of the WorkingTimes iterator
 */
class WorkingTimeDate
{
    /**
     * Start date of the working time period
     * @var \DateTime
     */
    public $dateTime;

    /**
     *
     * @var WorkingTime
     */
    public $workingTime;
    
    /**
     * 
     * @var \DateTime
     */
    private $fromDate;
    
    /**
     * 
     * @var \DateTime
     */
    private $toDate;


    /**
     * @return \DateTime
     */
    public function getFromDate()
    {
        if (!isset($this->fromDate)) {
            $this->fromDate = clone $this->dateTime;
            list($hour, $min, $sec) = explode(':', $this->workingTime->FromTime);
            $this->fromDate->setTime($hour, $min, $sec);
        }
        return $this->fromDate;
    }

    /**
     * @return \DateTime
     */
    public function getToDate()
    {
        if (!isset($this->toDate)) {
            $this->toDate = clone $this->dateTime;
            list($hour, $min, $sec) = explode(':', $this->workingTime->ToTime);
            $this->toDate->setTime($hour, $min, $sec);
        }
        return $this->toDate;
    }

    /**
     * @return bool
     */
    public function isOverlapedBy(\DateTime $start, \DateTime $end)
    {
        return ($this->getFromDate() <= $end && $this->getToDate() >= $start);
    }
    
    /**
     * Get intersection period if overlaped
     * @return array
     */
    public function getIntersection(\DateTime $start, \DateTime $end)
    {
        if (!$this->isOverlapedBy($start, $end)) {
            return null;
        }
        
        $intersectStart = $start > $this->getFromDate() ? $start : $this->getFromDate();
        $intersectEnd = $end < $this->getToDate() ? $end : $this->getToDate();
        
        return array($intersectStart, $intersectEnd);
    }
    
    /**
     * This method return the duration in seconds of the 
     * workingtime periods with only the time
     * after the startdate parameter
     * 
     * @param    \DateTime $startDate
     * 
     * @return int
     */
    public function getDurationFrom(\DateTime $startDate)
    {
        if ($this->getFromDate() >= $startDate) {
            return $this->workingTime->getDuration();
        }
        
        $duration = ($this->getToDate()->getTimestamp() - $startDate->getTimestamp());
        
        if ($duration < 0) {
            $duration = 0;
        }
        
        return $duration;
    }
    
    
    
    /**
     * This method return the duration in seconds of the
     * workingtime periods with only the time
     * form the todate parameter
     *
     * @param    \DateTime $toDate
     *
     * @return int
     */
    public function getDurationTo(\DateTime $toDate)
    {
        if ($this->getToDate() <= $toDate) {
            return $this->workingTime->getDuration();
        }
    
        $duration = ($toDate->getTimestamp() - $this->getFromDate()->getTimestamp());
    
        if ($duration < 0) {
            $duration = 0;
        }
        
        return $duration;
    }
}
