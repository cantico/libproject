; <?php
// @codingStandardsIgnoreStart
/*
[general]
name="LibProject"
version=0.1
addon_type="LIBRARY" 
mysql_character_set_database="latin1,utf8"
encoding="UTF-8"
description="Projects library. Storage for projects, tasks, resources. Export in various format"
description.fr="Librairie des projets. Stockage des projets, tâches, ressources. Export dans différents formats"
delete=1
longdesc=""
ov_version="7.3.0"
php_version="5.3.0"
addon_access_control=0
author="Cantico ( support@cantico.fr )"
icon="icon.png" 

[addons]
LibOrm=">=0.9.1"

;*/
// @codingStandardsIgnoreStart
?>
