<?php
// -------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
// -------------------------------------------------------------------------
/**
 *
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
namespace Ovidentia\LibProject;

/**
 *
 * @property int $id
 * @property string $FromTime
 * @property string $ToTime
 * @property \Ovidentia\LibProject\WeekDay $weekday
 */
class WorkingTime extends Record
{

    /**
     * convert a time in hours:minuntes:seconds to seconds
     * 
     * @param string $time
     *            ex : 11:11:11
     * @return int seconds since day start
     */
    public function timeToSeconds($time)
    {
        list ($hours, $minutes, $seconds) = explode(':', $time);
        
        $hours = (int) $hours;
        $minutes = (int) $minutes;
        $seconds = (int) $seconds;
        
        return ($seconds + 60 * $minutes + 3600 * $hours);
    }

    /**
     * Get duration of working time in seconds
     * 
     * @return int
     */
    public function getDuration()
    {
        if ('00:00:00' === $this->ToTime || $this->ToTime <= $this->FromTime) {
            return null;
        }
        
        return ($this->timeToSeconds($this->ToTime) - $this->timeToSeconds($this->FromTime));
    }

    /**
     * Receive the parent record
     * use this method to update links in record
     */
    public function updateParentByXml(Record $parent)
    {
        $this->weekday = $parent->id;
    }
}
