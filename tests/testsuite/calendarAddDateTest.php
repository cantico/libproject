<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 *  
 */
namespace Ovidentia\LibProject;



class CalendarAddDateTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @beforeClass
     */
    public static function loadMockObjects()
    {
        require_once dirname(__FILE__).'/../mockObjects.php';
    }
    
    
    /**
     * Get a base calendar, basic tests included
     *
     * @param bool $workingTimes       Include working times or not
     */
    protected function baseCalendar($workingTimes)
    {
        $mock = new MockCalendar();
        return $mock->getBaseCalendar($workingTimes);
    
    }
    
    
    /**
     * @expectedException \Ovidentia\LibProject\MissingWorkingTimes
     */
    public function testDateAddSecondsWithoutWorkingTimes()
    {
        $calendar = $this->baseCalendar(false);
        $set = $calendar->getParentSet();
    
        $monday = new \DateTime('2014-08-04');
        $calendar->date()->add($monday, $set->getInterval('PT3600S'))->format('Y-m-d');
    
    }
    
    
    public function testDateAddTimeIgnoreWorkingTimes()
    {
        $calendar = $this->baseCalendar(false);
        $set = $calendar->getParentSet();
    
        $monday = new \DateTime('2014-08-04');
        $add3600s = $calendar->date()->add($monday, $set->getInterval('PT3600S'), true)->format('Y-m-d');
        $add24h = $calendar->date()->add($monday, $set->getInterval('PT24H'), true)->format('Y-m-d');
    
        $this->assertEquals('2014-08-04', $add3600s);
        $this->assertEquals('2014-08-05', $add24h);
    }
    
    
    public function testDateAddWorkingDays()
    {
        $calendar = $this->baseCalendar(false);
        $set = $calendar->getParentSet();
    
        $monday = new \DateTime('2014-08-04');
        $add4d = $calendar->date()->add($monday, $set->getInterval('P4D'))->format('Y-m-d');
        $add5d = $calendar->date()->add($monday, $set->getInterval('P5D'))->format('Y-m-d');
        $add1w = $calendar->date()->add($monday, $set->getInterval('P1W'))->format('Y-m-d');
        $add1m = $calendar->date()->add($monday, $set->getInterval('P1M'))->format('Y-m-d');
        $add1m2d = $calendar->date()->add($monday, $set->getInterval('P1M2D'))->format('Y-m-d');
    
    
        $this->assertEquals('2014-08-08', $add4d);
        $this->assertEquals('2014-08-11', $add5d);
    
        // This will not work as expected because weeks are not stored in intervals, 1week = 7days
        $this->assertEquals('2014-08-13', $add1w);
    
        $this->assertEquals('2014-09-04', $add1m);
        $this->assertEquals('2014-09-08', $add1m2d);
    }
    
    
    public function testDateAddWorkingTimesHoursCompletePeriods()
    {
        $calendar = $this->baseCalendar(true);
        $set = $calendar->getParentSet();
    
        $monday = new \DateTime('2014-08-04 09:00:00');
        $add11h = $calendar->date()->add($monday, $set->getInterval('PT11H'))->format('Y-m-d H:i:s');
        $add16h = $calendar->date()->add($monday, $set->getInterval('PT16H'))->format('Y-m-d H:i:s');
    
        $this->assertEquals('2014-08-05 12:00:00', $add11h);
        $this->assertEquals('2014-08-05 18:00:00', $add16h);
    }
    
    
    public function testDateAddWorkingTimesHoursPartialPeriods()
    {
        $calendar = $this->baseCalendar(true);
        $set = $calendar->getParentSet();
    
        $monday = new \DateTime('2014-08-04 09:00:00');
        $add1h = $calendar->date()->add($monday, $set->getInterval('PT1H'))->format('Y-m-d H:i:s');
        $add17h = $calendar->date()->add($monday, $set->getInterval('PT17H'))->format('Y-m-d H:i:s');
    
        $this->assertEquals('2014-08-04 10:00:00', $add1h);
        $this->assertEquals('2014-08-06 10:00:00', $add17h);
    }
    
    
    public function testDateAddWorkingTimesMinutesCompletePeriods()
    {
        $calendar = $this->baseCalendar(true);
        $set = $calendar->getParentSet();
    
        $monday = new \DateTime('2014-08-04 09:00:00');
        $add3h = $calendar->date()->add($monday, $set->getInterval('PT180M'))->format('Y-m-d H:i:s');
        $add16h = $calendar->date()->add($monday, $set->getInterval('PT960M'))->format('Y-m-d H:i:s');
    
        $this->assertEquals('2014-08-04 12:00:00', $add3h);
        $this->assertEquals('2014-08-05 18:00:00', $add16h);
    }
    
    
    
    public function testDateAddWorkingTimesTimePartialPeriods()
    {
        $calendar = $this->baseCalendar(true);
        $set = $calendar->getParentSet();
    
        $monday = new \DateTime('2014-08-04 09:00:00');
        $addtest1 = $calendar->date()->add($monday, $set->getInterval('PT1H15M3S'))->format('Y-m-d H:i:s');
        $addtest2 = $calendar->date()->add($monday, $set->getInterval('PT2H61M61S'))->format('Y-m-d H:i:s');
    
        $this->assertEquals('2014-08-04 10:15:03', $addtest1);
        $this->assertEquals('2014-08-04 13:02:01', $addtest2);
    }
    
    
    
    public function testAddDateToChangeDay()
    {
        $calendar = $this->baseCalendar(true);
        $set = $calendar->getParentSet();
    
        $add2hours = $calendar->date()->add(new \DateTime('2014-09-10 17:00:00'), $set->getInterval('PT2H'));
        $this->assertEquals('2014-09-11 10:00:00', $add2hours->format('Y-m-d H:i:s'));
    }
    
    
    
    
    public function testAddDateOnDayOff()
    {
        $calendar = $this->baseCalendar(true);
        $set = $calendar->getParentSet();
    
        $add1day = $calendar->date()->add(new \DateTime('2014-12-24 17:00:00'), $set->getInterval('P1D'));
        $add2hours = $calendar->date()->add(new \DateTime('2014-12-24 17:00:00'), $set->getInterval('PT2H'));
    
        $this->assertEquals('2014-12-26 17:00:00', $add1day->format('Y-m-d H:i:s'));
        $this->assertEquals('2014-12-26 10:00:00', $add2hours->format('Y-m-d H:i:s'));
    }
    
    public function testAddDateOnExceptionPeriod()
    {
        $calendar = $this->baseCalendar(true);
        $set = $calendar->getParentSet();
    
        $add1day = $calendar->date()->add(new \DateTime('2014-12-23 17:00:00'), $set->getInterval('P1D'));
        $add1hour = $calendar->date()->add(new \DateTime('2014-12-24 14:30:00'), $set->getInterval('PT1H'));
    
        $this->assertEquals('2014-12-24 17:00:00', $add1day->format('Y-m-d H:i:s'));
        $this->assertEquals('2014-12-24 17:00:00', $add1hour->format('Y-m-d H:i:s'));
    }
    
    
    public function testAddDateWithDayTargetException()
    {
        $calendar = $this->baseCalendar(true);
        $set = $calendar->getParentSet();
    
        $add1day = $calendar->date()->add(new \DateTime('2014-12-23 15:30:00'), $set->getInterval('P1D'));
    
        $this->assertEquals('2014-12-24 16:30:00', $add1day->format('Y-m-d H:i:s'));
    }
    
    
    public function testAddWorkingTimesOnDayOff()
    {
        $calendar = $this->baseCalendar(true);
        
        $add1hour = $calendar->date()->addWorkingTimes(new \DateTime('2014-12-24 17:30:00'), 3600);
        
        $this->assertEquals(
            '2014-12-25 09:30:00',
            $add1hour->format('Y-m-d H:i:s'),
            'the addWorkingTimes ignore exception periods'
        );
    }
}
