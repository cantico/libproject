<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */

namespace Ovidentia\LibProject;









/**
 *
 * Fields are named according to the MSPDI format
 *
 * @property \ORM_PkField        	$UID
 * @property \ORM_IntField			$PercentWorkComplete
 * @property \ORM_DateTimeField		$ActualFinish
 * @property \ORM_DateTimeField		$ActualStart
 * @property \ORM_TextField			$Notes
 * @property \ORM_BoolField			$ResponsePending
 * @property \ORM_DateTimeField		$Start
 * @property \ORM_DateTimeField		$Finish
 * @property \ORM_DateTimeField		$Stop
 * @property \ORM_DateTimeField		$Resume
 * @property \ORM_DecimalField		$Units
 * @property \ORM_BoolField			$UpdateNeeded
 * @property \ORM_StringField		$Work                        scheduled work for resource
 * @property \ORM_StringField		$ActualWork                  the amount of work that has already been done by resource
 * @property \ORM_StringField		$ActualOvertimeWork          the actual amount of overtime work 
 *                                                               already performed by resource
 * @property \ORM_StringField		$RemainingWork
 * @property \ORM_StringField		$RemainingOvertimeWork
 * @property \ORM_StringField		$RegularWork
 * @property \ORM_StringField		$OvertimeWork
 * @property \ORM_EnumField			$BookingType
 * @property \ORM_DateTime			$CreationDate
 * @property TaskSet		        $TaskUID
 * @property ResourceSet	        $ResourceUID
 * @property ProjectSet	            $project
 * @method TaskUID()
 * @method ResourceUID()
 * @method project()
 * @method Assignment newRecord()
 */
class AssignmentSet extends RecordSet
{
    
    /**
     * Fields initialization
     */
    public function __construct()
    {
        parent::__construct();



        $this->addFields(
            ORM_PkField('UID')->setAutoIncrement(false),
            ORM_IntField('PercentWorkComplete'),
            ORM_DateTimeField('ActualFinish'),
            ORM_DateTimeField('ActualStart'),
            ORM_TextField('Notes'),
            ORM_BoolField('ResponsePending')
                ->setOutputOptions(translate('No'), translate('Yes'))
                ->setDescription('True if a response has not been received for a TeamAssign message.'),
            ORM_DateTimeField('Start')
                ->setDescription('The scheduled start date of the assignment.'),
            ORM_DateTimeField('Finish')
                ->setDescription('The scheduled finish date of the assignment.'),
            ORM_DateTimeField('Stop')
                ->setDescription('The date that the assignment was stopped.'),
            ORM_DateTimeField('Resume')
                ->setDescription('The date that the assignment resumed.'),
            ORM_DecimalField('Units', 2)
                ->setDescription('The number of units for the assignment.'),
            ORM_BoolField('UpdateNeeded')
            ->setDescription(
                'True if the resource assigned to a task needs to be updated as to the status of the task.'
            ),
            ORM_StringField('Work')			              // ex: PT112H0M0S
            ->setDescription('The amount of scheduled work for the assignment.'),
            ORM_StringField('ActualWork')			              // ex: PT112H0M0S
            ->setDescription('The actual amount of work incurred on the assignment.'),
            ORM_StringField('ActualOvertimeWork')			              // ex: PT112H0M0S
            ->setDescription('The actual amount of overtime work incurred on the assignment.'),
            ORM_StringField('RemainingWork')			 // ex: PT112H0M0S
            ->setDescription('The remaining work scheduled to complete the assignment.'),
            ORM_StringField('RemainingOvertimeWork')			// ex: PT112H0M0S
            ->setDescription('The remaining overtime work scheduled to complete the assignment.'),
            ORM_StringField('RegularWork')			// ex: PT112H0M0S
            ->setDescription('The amount of non-overtime work scheduled for the assignment.'),
            ORM_StringField('OvertimeWork')			// ex: PT112H0M0S
            ->setDescription('The scheduled overtime work scheduled for the assignment.'),
            ORM_EnumField('BookingType', $this->getBookingTypes())
            ->setDescription('Specifies the booking type of the assignment.'),
            ORM_DateTimeField('CreationDate')
        );

        $this->setPrimaryKey('UID');

        $this->hasOne('TaskUID', '\Ovidentia\LibProject\TaskSet');
        $this->hasOne('ResourceUID', '\Ovidentia\LibProject\ResourceSet');

        $this->hasOne('project', '\Ovidentia\LibProject\ProjectSet', 'ORM_StringField');			// parent in XML
    }


    /**
     *
     *
     * Warning, the documentation in the XSD propose 1=Commited, 2=Proposed
     * but values are differents in xml
     */
    public function getBookingTypes()
    {
        return array(
                0 => translate('Commited'),
                1 => translate('Proposed')
        );
    }


    /**
     * Save
     * and initialize creation date
     * 
     * @return bool
     */
    public function save(Assignment $record)
    {
        if (!isset($record->UID)) {
            $record->CreationDate = date('Y-m-d H:i:s');
        }
        
        return parent::save($record);
    }
}
