<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
namespace Ovidentia\LibProject;


class PathTest extends \PHPUnit_Framework_TestCase
{
    
    /**
     * @beforeClass
     */
    public static function loadMockObjects()
    {
        require_once dirname(__FILE__).'/../mockObjects.php';
    }
    
    /**
     * @return Project
     */
    public function getProject()
    {
        $mock = new MockProject();
        return $mock->getTwoRelatedTasks();
    }
    
    
    public function testLoadFromTask()
    {
        $tasks = $this->getProject()->selectActiveTasks();
        
        $this->assertCount(2, $tasks);
        
        foreach ($tasks as $task) {
            /*@var $task Task */
            $path = $task->getPath();
            
            $this->assertNotNull($path);
            $this->assertInstanceOf('\Ovidentia\LibProject\Path', $path);
        }
    }
    
    /**
     * @return Path
     */
    public function getPath()
    {
        $tasks = $this->getProject()->selectActiveTasks();
        foreach ($tasks as $task) {
            return $task->getPath();
        }
    }
    
    
    public function testPathDates()
    {
        $path = $this->getPath();
        $this->assertEquals('2014-01-01 00:00:00', $path->getStart());
        $this->assertEquals('2014-01-03 00:00:00', $path->getFinish());
    }
    
    
    
    
    public function testPathBoundaries()
    {
        list($firstTask, $secondTask) = MockTwoRelatedTasks();
        
        $path = $firstTask->getPath();
        $boundaries = $path->getDatesBoundaries($firstTask);
        
        $this->assertNull($boundaries->getStartMin());
        $this->assertNull($boundaries->getStartMax());
        
        $this->assertNull($boundaries->getFinishMin());
        $this->assertNull($boundaries->getFinishMax());
        
        $path = $secondTask->getPath();
        /* @var $path Path */
        $boundaries = $path->getDatesBoundaries($secondTask);
        
        $this->assertEquals('2014-01-02 00:00:00', $boundaries->getStartMin());
        $this->assertNull($boundaries->getStartMax());
        
        $this->assertEquals('2014-01-02 00:00:00', $boundaries->getFinishMin());
        $this->assertNull($boundaries->getFinishMax());
    
    }
    
    
    public function testTaskBoundaries()
    {
        
        list($firstTask, $secondTask) = MockTwoRelatedTasks();
        
        $boundaries = $firstTask->getDatesBoundaries();
        
        $this->assertNull($boundaries->getStartMin());
        $this->assertNull($boundaries->getStartMax());
        
        $this->assertNull($boundaries->getFinishMin());
        $this->assertNull($boundaries->getFinishMax());
        
        $boundaries = $secondTask->getDatesBoundaries();
        
        $this->assertEquals('2014-01-02 00:00:00', $boundaries->getStartMin());
        $this->assertNull($boundaries->getStartMax());
        
        $this->assertEquals('2014-01-02 00:00:00', $boundaries->getFinishMin());
        $this->assertNull($boundaries->getFinishMax());
    }
    
    
    public function testTaskBoundariesWithConstraints()
    {
    
        list($firstTask, $secondTask) = MockTwoRelatedTasks();
        
        /*@var $firstTask Task */
        /*@var $secondTask Task */
        
        $firstTask->ConstraintType = Task::START_NO_LATER_THAN;
        $firstTask->ConstraintDate = '2014-01-08 00:00:00';
        
        $secondTask->ConstraintType = Task::FINISH_NO_EARLIER_THAN;
        $secondTask->ConstraintDate = '2014-01-10 00:00:00';
    
        $boundaries = $firstTask->getDatesBoundaries();
        
        // First TASK

        $this->assertNull($boundaries->getStartMin());
        $this->assertEquals('2014-01-08 00:00:00', $boundaries->getStartMax());
    
        $this->assertNull($boundaries->getFinishMin());
        $this->assertNull($boundaries->getFinishMax());
    
        $boundaries = $secondTask->getDatesBoundaries();
        
        // Second TASK
    
        $this->assertEquals('2014-01-02 00:00:00', $boundaries->getStartMin());
        $this->assertNull($boundaries->getStartMax());
    
        $this->assertEquals('2014-01-10 00:00:00', $boundaries->getFinishMin());
        $this->assertNull($boundaries->getFinishMax());
    }
}
