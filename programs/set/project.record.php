<?php
// -------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
// -------------------------------------------------------------------------
/**
 *
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
namespace Ovidentia\LibProject;

/**
 *
 * @property int $uuid
 *
 * @property string $Name
 * @property string $Title
 * @property string $Subject
 * @property string $Category
 * @property string $Company
 * @property string $Manager
 * @property string $Author
 * @property string $CreationDate
 * @property int $Revision
 * @property string $LastSaved
 * @property int $ScheduleFromStart
 * @property string $StartDate
 * @property string $FinishDate
 * @property string $DefaultStartTime
 * @property string $DefaultFinishTime
 * 
 * @method \Ovidentia\LibProject\ProjectSet getParentSet()
 */
class Project extends Record
{
    
    /**
     *
     * @var TaskSet
     */
    private $taskSet = null;
    
    
    /**
     * Get or set the TaskSet used for internal queries
     *
     * @param \Ovidentia\LibProject\TaskSet $set
     * @return \Ovidentia\LibProject\TaskSet
     */
    public function taskSet($set = null)
    {
        if (isset($set)) {
            $this->taskSet = $set;
        }
    
        if (! isset($this->taskSet)) {
            $this->taskSet = Storage()->TaskSet();
        }
    
        return $this->taskSet;
    }
    

    /**
     * Remove all data associated to project
     */
    public function deleteAll()
    {
        $this->deleteAssignments();
        $this->deleteResources();
        $this->deleteCalendars();
        $this->deleteTasks();
    }

    /**
     * Select the calendars associated to project
     * @return \ORM_Iterator
     */
    public function selectCalendars()
    {
        $set = Storage()->CalendarSet();
        $res = $set->select($set->project->is($this->uuid));
        
        $res->orderAsc($set->Name);
        
        return $res;
    }

    /**
     * Delete the calendars associated to project
     * @return bool
     */
    public function deleteCalendars()
    {
        $set = Storage()->calendarSet();
        return $set->delete($set->project->is($this->uuid));
    }

    /**
     * Select the tasks associated to project 
     *
     * @return \ORM_Iterator
     */
    public function selectTasks()
    {
        $set = $this->taskSet();
        $res = $set->select($set->project->is($this->uuid));
        
        $res->orderAsc($set->ID);
        
        return $res;
    }
    
    
    /**
     * Select the actives tasks associated to project
     *
     * @return \ORM_Iterator
     */
    public function selectActiveTasks()
    {
        $set = $this->taskSet();
        $res = $set->select($set->project->is($this->uuid)->_AND_($set->Active->is(1)));
    
        $res->orderAsc($set->ID);
    
        return $res;
    }
    
    /**
     * Select tasks to be moved on project properties modifications
     * 
     * @return \ORM_Iterator
     */
    public function selectUnplannedTasks()
    {
        $set = $this->taskSet();
        $res = $set->select(
            $set->project->is($this->uuid)->_AND_(
                $set->Finish->is('0000-00-00 00:00:00')->_OR_($set->Finish->is(null))
            )->_AND_(
                $set->Start->is('0000-00-00 00:00:00')->_OR_($set->Start->is(null))
            )
        );
        
        $res->orderAsc($set->ID);
        
        return $res;
    }
    
    

    /**
     * delete the tasks associated to project
     * @return boolean
     */
    public function deleteTasks()
    {
        $set = $this->taskSet();
        return $set->delete($set->project->is($this->uuid));
    }

    
    /**
     * Select the resources associated to project
     * @return \ORM_Iterator
     */
    public function selectResources()
    {
        $set = Storage()->ResourceSet();
        $res = $set->select($set->project->is($this->uuid));
        
        $res->orderAsc($set->ID);
        
        return $res;
    }

    /**
     * Delete resources associated to project
     * @return bool
     */
    public function deleteResources()
    {
        $set = Storage()->ResourceSet();
        return $set->delete($set->project->is($this->uuid));
    }

    /**
     * Select assigments of resources to tasks
     * @return \ORM_Iterator
     */
    public function selectAssignments()
    {
        $storage = Storage();
        $storage->includeResourceSet();
        $set = $storage->AssignmentSet();
        $set->ResourceUID();
        $res = $set->select($set->project->is($this->uuid));
        
        $res->orderAsc($set->ResourceUID->Name);
        
        return $res;
    }

    /**
     * Delete assigments of resources to tasks
     * @return bool
     */
    public function deleteAssignments()
    {
        $set = Storage()->AssignmentSet();
        return $set->delete($set->project->is($this->uuid));
    }
    
    /**
     * Create new instance of project statistics
     * @return ProjectStatistics
     */
    public function statistics()
    {
        require_once dirname(__FILE__).'/../utilit/project_statistics.class.php';
        return new ProjectStatistics($this);
    }
    
    
    /**
     * Update all tasks according to new properties of the project
     * 
     * 
     */
    public function updateTasks()
    {
        foreach ($this->selectTasks() as $task) {
            
            /*@var $task Task */
            
            if ($task->isFrozen()) {
                continue;
            }

            if ($this->ScheduleFromStart) {
                if ($task->setActualStartAfter(new \DateTime($this->StartDate))) {
                    // sucessors have to me moved accordingly
                    
                    $task->updateSuccessors();
                }
            } else {
                if ($task->setActualFinishBefore(new \DateTime($this->FinishDate))) {
                    // predecessors have to be moved accordingly
                    
                    $task->updatePredecessors();
                }
            }
            
            $task->save();
        }
    }
    
    
    /**
     * Get period boundaries for the project tasks
     * 
     * @return PeriodBoundaries
     */
    public function getTaskBoundaries()
    {
        $boundaries = new PeriodBoundaries();
        
        if ($this->ScheduleFromStart) {
            if (!isset($this->StartDate) || '0000-00-00 00:00:00' === $this->StartDate) {
                return null;
            }
            
            $boundaries->setStartMin($this->StartDate);
            $boundaries->setFinishMin($this->StartDate);
            
        } else {
            if (!isset($this->FinishDate) || '0000-00-00 00:00:00' === $this->FinishDate) {
                return null;
            }
            
            $boundaries->setStartMax($this->FinishDate);
            $boundaries->setFinishMax($this->FinishDate);
        }
        
        return $boundaries;
    }
    
    
    /**
     * Set default values for a new project
     * @return Project
     */
    public function setNewProjectdefaultValues()
    {
        require_once $GLOBALS['babInstallPath'].'utilit/settings.class.php';
        $curentUser = bab_getUserName(bab_getUserId());
        
        $this->Author = $curentUser;
        $this->Manager = $curentUser;
        $this->Revision = 1;
        $this->StartDate = date('Y-m-d H:i:s');
        $this->ScheduleFromStart = 1;
        
        
        $settings = bab_getInstance('bab_Settings');
        /*@var $settings bab_Settings */
        $site = $settings->getSiteSettings();
        
        $this->DefaultStartTime = $site['start_time'];
        $this->DefaultFinishTime = $site['end_time'];
        
        return $this;
    }
    
    

    /**
     * Import xml data in MSPDI format into project
     * 
     * @param \DOMDocument $mspdi            
     */
    public function putDOMDocument(\DOMDocument $mspdi)
    {
        require_once dirname(__FILE__) . '/../utilit/import.class.php';
        
        $import = new MspdiImport($mspdi, $this);
        $import->importProject();
    }

    /**
     * get XML in MSPDI format
     * 
     * @return \DOMDocument
     */
    public function getDOMDocument()
    {
        $xml = new \DOMDocument('1.0', 'UTF-8');
        
        $xml_Project = $xml->createElement('Project');
        $xml_Project->setAttribute('xmlns', 'http://schemas.microsoft.com/project');
        
        $xml->appendChild($xml_Project);
        
        $this->appendXml($xml, $xml_Project);
        
        return $xml;
    }

    /**
     * Append record to XML under the $node
     * 
     * @param \DOMDocument $xml            
     * @param \DOMElement $node            
     */
    protected function appendXml(\DOMDocument $xml, \DOMElement $node)
    {
        parent::appendXml($xml, $node);
        
        // append Calendars
        $xml_Calendars = $xml->createElement('Calendars');
        $node->appendChild($xml_Calendars);
        
        $this->appendXmlIterator($xml, $xml_Calendars, $this->selectCalendars());
        
        // append Tasks
        $xml_Tasks = $xml->createElement('Tasks');
        $node->appendChild($xml_Tasks);
        
        $this->appendXmlIterator($xml, $xml_Tasks, $this->selectTasks());
        
        // append Resources
        $xml_Resources = $xml->createElement('Resources');
        $node->appendChild($xml_Resources);
        
        $this->appendXmlIterator($xml, $xml_Resources, $this->selectResources());
        
        // append Assignments
        $xml_Assignments = $xml->createElement('Assignments');
        $node->appendChild($xml_Assignments);
        
        $this->appendXmlIterator($xml, $xml_Assignments, $this->selectAssignments());
    }
}
