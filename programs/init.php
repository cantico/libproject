<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
// 
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */

// @codingStandardsIgnoreStart


function LibProject_onDeleteAddon()
{
    require_once $GLOBALS['babInstallPath'] . 'utilit/functionalityincl.php';
    
    $functionalities = new bab_functionalities();
    $functionalities->unregister('ProjectStorage');
    
    return true;
}



function LibProject_upgrade()
{

    require_once $GLOBALS['babInstallPath'] . 'utilit/functionalityincl.php';
    require_once $GLOBALS['babInstallPath'] . 'utilit/devtools.php';
    require_once dirname(__FILE__).'/functions.php';
    

    $functionalities = new bab_functionalities();
    $functionalities->register('ProjectStorage', dirname(__FILE__) . '/storage.class.php');
    
    $storage = \Ovidentia\LibProject\storage();

    
    $synchronize = new bab_synchronizeSql();
    $synchronize->addOrmSet($storage->AssignmentSet());
    $synchronize->addOrmSet($storage->AvailabilityPeriodSet());
    $synchronize->addOrmSet($storage->CalendarSet());
    $synchronize->addOrmSet($storage->PredecessorLinkSet());
    $synchronize->addOrmSet($storage->ProjectSet());
    $synchronize->addOrmSet($storage->ResourceSet());
    $synchronize->addOrmSet($storage->TaskSet());
    $synchronize->addOrmSet($storage->WeekDaySet());
    $synchronize->addOrmSet($storage->WorkingTimeSet());
    $synchronize->addOrmSet($storage->TimePeriodSet());
    $synchronize->updateDatabase();

    return true;
}

// @codingStandardsIgnoreEnd
