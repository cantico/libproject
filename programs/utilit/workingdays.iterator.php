<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
namespace Ovidentia\LibProject;

/**
 * Iterator of working days on a calendar
 * This iterator has no end and return only weekdays with the DayWorked property set to true except 
 * the days from the exception list
 * each iteration return a DateTime corresponding to the date start time of the worked day
 */
class WorkingDays implements \Iterator
{
    /**
     * @var \DateTime
     */
    protected $startDate;
    
    /**
     * @var Calendar
     */
    protected $calendar;
    
    
    /**
     * Number of days from startDate
     * @var int
     */
    private $days;
    
    
    /**
     * @var \DateTime
     */
    private $currentDate;
    
    /**
     * @var bool
     */
    private $valid = true;
    
    /**
     * Number of days to add
     * @var int
     */
    private $add;
    
    /**
     * The interval to add
     * @var \DateInterval
     */
    private $interval;
    
    
    /**
     * 
     * @param Calendar  	 $calendar       Calendar used to retrive the worked week days
     * @param \DateTime      $startDate      Start date, hours will be ignored
     * @param bool			 $invert		 The iterator will bo backward in dates
     */
    public function __construct(Calendar $calendar, \DateTime $startDate, $invert = false)
    {
        $this->calendar = $calendar;
        $this->startDate = $startDate;
        $this->currentDate = clone $startDate;
        
        $this->interval = new \DateInterval('P1D');
        
        if ($invert) {
            $this->add = -1;
            $this->interval->invert = 1;
        } else {
            $this->add = 1;
        }
    }
    
    /**
     * @return DateTime
     */
    public function current()
    {
        return $this->currentDate;
    }
    
    /**
     * Test if the current date is worked
     * @return bool
     */
    protected function isWorked()
    {
        $daytype = getDayType($this->currentDate);
        
        $weekday = $this->calendar->getWeekday($daytype);
        
        if (!isset($weekday)) {
            return false;
        }
        
        if (!$weekday->isWorking()) {
            return false;
        }
        
        // tests exceptions
        
        $timeperiodday = $this->calendar->getTimePeriodDay($this->currentDate);
        
        
        if (isset($timeperiodday) && !$timeperiodday->isWorked()) {
            return false;
        }
        
        return true;
    }
    
    
    /**
     * @return int
     */
    public function key()
    {
        return $this->days;
    }
    
    /**
     * Forward 1 day, limited to 6 days
     */
    public function next()
    {
        $limit = 6;
        
        do {
            
            if ($limit <= 0) {
                $this->valid = false;
                return;
            }
            
            
            $this->days += $this->add;
            $this->currentDate->add($this->interval);
            $limit--;
        } while (!$this->isWorked());
    }
    
    /**
     * 
     */
    public function rewind()
    {
        $this->days = 0;
        $this->currentDate = clone $this->startDate;
    }
    
    /**
     * @return bool
     */
    public function valid()
    {
        return $this->valid;
    }
}
