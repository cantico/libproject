<?php
// -------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
// -------------------------------------------------------------------------
/**
 *
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
namespace Ovidentia\LibProject;

/**
 * Calendar
 * A calendar contain weekdays, weekdays contains workingtimes
 *
 * @property int $UID
 * @property string $Name
 * @property bool $IsBaseCalendar
 * @property Project $project
 * @property Calendar $BaseCalendarUID
 * @method project()
 * @method BaseCalendarUID()
 * @method CalendarSet getParentSet()
 */
class Calendar extends Record
{

    /**
     *
     * @var WeekDaySet
     */
    private $weekDaySet = null;

    /**
     * Cache for base calendar
     * 
     * @var Calendar
     */
    private $baseCalendar = null;

    /**
     * Cache for exception time periods
     * time periods are indexed by day in this array
     * 
     * @see Calendar::getExceptionTimePeriods()
     * @var Array
     */
    private $exceptionDays = null;
    
    
    
    /**
     * @see Calendar::user()
     * @var CalendarUser
     */
    private $calendarUser = null;
    
    
    /**
     * @see Calendar::date()
     * @var CalendarDate
     */
    private $calendarDate = null;
    
    
    /**
     * @see Calendar::hasWeekDayDefinition()
     * @var bool
     */
    private $weekDaydefinition = null;
    
    

    /**
     * Get or set the weekday set used for internal queries
     * 
     * @param \Ovidentia\LibProject\WeekDaySet $set            
     * @return \Ovidentia\LibProject\WeekDaySet
     */
    public function weekDaySet($set = null)
    {
        if (isset($set)) {
            $this->weekDaySet = $set;
        }
        
        if (! isset($this->weekDaySet)) {
            $this->weekDaySet = Storage()->WeekDaySet();
        }
        
        return $this->weekDaySet;
    }

    /**
     * Get base calendar
     * 
     * @return Calendar
     */
    public function getBaseCalendar()
    {
        if (! isset($this->baseCalendar)) {
            $set = $this->getParentSet();
            $this->baseCalendar = $set->get($set->UID->is($this->BaseCalendarUID));
        }
        
        return $this->baseCalendar;
    }

    /**
     * Select week days of the calendar
     * if the calendar is a resource calendar, the weekdays
     * are retreived from the base calendar
     * instead of the resource calendar
     * the default behaviour will be to use the base calendar
     * infos only if the are no weekdays defined on the resource calendar
     *
     *
     * @param string $sortMethod            
     *
     * @return ORM_Iterator
     */
    public function selectWeekDays($sortMethod = 'orderAsc')
    {
        $res = $this->selectCalendarWeekDays($sortMethod);
        
        if ($this->IsBaseCalendar || ! $this->BaseCalendarUID) {
            return $res;
        }
        
        if ($res->count() > 0) {
            return $res;
        }
        
        if ($baseCalendar = $this->getBaseCalendar()) {
            return $baseCalendar->selectCalendarWeekDays();
        }
        
        return $res;
    }
    
    
    /**
     * Select week days of the current calendar
     *
     *
     * @param string $sortMethod        orderAsc | orderDesc
     *
     * @return ORM_Iterator
     */
    public function selectCalendarWeekDays($sortMethod = null)
    {
        
        $set = $this->WeekDaySet();
        $res = $set->select(
            $set->calendar->is($this->UID)
            ->_AND_($set->DayType->is(WeekDay::EXCEPTION)->_NOT())
        );
        
        if (isset($sortMethod)) {
            $res->$sortMethod($set->DayType);
        }
        
        return $res;
    }

    /**
     * Select exception week days of the calendar
     * 
     * @see Calendar::getExceptionTimePeriods() use
     *      getExceptionTimePeriods to get a merged view
     *      of all exception day for a resource
     *     
     * @return ORM_Iterator
     */
    public function selectExceptionDays()
    {
        $set = $this->WeekDaySet();
        return $set->select(
            $set->calendar->is($this->UID)
            ->_AND_($set->DayType->is(WeekDay::EXCEPTION))
        );
    }

    /**
     * If the calendar is the main calendar :
     * Get exception week days (non working days)
     * If the calendar is a resource calendar :
     * Get exception week days from the base calendar (non working days)
     * merged with exception week days from the resource calendar (days off)
     *
     * @return array of TimePeriod
     */
    protected function getExceptionTimePeriods()
    {
        $return = array();
        
        foreach ($this->selectExceptionDays() as $weekDay) {
            
            /*@var $weekDay WeekDay */
            foreach ($weekDay->selectTimePeriods() as $timePeriod) {
                /*@var $timePeriod TimePeriod */
                $return[] = $timePeriod;
            }
        }
        
        if ($base = $this->getBaseCalendar()) {
            $return += $base->getExceptionTimePeriods();
        }
        
        return $return;
    }

    /**
     * exception Time periods indexed by days (day off and non working days)
     * The result contain non working days from the base calendar and days off from the ressource calendar
     * To get only the non working days from the base calendar use ->getBaseCalendar->getExceptionDays()
     *
     * @return array [TimePeriodDay]
     */
    public function getExceptionDays()
    {
        if (! isset($this->exceptionDays)) {
            $this->exceptionDays = array();
            $arr = $this->getExceptionTimePeriods();
            

            foreach ($arr as $timePeriod) {

                $loopOnPeriod = clone $timePeriod->getFromDate();
                $loopOnPeriod->setTime(0, 0, 0);
                
                // var_dump($loopOnPeriod->format('Y-m-d H:i:s').' '.$timePeriod->getToDate()->format('Y-m-d H:i:s'));
                
                while ($loopOnPeriod < $timePeriod->getToDate()) {
                    $day = $loopOnPeriod->format('Y-m-d');
                    
                    if (! isset($this->exceptionDays[$day])) {
                        $this->exceptionDays[$day] = new TimePeriodDay($this, $day);
                    }
                    
                    $this->exceptionDays[$day]->timeperiod[] = $timePeriod;
                    
                    $loopOnPeriod->add(new \DateInterval('P1D'));
                }
            }
        }
        
        
        // var_dump(array_keys($this->exceptionDays));
        
        return $this->exceptionDays;
    }

    /**
     * Get the timeperiodday if there is a weekday exception
     * or NULL if this is a regular day
     * 
     * @param \DateTime $datetime            
     * @return TimePeriodDay
     */
    public function getTimePeriodDay(\DateTime $datetime)
    {
        $arr = $this->getExceptionDays();
        $day = $datetime->format('Y-m-d');
        
        if (! isset($arr[$day])) {
            return null;
        }
        
        return $arr[$day];
    }

    /**
     *
     * @return bool
     */
    public function deleteWeekDays()
    {
        $set = $this->WeekDaySet();
        return $set->delete($set->calendar->is($this->UID));
    }

    /**
     * Get weekday from calendar or NULL if the weekday does not exists in calendar
     * get weekday in base calendar if there is no weekday definition in this calendar
     * 
     * @param int $daytype
     *            daytype value constants :
     *            WeekDay::MONDAY
     *            WeekDay::TUESDAY
     *            WeekDay::WEDNESDAY
     *            WeekDay::THURSDAY
     *            WeekDay::FRIDAY
     *            WeekDay::SATURDAY
     *            WeekDay::SUNDAY
     *            
     * @return \Ovidentia\LibProject\WeekDay
     */
    public function getWeekday($daytype)
    {
        if ($daytype <= WeekDay::EXCEPTION) {
            return null;
        }
        
        if (!$this->IsBaseCalendar && !$this->hasWeekDayDefinition()) {
            
            if ($baseCalendar = $this->getBaseCalendar()) {
                return $baseCalendar->getWeekday($daytype);
            }
            
            return null;
        }
        
        $set = $this->WeekDaySet();
        return $set->get(
            $set->calendar->is($this->UID)
            ->_AND_($set->DayType->is($daytype))
        );
    }

    
    /**
     * Test if the calendar has week days definition set
     * if this method return false, week days definition from the base
     * calendar must be used instead
     * 
     * @return bool
     */
    public function hasWeekDayDefinition()
    {
        if (!isset($this->weekDaydefinition)) {
            
            $res = $this->selectCalendarWeekDays();
            $this->weekDaydefinition = (0 < $res->count());
        }
        return $this->weekDaydefinition;
    }

    
    
    /**
     * @return CalendarUser
     */
    public function user()
    {
        if (!isset($this->calendarUser)) {
            require_once dirname(__FILE__).'/calendar.record.user.php';
            $this->calendarUser = new CalendarUser($this);
        }
        
        return $this->calendarUser;
    }
    
    
    /**
     * @return CalendarDate
     */
    public function date()
    {
        if (!isset($this->calendarDate)) {
            require_once dirname(__FILE__).'/calendar.record.date.php';
            $this->calendarDate = new CalendarDate($this);
        }
        
        return $this->calendarDate;
    }
    

    /**
     * Append record to XML under the $node
     * 
     * @param \DOMDocument $xml            
     * @param \DOMElement $node            
     */
    protected function appendXml(\DOMDocument $xml, \DOMElement $node)
    {
        parent::appendXml($xml, $node);
        
        // append WeekDays
        $xml_WeekDays = $xml->createElement('WeekDays');
        $node->appendChild($xml_WeekDays);
        
        $this->appendXmlIterator($xml, $xml_WeekDays, $this->selectWeekDays());
    }

    /**
     * Receive the parent record
     * use this method to update links in record
     * 
     * @param Project $parent
     */
    public function updateParentByXml(Project $parent)
    {
        $this->project = $parent->uuid;
    }
}
