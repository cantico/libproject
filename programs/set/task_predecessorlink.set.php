<?php
// -------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
// -------------------------------------------------------------------------
/**
 *
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
namespace Ovidentia\LibProject;

/**
 * This set provide links beetween tasks (predecessors or successors)
 *
 * A predecessor is a task whose start or finish date determines the start or finish date of its successor task.
 * A successor is a task whose start or finish date is driven by its predecessor task.
 * The nature of the relationship between a predecessor task and a successor task
 * determines the type of task dependency to use.
 *
 * Fields are named according to the MSPDI format
 *
 * @property \ORM_PkField $id
 * @property \ORM_EnumField $Type
 * @property \ORM_IntField $LinkLag
 * @property \Ovidentia\LibProject\TaskSet $task
 * @property \Ovidentia\LibProject\TaskSet $PredecessorUID
 */
class PredecessorLinkSet extends RecordSet
{

    /**
     * predecessor link set
     */
    public function __construct()
    {
        parent::__construct();
        
        $this->setPrimaryKey('id');
        
        $this->addFields(
            ORM_EnumField('Type', $this->getTypes()), // 1: default value (FS)
            ORM_IntField('LinkLag')->setDescription('The amount of lag in tenths of a minute.')
        );
        
        $this->hasOne('task', '\Ovidentia\LibProject\TaskSet'); // parent in XML
        $this->hasOne('PredecessorUID', '\Ovidentia\LibProject\TaskSet');
    }

    /**
     *
     * @return array
     */
    public function getTypes()
    {
        return array(
            PredecessorLink::FF => 'Finish to finish', // Task cannot finish until predecessor finishes
            PredecessorLink::FS => 'Finish to start', // Task cannot start until predecessor finishes (default)
            PredecessorLink::SF => 'Start to finish', // Task cannot finish until predecessor starts
            PredecessorLink::SS => 'Start to start' // Task cannot start until predecessor starts
        );
    }

    /**
     * Save
     * Init type to FS if not set (finish to start)
     * @see Ovidentia\LibProject.RecordSet::save()
     * 
     * @param PredecessorLink $record
     * 
     * @return bool
     */
    public function save(PredecessorLink $record)
    {
        if (! isset($record->Type)) {
            $record->Type = PredecessorLink::FS;
        }
        
        return parent::save($record);
    }
}
