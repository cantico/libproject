<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */

namespace Ovidentia\LibProject;



/**
 * @property int       				$UID
 * @property int					$PercentWorkComplete
 * @property string					$ActualFinish
 * @property string					$ActualStart
 * @property string					$Notes
 * @property int					$ResponsePending
 * @property string					$Start
 * @property string					$Finish
 * @property string					$Stop
 * @property string					$Resume
 * @property float					$Units
 * @property int					$UpdateNeeded
 * @property string					$Work
 * @property string		            $ActualWork
 * @property string		            $ActualOvertimeWork
 * @property string		            $RemainingWork
 * @property string		            $RemainingOvertimeWork
 * @property string		            $RegularWork
 * @property string		            $OvertimeWork
 * @property int					$BookingType
 * @property string					$CreationDate
 * @property Task		            $TaskUID
 * @property Resource	            $ResourceUID
 * @property Project		        $project
 * 
 * @method AssignmentSet getParentSet()
 * @method Project project()
 */
class Assignment extends Record
{

    /**
     * Update the actual dates according to the resources calendars and availability
     * @return Assignment
     */
    public function updateActualDates()
    {
        //TODO

        return $this;
    }


    /**
     * Receive the parent record
     * use this method to update links in record
     * 
     * @param Project $parent
     */
    public function updateParentByXml(Project $parent)
    {
        $this->project = $parent->uuid;
    }
}
