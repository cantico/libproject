<?php
// -------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
// -------------------------------------------------------------------------
/**
 *
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
namespace Ovidentia\LibProject;


/**
 * Min and max dates for a period
 *
 */
class PeriodBoundaries
{
    /**
     * 
     * @var array
     */
    private $start;
    
    /**
     * 
     * @var array
     */
    private $finish;
    
    
    /**
     * Set boundaries default values
     */
    public function __construct()
    {
        $this->start = array('min' => null, 'max' => null);
        $this->finish = array('min' => null, 'max' => null);
    }
    
    /**
     * 
     * @param string $min
     * @return \Ovidentia\LibProject\PeriodBoundaries
     */
    public function setStartMin($min)
    {
        $this->start['min'] = $min;
        return $this;
    }
    
    /**
     * 
     * @return string
     */
    public function getStartMin()
    {
        return $this->start['min'];
    }
    
    /**
     * 
     * @param string $max
     * @return \Ovidentia\LibProject\PeriodBoundaries
     */
    public function setStartMax($max)
    {
        $this->start['max'] = $max;
        return $this;
    }
    
    /**
     *
     * @return string
     */
    public function getStartMax()
    {
        return $this->start['max'];
    }
    
    /**
     *
     * @param string $min
     * @return \Ovidentia\LibProject\PeriodBoundaries
     */
    public function setFinishMin($min)
    {
        $this->finish['min'] = $min;
        return $this;
    }
    
    /**
     *
     * @return string
     */
    public function getFinishMin()
    {
        return $this->finish['min'];
    }
    
    /**
     *
     * @param string $max
     * @return \Ovidentia\LibProject\PeriodBoundaries
     */
    public function setFinishMax($max)
    {
        $this->finish['max'] = $max;
        return $this;
    }
    
    /**
     *
     * @return string
     */
    public function getFinishMax()
    {
        return $this->finish['max'];
    }
    
    
    
    
    /**
     * Combine min and max for start or finish
     * 
     * @param string $min
     * @param string $max
     */
    public function combineProperty($property, $min, $max)
    {
        $boundary = & $this->$property;

        if (isset($min)) {
            if (null === $boundary['min'] || $min > $boundary['min']) {
                $boundary['min'] = $min;
            }
        }
        
        if (isset($max)) {
            if (null === $boundary['max'] || $max < $boundary['max']) {
                $boundary['max'] = $max;
            }
        }
    }
    
    
    /**
     * Combine a period boundaries into this object
     * the current object will be modified with the most restrictives boundaries
     * 
     * @param PeriodBoundaries $boundaries
     * 
     */
    public function combine(PeriodBoundaries $boundaries)
    {
        $this->combineProperty('start', $boundaries->getStartMin(), $boundaries->getStartMax());
        $this->combineProperty('finish', $boundaries->getFinishMin(), $boundaries->getFinishMax());
    }
}
