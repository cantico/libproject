<?php
// -------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
// -------------------------------------------------------------------------
/**
 *
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
namespace Ovidentia\LibProject;

/**
 *
 * @property int $id
 * @property int $DayType
 * @property int $DayWorking
 * @property Calendar $calendar
 */
class WeekDay extends Record
{

    const EXCEPTION = 0; // day off, the weekday should contain TimePeriod records
    const MONDAY    = 1;
    const TUESDAY   = 2;
    const WEDNESDAY = 3;
    const THURSDAY  = 4;
    const FRIDAY    = 5;
    const SATURDAY  = 6;
    const SUNDAY    = 7;

    /**
     *
     * @var \Ovidentia\LibProject\WorkingTimeSet
     */
    private $workingTimeSet = null;

    /**
     *
     * @var \Ovidentia\LibProject\TimePeriodSet
     */
    private $timePeriodSet = null;

    /**
     * Get or set the workingtime set used for internal queries
     * 
     * @param \Ovidentia\LibProject\WorkingTimeSet $set            
     * @return \Ovidentia\LibProject\WorkingTimeSet
     */
    public function workingTimeSet($set = null)
    {
        if (isset($set)) {
            $this->workingTimeSet = $set;
        }
        
        if (! isset($this->workingTimeSet)) {
            $this->workingTimeSet = Storage()->WorkingTimeSet();
        }
        
        return $this->workingTimeSet;
    }

    /**
     * Get or set the TimePeriod set used for internal queries
     * 
     * @param \Ovidentia\LibProject\TimePeriodSet $set            
     * @return \Ovidentia\LibProject\TimePeriodSet
     */
    public function timePeriodSet($set = null)
    {
        if (isset($set)) {
            $this->timePeriodSet = $set;
        }
        
        if (! isset($this->timePeriodSet)) {
            $this->timePeriodSet = Storage()->TimePeriodSet();
        }
        
        return $this->timePeriodSet;
    }

    /**
     *
     * @return ORM_Iterator
     */
    public function selectWorkingTimes($orderMethod = 'orderAsc')
    {
        $set = $this->WorkingTimeSet();
        $res = $set->select($set->weekday->is($this->id));
        
        $res->$orderMethod($set->FromTime);
        
        return $res;
    }

    /**
     * Worked total duration of the week day in seconds
     * 
     * @return int
     */
    public function getWorkingTimeDuration()
    {
        $total = 0;
        
        foreach ($this->selectWorkingTimes() as $workingTime) {
            /*@var $workingTime WorkingTime */
            $wtDuration = $workingTime->getDuration();
            
            if (isset($wtDuration)) {
                $total += $wtDuration;
            }
        }
        
        return $total;
    }

    /**
     *
     * @return bool
     */
    public function deleteWorkingTimes()
    {
        $set = $this->WorkingTimeSet();
        return $set->delete($set->weekday->is($this->id));
    }

    /**
     *
     * @return ORM_Iterator
     */
    public function selectTimePeriods()
    {
        $set = $this->TimePeriodSet();
        $res = $set->select($set->weekday->is($this->id));
        
        $res->orderAsc($set->FromDate);
        
        return $res;
    }

    /**
     *
     * @return bool
     */
    public function deleteTimePeriods()
    {
        $set = $this->TimePeriodSet();
        return $set->delete($set->weekday->is($this->id));
    }

    /**
     * Test if the day is a working day
     * using the DayWorking field
     *
     * @return bool
     */
    public function isWorking()
    {
        $DayWorking = (bool) $this->DayWorking;
        
        if (false === $DayWorking) {
            return false;
        }
        
        return true;
    }

    /**
     * Append record to XML under the $node
     * 
     * @param \DOMDocument $xml            
     * @param \DOMElement $node            
     */
    protected function appendXml(\DOMDocument $xml, \DOMElement $node)
    {
        parent::appendXml($xml, $node);
        
        // append WorkingTimes
        $xml_WorkingTimes = $xml->createElement('WorkingTimes');
        $node->appendChild($xml_WorkingTimes);
        
        $this->appendXmlIterator($xml, $xml_WorkingTimes, $this->selectWorkingTimes());
        
        // append TimePeriods
        $this->appendXmlIterator($xml, $node, $this->selectTimePeriods());
    }

    /**
     * Receive the parent record
     * use this method to update links in record
     * 
     * @param Calendar $parent
     */
    public function updateParentByXml(Calendar $parent)
    {
        $this->calendar = $parent->UID;
    }
}
