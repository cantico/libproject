<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
namespace Ovidentia\LibProject;


/**
 * Iterator of schedulables periods on a calendar
 * schedulables periods are working time periods less the exceptions periods 
 * of the same calendar and exceptions of the base calendar
 * This iterator has no end
 */
class SchedulablePeriods implements \Iterator
{
    
    /**
     * @var \DateTime
     */
    protected $startDate;
    
    /**
     * @var Calendar
     */
    protected $calendar;
    

    /**
     * @var WorkingTimes
     */
    protected $workingTimes;
    
    
    /**
     * 
     * @var WorkingTimeDate
     */
    protected $currentWTDate;
    
    
    /**
     * 
     * @var \DateTime
     */
    protected $currentDate;
    
    /**
     *
     * @var SchedulablePeriod
     */
    protected $currentSchedulable;
    
    
    /**
     * list of schedulable periods from on WorkingTimeDate
     * @var array
     */
    protected $schedulableStack = array();
    
    /**
     * 
     * @var bool
     */
    private $valid =true;
    
    /**
     * 
     * @var int
     */
    private $key = 0;
    
    /**
     * 
     * @var boolean
     */
    public $invert;
    
    
    /**
     *
     * @param Calendar  	$calendar        Calendar used to retrive the working times and the exceptions
     * @param \DateTime      $startDate       Start date, hours will be ignored
     * @param bool			$invert			 The iterator will bo backward in dates
     */
    public function __construct(Calendar $calendar, \DateTime $startDate, $invert = false)
    {
        $this->calendar = $calendar;
        $this->startDate = $startDate;
        $this->invert = $invert;

        $this->workingTimes = new WorkingTimes($calendar, $startDate, $invert);
    }
    
    
    /**
     * Get a list of schedulable periods from a working time date
     * a working time period with one exception period in the middle will return 2 schedulable periods
     * 
     * @param WorkingTimeDate $WTDate
     * 
     * @return Array
     */
    protected function getSchedulablePeriods(WorkingTimeDate $WTDate)
    {
        $timePeriodDay = $this->calendar->getTimePeriodDay($WTDate->dateTime);
        
        // timeperiodday is a list of exception periods
        
        if (!isset($timePeriodDay)) {
            $schedulablePeriod = new SchedulablePeriod();
            
            $schedulablePeriod->fromDate = $WTDate->getFromDate();
            $schedulablePeriod->toDate = $WTDate->getToDate();
            
            return array($schedulablePeriod);
        }

        $return = array();
        $uncovered = $timePeriodDay->getUncoveredPeriods();

        foreach ($uncovered as $uncoverdPeriod) {
    
            $intersection = $WTDate->getIntersection($uncoverdPeriod[0], $uncoverdPeriod[1]);
    
            if (isset($intersection)) {
                $period = new SchedulablePeriod();
                $period->fromDate = $intersection[0];
                $period->toDate = $intersection[1];
    
                $return[] = $period;
            }
        }

        return $return;
    }
    
    
    /**
     * ignore periods before/after the workingtime start date
     * @param array $schedulablePeriods
     * @param \DateTime $start
     * @return SchedulablePeriod[]
     */
    protected function filterSchedulablePeriods(Array $schedulablePeriods, \DateTime $start)
    {
        $selection = array();
        foreach ($schedulablePeriods as $schedulablePeriod) {
            /*@var $schedulablePeriod SchedulablePeriod */
            
            if ($this->invert) {
        
                if ($schedulablePeriod->fromDate < $start) {
                    
                    if ($schedulablePeriod->toDate > $start) {
                        $schedulablePeriod->toDate = $start;
                    }
                    
                    $selection[] = $schedulablePeriod;
                }
            
            } else {
                
                // var_dump($schedulablePeriod->toDate->format('Y-m-d H:i:s').' '. $start->format('Y-m-d H:i:s'));
        
                if ($schedulablePeriod->toDate > $start) {
                    
                    if ($schedulablePeriod->fromDate < $start) {
                        $schedulablePeriod->fromDate = $start;
                    }
                    
                    $selection[] = $schedulablePeriod;
                }
            
            }
        }
        
        return $selection;
    }
    
    
    
    /**
     * @return SchedulablePeriod
     */
    public function current()
    {
        return $this->currentSchedulable;
    }
    
    
    /**
     * @return int
     */
    public function key()
    {
        return $this->key;
    }
    
    /**
     * (non-PHPdoc)
     * @see Iterator::next()
     */
    public function next()
    {
        $this->key++;
        
        if (!empty($this->schedulableStack)) {
            $this->currentSchedulable = $this->fromStack();
            $this->setCurrentDate();
            return;
        }
        
        $this->workingTimes->next();
        $this->nextSchedulablePeriods();
        
    }
    
    /**
     * init currentSchedulable and fill stack if necessary
     * @return array
     */
    private function nextSchedulablePeriods()
    {
        $limit = 365;
        
        do {
            if (!$this->workingTimes->valid()) {
                $this->valid = false;
                return;
            }
        
            $WTDate = $this->workingTimes->current();
            // var_dump($this->currentDate->format('Y-m-d H:i:s').'  '.$WTDate->dateTime->format('Y-m-d H:i:s'));
            
            $schedulablePeriods = $this->getSchedulablePeriods($WTDate);
            $schedulablePeriods = $this->filterSchedulablePeriods($schedulablePeriods, $this->currentDate);
            
           
            
            

            $limit--;
            
            if (empty($schedulablePeriods)) {
                $this->workingTimes->next();
            }
            
        
        } while (0 === count($schedulablePeriods) && $limit > 0);
        
        if (0 === count($schedulablePeriods)) {
            throw new Exception('No schedulable periods founds in a calendar, more than 365 days of unavailability');
        }
        
        
        $this->schedulableStack = $schedulablePeriods;
        $this->currentSchedulable = $this->fromStack();

        $this->setCurrentDate();
    }
    
    /**
     * Set current date according to the invert property
     */
    private function setCurrentDate()
    {
        if ($this->invert) {
            $this->currentDate = clone $this->currentSchedulable->fromDate;
        } else {
            $this->currentDate = clone $this->currentSchedulable->toDate;
        }
    }
    
    
    /**
     * get a period from the stack
     * the stack is in chronological order
     * 
     * @return SchedulablePeriod
     */
    private function fromStack()
    {
        if ($this->invert) {
            return array_pop($this->schedulableStack);
        }
        
        return array_shift($this->schedulableStack);
    }
    
    
    /**
     *
     */
    public function rewind()
    {
        $this->currentDate = clone $this->startDate;
        
        $this->workingTimes->rewind();
        $this->schedulableStack = array();
        
        $this->nextSchedulablePeriods();
        
        $this->key = 0;
    }
    
    /**
     * @return bool
     */
    public function valid()
    {
        return $this->valid;
    }
}
