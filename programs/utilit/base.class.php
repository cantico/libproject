<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
namespace Ovidentia\LibProject;

/**
 * ORM Record set
 * all sets are extended from this class
 */
class RecordSet extends \ORM_MySqlRecordSet
{
    /**
     * Save
     * create a unique UID if not exists
     * 
     * @param Record $record
     * 
     * @return bool
     */
    public function save(Record $record)
    {
        
        try {
            $field = $this->getField('UID');
            
            if (($field instanceof \ORM_PkField) && !isset($record->UID)) {
                $record->UID = $this->NewUID();
            }
            
        } catch (\ORM_OutOfBoundException $exception) {
            // ignore error
        }

        return parent::save($record);
    }

    /**
     * Create a numeric ID for an UID field
     * @return double
     */
    protected function newUID()
    {
        static $static_salt = 0;
        $session = bab_getInstance('bab_Session');
        /*@var $session bab_Session */

        if (!isset($session->UID)) {
            $session_salt = 0;
            $session->UID = $session_salt;
        } else {
            $session_salt = $session->UID;
        }

        $static_salt++;
        $session_salt++;

        $str = base_convert(hash('crc32', $session->getId().$static_salt.$session_salt.time()), 16, 10);

        return (double) $str;
    }

    /**
     * Get a DateInterval object from an xsd:duration string
     * 
     * @param string $duration      Duration from database, empty string allowed
     * 
     * @return \DateInterval | null
     */
    public function getInterval($duration)
    {
        if ('' === $duration) {
            return null;
        }
        
        
        if ('-' === substr($duration, 0, 1)) {
            $interval = new \DateInterval(substr($duration, 1));
            $interval->invert = 1;

            return $interval;
        }

        return new \DateInterval($duration);
    }
    
    
    
    /**
     * Convert an ORM_RecordSet class name to a table name
     *
     * @param string $sSetClassName set class name from which the table name will be returned
     *
     * @return string The table name
     */
    public static function computeTableName($sSetClassName)
    {
        return strtolower(str_replace('\\', '_', substr($sSetClassName, 0, -3)));
    }
}
