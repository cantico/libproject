<?php 

namespace Ovidentia\LibProject;


class MockCalendar
{
    
    /**
     * 
     * @var \Ovidentia\LibProject\WeekDaySet
     */
    protected $wdSet;
    
    /**
     * 
     * @var \Ovidentia\LibProject\WorkingTimeSet
     */
    protected $wtSet;
    
    public function __construct()
    {
        $storage = Storage();
        $this->wdSet = $storage->WeekDaySet();
        $this->wtSet = $storage->WorkingTimeSet();
        $this->tpSet = $storage->TimePeriodSet();
    }
    
    
    protected function getWorkingTime(WeekDay $weekday, $FromTime, $ToTime)
    {
        $workingTime = $this->wtSet->newRecord();
        $workingTime->FromTime = $FromTime.':00';
        $workingTime->ToTime = $ToTime.':00';
        $workingTime->weekday = $weekday->id;
        $workingTime->save();
        
        return $workingTime;
    }
    
    
    protected function getWeekDay(Calendar $calendar, $type, $working, $setWorkingTimes = false)
    {
        static $idWeekDay = 1;
        
        $weekday = $this->wdSet->newRecord();

        $weekday->id = $idWeekDay++;
        $weekday->calendar = $calendar->UID;
        $weekday->DayType = $type;
        $weekday->DayWorking = $working;
        $weekday->save();
        
        $weekday->WorkingTimeSet($this->wtSet);
        $weekday->TimePeriodSet($this->tpSet);
        
        if ($setWorkingTimes) {
            $workingTimes = array();
            
            $workingTimes[] = $this->getWorkingTime($weekday, '09:00', '12:00');
            $workingTimes[] = $this->getWorkingTime($weekday, '13:00', '18:00');
            
            $backend = $this->wdSet->getBackend();
            
            $backend->setSelectReturn($this->wtSet, $this->wtSet->weekday->is($weekday->id), $workingTimes);
        }
        
        return $weekday;
    }
    
    
    /**
     * Create a non working day
     * @param WeekDay $weekday
     * @param unknown_type $date
     * 
     * @return TimePeriod
     */
    protected function getTimePeriod(WeekDay $weekday, $fromdate, $todate)
    {
        $timePeriod = $this->tpSet->newRecord();
        $timePeriod->FromDate = $fromdate;
        $timePeriod->ToDate = $todate;
        $timePeriod->weekday = $weekday->id;
        $timePeriod->save();
        
        return $timePeriod;
    }
    
    
    
    /**
     * Mock base calendar of a project with weekdays and non working days
     * 
     * @param    bool $setWorkingTimes     Add working times to the weekdays
     * 
     * @return \Ovidentia\LibProject\Calendar
     */
    public function getBaseCalendar($setWorkingTimes)
    {
        $storage = Storage();
        $calSet = $storage->CalendarSet();
        $backend = $calSet->getBackend();
    
        $calendar = $calSet->newRecord();
        $calendar->WeekDaySet($this->wdSet);
        $calendar->Name = 'Base calendar';
        $calendar->IsBaseCalendar = true;
        $calendar->project = bab_uuid();
        $calendar->UID = 1;
        $calendar->save();
    
        $weekdays = array();
        
        $weekdays[] = $this->getWeekDay($calendar, WeekDay::MONDAY, true, $setWorkingTimes);
        $weekdays[] = $this->getWeekDay($calendar, WeekDay::TUESDAY, true, $setWorkingTimes);
        $weekdays[] = $this->getWeekDay($calendar, WeekDay::WEDNESDAY, true, $setWorkingTimes);
        $weekdays[] = $this->getWeekDay($calendar, WeekDay::THURSDAY, true, $setWorkingTimes);
        $weekdays[] = $this->getWeekDay($calendar, WeekDay::FRIDAY, true, $setWorkingTimes);
        $weekdays[] = $this->getWeekDay($calendar, WeekDay::SATURDAY, false);
        $weekdays[] = $this->getWeekDay($calendar, WeekDay::SUNDAY, false);
        
        $backend->setSelectReturn(
            $this->wdSet,
            $this->wdSet->calendar->is($calendar->UID)->_AND_($this->wdSet->DayType->is(WeekDay::EXCEPTION)->_NOT()),
            $weekdays
        );
        
        
        for ($i = 1; $i <= 7; $i++) {
            $backend->setSelectReturn(
                $this->wdSet,
                $this->wdSet->calendar->is($calendar->UID)->_AND_($this->wdSet->DayType->is($i)),
                array($weekdays[$i - 1])
            );
        }
        
        $exceptions = array();
        $nonWorkingDays = array();
        
        $christmas = $this->getWeekDay($calendar, WeekDay::EXCEPTION, false, false);
        $absence = $this->getWeekDay($calendar, WeekDay::EXCEPTION, false, false);
        
        $exceptions[] = $christmas;
        $exceptions[] = $absence;
        $nonWorkingDays[] = $this->getTimePeriod($christmas, '2014-12-25 00:00:00', '2014-12-26 00:00:00');
        $nonWorkingDays[] = $this->getTimePeriod($absence, '2014-12-24 15:00:00', '2014-12-24 16:30:00');

        $backend->setSelectReturn(
            $this->wdSet,
            $this->wdSet->calendar->is($calendar->UID)->_AND_($this->wdSet->DayType->is(WeekDay::EXCEPTION)),
            $exceptions
        );
        $backend->setSelectReturn($this->tpSet, $this->tpSet->weekday->is($christmas->id), $nonWorkingDays);
        
        return $calendar;
    }
    
    
    
    /**
     * Create a resource calendar based on a base calendar for weekdays
     * this calendar add only vacation periods for the resource
     * 
     */
    public function getResourceCalendar()
    {
        $baseCalendar = $this->getBaseCalendar(true);

        $calSet = $baseCalendar->getParentSet();
        $backend = $calSet->getBackend();
        
        $calendar = $calSet->newRecord();
        $calendar->WeekDaySet($this->wdSet);
        $calendar->Name = 'Resource calendar';
        $calendar->IsBaseCalendar = false;
        $calendar->project = $baseCalendar->project;
        $calendar->BaseCalendarUID = $baseCalendar->UID;
        $calendar->UID = 2;
        $calendar->save();
        
        $backend->setSelectReturn(
            $this->wdSet,
            $this->wdSet->calendar->is($calendar->UID)->_AND_($this->wdSet->DayType->is(WeekDay::EXCEPTION)->_NOT()),
            array()
        );
        
        $backend->setSelectReturn($calSet, $calSet->UID->is($baseCalendar->UID), array($baseCalendar));
        
        
        $exceptions = array();
        $vacations = array();
        
        $absence = $this->getWeekDay($calendar, WeekDay::EXCEPTION, false, false);
        

        $exceptions[] = $absence;
        $vacations[] = $this->getTimePeriod($absence, '2014-08-18 00:00:00', '2014-08-30 00:00:00');
        
        $backend->setSelectReturn(
            $this->wdSet,
            $this->wdSet->calendar->is($calendar->UID)->_AND_($this->wdSet->DayType->is(WeekDay::EXCEPTION)),
            $exceptions
        );
        $backend->setSelectReturn($this->tpSet, $this->tpSet->weekday->is($absence->id), $vacations);
        
        return $calendar;
    }
}
