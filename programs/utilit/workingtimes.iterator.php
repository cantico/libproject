<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
namespace Ovidentia\LibProject;




/**
 * Iterator of working times on a calendar
 * each iteration return the next working time and datetime
 * This iterator has no end
 */
class WorkingTimes implements \Iterator
{
    /**
     * @var \DateTime
     */
    protected $startDate;

    /**
     * @var Calendar
     */
    protected $calendar;


    /**
     * @var \ORM_Iterator
     */
    private $weekDays;

    /**
     * The current weekday
     * @var WeekDay
     */
    private $weekDay;

    /**
     * The current working times iterator
     * @var \ORM_Iterator
     */
    private $workingTimes;


    /**
     * Browse working times in revert order
     * @var string		orderDesc | orderAsc
     */
    private $orderMethod;


    /**
     *
     * @var \DateTime
     */
    private $currentDate;


    /**
     *
     * @var \DateInterval
     */
    private $addDayInterval;


    /**
     *
     * @param Calendar  	$calendar        Calendar used to retrive the working times
     * @param \DateTime     $startDate       Start date, hours will be ignored
     * @param bool			$invert			 The iterator will bo backward in dates
     */
    public function __construct(Calendar $calendar, \DateTime $startDate, $invert = false)
    {
        $this->calendar = $calendar;
        $this->startDate = $startDate;
        $this->orderMethod = $invert ? 'orderDesc' : 'orderAsc';

        $this->addDayInterval = new \DateInterval('P1D');
        if ($invert) {
            $this->addDayInterval->invert = 1;
        }

        $this->weekDays = $calendar->selectWeekDays($this->orderMethod);
    }

    /**
     * @return WorkingTimeDate
     */
    public function current()
    {
       
        $wtDate = new WorkingTimeDate;
        $wtDate->dateTime = clone $this->currentDate;
        $wtDate->workingTime = $this->workingTimes->current();
        $startSeconds = $wtDate->workingTime->timeToSeconds($wtDate->workingTime->FromTime);
        $wtDate->dateTime->add(new \DateInterval('PT'.$startSeconds.'S'));


        return $wtDate;
    }



    /**
     * @return int
     */
    public function key()
    {
        return $this->workingTimes->key();
    }

    /**
     * (non-PHPdoc)
     * @see Iterator::next()
     */
    public function next()
    {
        
        
        /*
        $backtrace = debug_backtrace();
        var_dump( $backtrace[1]['function'] );
        */
        $this->workingTimes->next();
    }
    
    
    /**
     * Set weekdays iterator to start date
     * @throws \UnexpectedValueException
     */
    private function setWeekDaysOnStartDate()
    {
        $this->weekDays->rewind();
        
        while (true) {
            
            if (!$this->weekDays->valid() && $this->weekDays->count() > 0) {
            
                // start with next week
            
                $this->weekDays->rewind();
                if (!$this->weekDays->valid()) {
                    throw new \UnexpectedValueException('Configuration not acceptable');
                }
            }
            
            $this->weekDay = $this->weekDays->current();
            
            if ($this->weekDay->DayType === getDayType($this->startDate)) {
                break;
            }

            $this->weekDays->next();
        }
    }
    
    
    
    /**
     * Move the weekDay iterator from the start date to the first working day available
     * return the number of days for the gap beetween start date
     * 
     * 
     * @throws \UnexpectedValueException
     * 
     * @return int
     */
    private function setWeekDaysOnNextWorkedDay()
    {
        $days = 0;
        
        while (true) {
        
            if (!$this->weekDays->valid() && $this->weekDays->count() > 0) {
        
                // start with next week
        
                $this->weekDays->rewind();
                if (!$this->weekDays->valid()) {
                    throw new \UnexpectedValueException('Configuration not acceptable');
                }
            }
        
            $this->weekDay = $this->weekDays->current();
        
            if ($this->weekDay->isWorking()) {
                break;
            }
        
            $this->weekDays->next();
            $days++;
        }
        
        return $days;
    }
    


    /**
     *
     */
    public function rewind()
    {
        
        $this->setWeekDaysOnStartDate();
        $days = $this->setWeekDaysOnNextWorkedDay();
        
        
        
        $this->currentDate = new \DateTime($this->startDate->format('Y-m-d'));
        
        
        if (0 < $days) {
            $toWorkedDate = new \DateInterval('P'.$days.'D');
            $toWorkedDate->invert = $this->addDayInterval->invert;
            $this->currentDate->add($toWorkedDate);
        }
        
        
        if ($this->addDayInterval->invert) {
            $this->currentDate->add(new \DateInterval('P1D'));
        }
       
        // first working day after start date
        $this->workingTimes = $this->weekDay->selectWorkingTimes($this->orderMethod);
        

        $this->rewindToStartDate();
        
        
        if ($this->addDayInterval->invert) {
            $this->currentDate->add($this->addDayInterval);
        }
    }
    
    
    /**
     * Rewind the internal ORM workingTimes iterator to start date
     * @throws \UnexpectedValueException
     */
    private function rewindToStartDate()
    {
        if (!isset($this->workingTimes)) {
            //var_dump($this->startDate->format('Y-m-d H:i:s'));
            throw new \UnexpectedValueException('Configuration not acceptable');
        }
        
        $this->workingTimes->rewind();
        
        // forward working times to start date
        while ($this->workingTimes->valid()) {
        
            $workingTime = $this->workingTimes->current();
            /*@var $workingTime WorkingTime */
        
        
            if ($this->addDayInterval->invert && $this->startDate->format('H:i:s') > $workingTime->FromTime) {
                break;
            } elseif (!$this->addDayInterval->invert && $this->startDate->format('H:i:s') < $workingTime->ToTime) {
                break;
            }
        
            $this->workingTimes->next();
        }
    }
    

    /**
     * @return bool
     */
    public function valid()
    {
        $valid = $this->workingTimes->valid();
         
        if ($valid) {
            return true;
        }
        
        $limit = 7;
         
        do {
            $this->weekDays->next();

            if (!$this->weekDays->valid() && $this->weekDays->count() > 0) {
                
                // start with next week
                 
                $this->weekDays->rewind();
                if (!$this->weekDays->valid()) {
                    return false;
                }
            }
            

            $this->currentDate->add($this->addDayInterval);
            
           
            
            

            $this->weekDay = $this->weekDays->current();

            $this->workingTimes = $this->weekDay->selectWorkingTimes($this->orderMethod);
            $this->workingTimes->rewind();

            $limit--;

        } while (!$this->workingTimes->valid() && $limit > 0);
        

        return $this->workingTimes->valid();
    }
}
