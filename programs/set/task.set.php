<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
namespace Ovidentia\LibProject;




/**
 *
 * Fields are named according to the MSPDI format
 * 
 * @see ftp://ftp.zums.ac.ir/software/Microsoft%20Office%202003/DOCS/MSPDI.XSD
 *
 * @property \ORM_PkField         $UID
 * @property \ORM_IntField        $ID                  The position identifier of the task within the list of tasks.
 * @property \ORM_StringField     $Name                
 * @property \ORM_EnumField       $Type
 * @property \ORM_StringField     $WBS                The work breakdown structure code of the task.
 * @property \ORM_StringField     $WBSLevel
 * @property \ORM_IntField        $OutlineNumber
 * @property \ORM_IntField        $OutlineLevel
 * @property \ORM_IntField        $Priority            The priority of the task from 0 to 1000.
 * @property \ORM_DateTimeField   $Start                The scheduled start date of the task.
 * @property \ORM_DateTimeField   $Finish                The scheduled finish date of the task.
 * @property \ORM_StringField     $Duration            iso-8601 duration, The planned duration of the task.
 * @property \ORM_StringField     $Work                iso-8601 duration, The amount of scheduled work for the task.
 *                                                  Calculated or entered
 *                                                  total task work as the sum of the work that all assigned 
 *                                                  resources are to perform on a task. 
 *                                                  This is based on the assignment span, 
 *                                                  the number of resources assigned, and the assignment units 
 *                                                  for the assigned resources. Work includes 
 *                                                  actual work and remaining work, 
 *                                                  as well as overtime work.
 *
 * @property \ORM_StringField     $ActualWork          iso-8601 duration, The actual work for the task.
 * @property \ORM_DateTime        $Stop                The date that the task was stopped.
 * @property \ORM_DateTime        $Resume                The date that the task resumed.
 * @property \ORM_BoolField       $ResumeValid        Whether the task can be resumed.
 * 
 * @property \ORM_BoolField       $EffortDriven        Effort-driven means that when you assign or 
 *                                                     remove people from a task, 
 *                                                     Project lengthens or shortens the duration 
 *                                                     of the task based on 
 *                                                     the amount of resource units assigned to it, 
 *                                                     but it doesn't change 
 *                                                     the total amount of work for the task.
 *                                                     
 * @property \ORM_BoolField       $Recurring
 * @property \ORM_BoolField       $Estimated
 * @property \ORM_BoolField       $Milestone
 * @property \ORM_BoolField       $Summary            Whether the task is a summary task.
 * @property \ORM_BoolField       $Critical
 * 
 * @property \ORM_DateTime        $EarlyStart
 * @property \ORM_DateTime        $EarlyFinish
 *     
 * @property \ORM_DateTime        $LateStart
 * @property \ORM_DateTime        $LateFinish
 * 
 * @property \ORM_IntField        $PercentComplete        The percentage of the task duration completed.
 * @property \ORM_IntField        $PercentWorkComplete    The percentage of the task work completed.
 * @property \ORM_StringField     $OvertimeWork         iso-8601 duration, 
 *                                                      The amount of overtime work scheduled for the task. 
 *                                                      (calculated)
 *                                                      When a task is first created, the Overtime Work 
 *                                                      field shows "0 hrs." When you enter scheduled overtime 
 *                                                      work in the sheet portion of the Task Usage or 
 *                                                      Resource Usage view for the assignment, 
 *                                                      Microsoft Office Project rolls up these overtime 
 *                                                      work hours and enters the total overtime work for 
 *                                                      the task into this field.
 *
 * @property \ORM_DateTimeField   $ActualStart
 * @property \ORM_DateTimeField   $ActualFinish
 * @property \ORM_StringField     $ActualDuration            iso-8601 duration, The actual duration of the task.
 * @property \ORM_StringField     $ActualOvertimeWork        iso-8601 duration, The actual overtime work for the task. 
 *                                                          (calculated)
 *                                                          When you first create a task, the Actual Overtime Work 
 *                                                          field contains zero. As the assigned resources for the 
 *                                                          task report actual overtime work, it is entered into 
 *                                                         the Actual Overtime Work field.
 * 
 * @property \ORM_StringField     $RegularWork            iso-8601 duration, 
 *                                                      The amount of non-overtime work scheduled for the task.
 *                                                      
 * @property \ORM_StringField     $RemainingDuration        iso-8601 duration, 
 *                                                      The amount of time required to complete 
 *                                                      the unfinished portion of the task.
 *                                                      Remaining Duration = Duration - Actual Duration
 *                                                      Remaining Duration = Duration - (Duration * Percent Complete)
 *                                                      
 * @property \ORM_StringField     $RemainingWork            iso-8601 duration, 
 *                                                      The remaining work scheduled to complete the task.
 * @property \ORM_EnumField       $ConstraintType
 * @property \ORM_DateTimeField   $ConstraintDate            The date argument for the task constraint type.
 * @property \ORM_DateTimeField   $Deadline                The deadline for the task to be completed.
 * @property \ORM_StringField     $Hyperlink                The title of the hyperlink associated with the task.
 * @property \ORM_StringField     $HyperlinkAddress        The hyperlink associated with the task.
 * @property \ORM_BoolField       $IgnoreResourceCalendar
 * @property \ORM_TextField       $Notes
 * @property \ORM_BoolField       $Active
 * 
 * @property ProjectSet           $project
 * 
 * 
 * @method  \Ovidentia\LibProject\Task        newRecord()
 * 
 */
class TaskSet extends RecordSet
{

    /**
     * Field initialization
     */
    public function __construct()
    {
        parent::__construct();


        $this->addFields(
            ORM_PkField('UID')->setAutoIncrement(false),
            ORM_IntField('ID')
                ->index()
                ->setDescription('The position identifier of the task within the list of tasks.'),
            ORM_StringField('Name')->index(),
            ORM_EnumField('Type', $this->getTypes()),
            ORM_StringField('WBS')
                ->setDescription('The work breakdown structure code of the task.'),
            ORM_StringField('WBSLevel')
                ->setDescription('The rightmost WBS level of the task.'),
            ORM_IntField('OutlineNumber'),
            ORM_IntField('OutlineLevel'),
            ORM_IntField('Priority')
                ->setDescription('The priority of the task from 0 to 1000.'),
            ORM_DateTimeField('Start')
                ->setDescription('The scheduled start date of the task.'),
            ORM_DateTimeField('Finish')
                ->setDescription('The scheduled finish date of the task.'),
            ORM_StringField('Duration')                            // iso-8601 duration ex: PT112H0M0S
                ->setDescription('The planned duration of the task.'),
            ORM_StringField('Work')                                // iso-8601 duration ex: PT112H0M0S
                ->setDescription('The amount of scheduled work for the task.'),
            ORM_StringField('ActualWork')                          // iso-8601 duration ex: PT112H0M0S
                ->setDescription('The amount of actual work for the task.'),
            ORM_DateTimeField('Stop')
                ->setDescription('The date that the task was stopped.'),
            ORM_DateTimeField('Resume')
                ->setDescription('The date that the task resumed.'),
            ORM_BoolField('ResumeValid')
                ->setOutputOptions(translate('No'), translate('Yes'))
                ->setDescription('Whether the task can be resumed.'),
            ORM_BoolField('EffortDriven')
                ->setOutputOptions(translate('No'), translate('Yes')),
            ORM_BoolField('Recurring')
                ->setOutputOptions(translate('No'), translate('Yes')),
            ORM_BoolField('Estimated')
                ->setOutputOptions(translate('No'), translate('Yes')),
            ORM_BoolField('Milestone')
                ->setOutputOptions(translate('No'), translate('Yes')),
            ORM_BoolField('Summary')
                ->setOutputOptions(translate('No'), translate('Yes'))
                ->setDescription('Whether the task is a summary task.'),
            ORM_BoolField('Critical')
                ->setOutputOptions(translate('No'), translate('Yes')),
            ORM_DateTimeField('EarlyStart'),
            ORM_DateTimeField('EarlyFinish'),
            ORM_DateTimeField('LateStart'),
            ORM_DateTimeField('LateFinish'),
            ORM_intField('PercentComplete')
                ->setDescription('The percentage of the task duration completed.'),
            ORM_intField('PercentWorkComplete')
                ->setDescription('The percentage of the task work completed.'),
            ORM_StringField('OvertimeWork')     // iso-8601 duration
                ->setDescription('The amount of overtime work scheduled for the task.'),
            ORM_DateTimeField('ActualStart'),
            ORM_DateTimeField('ActualFinish'),
            ORM_StringField('ActualDuration')        // iso-8601 duration
                ->setDescription('The actual duration of the task.'),
            ORM_StringField('ActualOvertimeWork')     // iso-8601 duration
                ->setDescription('The actual overtime work for the task.'),
            ORM_StringField('RegularWork')            // iso-8601 duration
                ->setDescription('The amount of non-overtime work scheduled for the task.'),
            ORM_StringField('RemainingDuration')    // iso-8601 duration
                ->setDescription('The amount of time required to complete the unfinished portion of the task.'),
            ORM_StringField('RemainingWork')        // iso-8601 duration
                ->setDescription('The remaining work scheduled to complete the task.'),
            ORM_EnumField('ConstraintType', $this->getConstraintTypes()),
            ORM_DateTimeField('ConstraintDate')
                ->setDescription('The date argument for the task constraint type.'),
            ORM_DateTimeField('Deadline')
                ->setDescription('The deadline for the task to be completed.'),
            ORM_StringField('Hyperlink')
                ->setDescription('The title of the hyperlink associated with the task.'),
            ORM_StringField('HyperlinkAddress')
                ->setDescription('The hyperlink associated with the task.'),
            ORM_BoolField('IgnoreResourceCalendar')
                ->setOutputOptions(translate('No'), translate('Yes')),
            ORm_TextField('Notes'),
            ORM_BoolField('Active')
                ->setOutputOptions(translate('No'), translate('Yes'))
                ->index()
        );

        $this->setPrimaryKey('UID');

        $this->hasOne('project', '\Ovidentia\LibProject\ProjectSet', 'ORM_StringField')->index(); // parent in XML
    }

    /**
     * Get task types
     * 
     * Project uses task type settings to calculate the work, duration, and resource 
     * units for a task. By applying a task type setting to tasks, you can control how 
     * Project calculates the other two formula values as you modify your schedule.
     * 
     * The default task type setting when Project calculates duration is Fixed Units.
     * 
     * @return array
     */
    public function getTypes()
    {
        return array(
            Task::FIXED_UNITS         => translate('Fixed Units'),
            Task::FIXED_DURATION      => translate('Fixed Duration'),
            Task::FIXED_WORK          => translate('Fixed Work')
        );
    }
    
    /**
     * Constraint types to use associated with the ConstraintDate field
     * @return array
     */
    public function getConstraintTypes()
    {
        return array(
            Task::ASAP                         => translate('As Soon As Possible'),
            Task::ALAP                         => translate('As Late As Possible'),
            Task::START_ON                     => translate('Must Start On'),
            Task::FINISH_ON                    => translate('Must Finish On'),
            Task::START_NO_EARLIER_THAN        => translate('Start No Earlier Than'),
            Task::START_NO_LATER_THAN          => translate('Start No Later Than'),
            Task::FINISH_NO_EARLIER_THAN       => translate('Finish No Earlier Than'),
            Task::FINISH_NO_LATER_THAN         => translate('Finish No Later Than')
        );
    }
    
    /**
     * Save
     * Verify mandatory fields
     * @return bool
     */
    public function save(Task $record)
    {
        if (empty($record->Name)) {
            throw new MandatoryFieldException('Name is mandatory');
        }
        
        if (empty($record->project)) {
            throw new MandatoryFieldException('project is mandatory');
        }
        
        // actual dates must satisfy constraint
        if (!$record->isConstraintSatisfied(Task::ACTUAL)) {
            throw new TaskConstraintException('Constraint unsatisfied for actual dates');
        }

        return parent::save($record);
    }
}
