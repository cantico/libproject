<?php
// -------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
// -------------------------------------------------------------------------
/**
 *
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
namespace Ovidentia\LibProject;

/**
 * a project path is a sequence of linked tasks
 * 
 * 
 */
class Path
{
    /**
     * Loaded task records in path
     * @var array
     */
    protected $tasks = array();
    
    /**
     * @var string
     */
    protected $start = '9999-00-00 00:00:00';
    
    /**
     * @var string
     */
    protected $finish = '0000-00-00 00:00:00';
    
    /**
     * Loaded successors links for tasks
     * successors tasks should be modified if the task is modified to fullfill the links contraints
     * @var array
     */
    protected $successor = array();
    
    /**
     * Loaded predecessors links for tasks
     * procecessors add contraints on task modification
     * @var array
     */
    protected $predecessor = array();
    
    
    /**
     * Load a path from a task
     * Return true if a path has been loaded
     * or false if the task is an orphan
     * 
     * @param Task $task
     * 
     * @return boolean
     */
    public function loadFromTask(Task $task)
    {
        $this->addTask($task, true, true);
        
        if (1 === count($this->tasks)) {
            return false;
        }
        
        return true;
    }
    
    
    
    /**
     * Load a upper path from a task
     * Return true if a path has been loaded
     * or false if the task is an orphan
     *
     * @param Task $task
     *
     * @return boolean
     */
    public function loadUpperFromTask(Task $task)
    {
        $this->addTask($task, true, false);
    
        if (1 === count($this->tasks)) {
            return false;
        }
    
        return true;
    }
    
    
    /**
     * Load a lower path from a task
     * Return true if a path has been loaded
     * or false if the task is an orphan
     *
     * @param Task $task
     *
     * @return boolean
     */
    public function loadLowerFromTask(Task $task)
    {
        $this->addTask($task, false, true);
    
        if (1 === count($this->tasks)) {
            return false;
        }
    
        return true;
    }
    
    
    
    
    /**
     * Test if the path contain a task
     * 
     * @param Task $task
     * 
     * @return bool
     */
    public function containTask(Task $task)
    {
        if (isset($this->tasks[$task->UID])) {
            return true;
        }
        
        return false;
    }
    
    
    /**
     * Load a path recursively from a task
     * 
     * @param Task $task
     * @param bool $upper
     * @param bool $lower
     */
    protected function addTask(Task $task, $upper, $lower)
    {
        if (!isset($task->UID)) {
            throw new TaskPathException('Failed to load a task without UID in path');
        }
        
        if (isset($this->tasks[$task->UID])) {
            throw new TaskPathException(sprintf('Task %s allready loaded in path', $task->UID));
        }
        
        $this->tasks[$task->UID] = $task;
        
        if ($task->ActualStart < $this->start) {
            $this->start = $task->ActualStart;
        }
        
        if ($task->ActualFinish > $this->finish) {
            $this->finish = $task->ActualFinish;
        }
        
        try {
            $upper && $this->loadPredecessorsLinks($task);
            $lower && $this->loadSucessorsLinks($task);
            
        } catch (TaskPathException $exception) {
            // ignore allready inserted tasks
        }
    }
    
    /**
     * Store every links in path
     * @param PredecessorLink $link
     */
    protected function addLink(PredecessorLink $link)
    {
        $predecessor = $link->PredecessorUID;
        $sucessor = $link->task;
        
        if (($predecessor instanceof Task) && ($sucessor instanceof Task)) {
            $this->successor[$predecessor->UID][$link->task->UID] = $link;
            $this->predecessor[$sucessor->UID][$link->task->UID] = $link;
        }
    }
    
    
    /**
     * Load upper tasks
     * Do addTask for all upper tasks
     * 
     * @param Task task
     * @throws TaskPathException
     */
    private function loadPredecessorsLinks(Task $task)
    {
        foreach ($task->selectPredecessors() as $link) {
            /* @var $link PredecessorLink */
            $this->addLink($link);
            $predecessor = $link->PredecessorUID;
        
            if (($predecessor instanceof Task)) {
                $this->addTask($predecessor, true, true);
            }
        }
    }
    
    /**
     * Load lower tasks
     * Do addTask for all upper tasks
     * 
     * 
     * @param Task task
     * @throws TaskPathException
     */
    private function loadSucessorsLinks(Task $task)
    {
        foreach ($task->selectSuccessors() as $link) {
            /* @var $link PredecessorLink */
            $this->addLink($link);
            $sucessor = $link->task;
        
            if (($sucessor instanceof Task)) {
                $this->addTask($sucessor, true, true);
            }
        }
    }
    
    
    
    /**
     * Load tasks
     * do addTask for lower and upper tasks
     * 
     * @param Task task
     * @throws TaskPathException
     */
    private function loadLinks(Task $task)
    {
        $this->loadPredecessorsLinks($task);
        $this->loadSucessorsLinks($task);
    }
    
    
    /**
     * Get the first actual start date in path
     * @return string
     */
    public function getStart()
    {
        if ('9999-00-00 00:00:00' === $this->start) {
            return null;
        }
        
        return $this->start;
    }
    
    /**
     * Get the last finish actual date in path
     * @return string
     */
    public function getFinish()
    {
        if ('0000-00-00 00:00:00' === $this->finish) {
            return null;
        }
        
        return $this->finish;
    }
    
    /**
     * Get list of tasks in conflict with the new actual period for a task in the path
     *
     * @param Task $task
     * @param string $newStart      actual start date to set on the task
     * @param string $newFinish      actual finish date to set on the task
     *
     * @return array
     */
    protected function getConflictsTasks(Task $task, $newStart, $newFinish)
    {
        if (!isset($this->tasks[$task->UID])) {
            throw new \Exception('Task does not exists in path');
        }
        
        if (0 === count($this->predecessor[$task->UID])) {
            return array();
        }
        
        $conflicts = array();
        foreach ($this->predecessor[$task->UID] as $link) {
            if ($this->isInConflictWithPredecessor($link, $newStart, $newFinish)) {
                $conflicts[] = $link->PredecessorUID;
            }
        }
        
        return $conflicts;
    }
    
    
    
    /**
     * Test if dates are in conflict with a task because of a relation type constraint
     * 
     * @param PredecessorLink $link     
     * @param string $newStart          actual start date to set on the task
     * @param string $newFinish         actual finish date to set on the task
     * 
     * @return bool
     */
    private function isInConflictWithPredecessor(PredecessorLink $link, $newStart, $newFinish)
    {
        $predecessor = $link->PredecessorUID;
        $actualStart = $predecessor->ActualStart;
        $actualFinish = $predecessor->ActualFinish;
        
        $type = (int) $link->Type;
        
        switch($type) {
            case PredecessorLink::FF: // Predecessor task actual finish date must be <= $newFinish
                return ($actualFinish > $newFinish);
            case PredecessorLink::FS: // Predecessor task actual finish date must be <= $newStart
                return ($actualFinish > $newStart);
            case PredecessorLink::SF: // Predecessor task actual start date must be <= $newFinish
                return ($actualStart > $newFinish);
            case PredecessorLink::SS: // Predecessor task actual start date must be <= $newStart
                return ($actualStart > $newStart);
        }
        
        return false;
    }
    
    
    /**
     * Get dates boundaries to fullfill the predecessors constraints
     * @param PredecessorLink $link
     * @return PeriodBoundaries
     */
    private function getPredecessorDatesBoundaries(PredecessorLink $link)
    {
        $predecessor = $link->PredecessorUID;
        $actualStart = $predecessor->ActualStart;
        $actualFinish = $predecessor->ActualFinish;
        
        
        
        $type = (int) $link->Type;

        $boundaries = new PeriodBoundaries();

        switch($type) {
            case PredecessorLink::FF: // Predecessor task actual finish date must be <= $newFinish
                $boundaries->setFinishMin($actualFinish);
                break;
            case PredecessorLink::FS: // Predecessor task actual finish date must be <= $newStart
                $boundaries->setStartMin($actualFinish);
                $boundaries->setFinishMin($actualFinish);
                break;
            case PredecessorLink::SF: // Predecessor task actual start date must be <= $newFinish
                $boundaries->setFinishMin($actualStart);
                break;
            case PredecessorLink::SS: // Predecessor task actual start date must be <= $newStart
                $boundaries->setStartMin($actualStart);
                $boundaries->setFinishMin($actualStart);
                break;
        }
        
        return $boundaries;
    }
    
    
    /**
     * Get dates boundaries for a task according to all predecessors
     * @param Task $task
     * @throws \Exception
     * 
     * @return PeriodBoundaries
     */
    public function getDatesBoundaries(Task $task)
    {
        if (!isset($this->tasks[$task->UID])) {
            throw new \Exception('Task does not exists in path');
        }
        
        $boundaries = new PeriodBoundaries();
        
        if (!isset($this->predecessor[$task->UID]) || 0 === count($this->predecessor[$task->UID])) {
            return $boundaries;
        }

        foreach ($this->predecessor[$task->UID] as $link) {
            $boundaries->combine($this->getPredecessorDatesBoundaries($link));
        }

        return $boundaries;
    }
    
    
    
    /**
     * Test predessecors constraints for a new period on the task
     * 
     * @param Task $task
     * @param string $newStart      actual start date to set on the task
     * @param string $newFinish      actual finish date to set on the task
     * 
     * @return bool
     */
    public function isConstraintSatisfied(Task $task, $newStart, $newFinish)
    {
        $tasks = $this->getConflictsTasks($task, $newStart, $newFinish);
        
        return (0 === count($tasks));
    }
    
    
    /**
     * Task dates has been modified.
     * update the other tasks in path according to this date
     * 
     * @param Task $task
     */
    public function onTaskUpdated(Task $task)
    {
        //TODO update other tasks in path
    }
}
