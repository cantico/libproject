<?php
// -------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
// -------------------------------------------------------------------------
/**
 *
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
namespace Ovidentia\LibProject;


/**
 * Project statistics
 */
class ProjectStatistics
{
    /**
     * @var Project
     */
    private $project;
    
    /**
     * @var array
     */
    private $tasksStatistics;
    
    /**
     * sum of variable to get average for
     * @var array
     */
    private $average;
    
    /**
     * list of durations to get sum for
     * @var array
     */
    private $duration;
    
    
    /**
     * @param \Ovidentia\LibProject\Project  $project
     */
    public function __construct(Project $project)
    {
        $this->project = $project;
    }
    
    /**
     * Get default statistics
     * will be used as task statistics in no tasks on project
     * @return array
     */
    private function getDefault()
    {
        return array(
            'Duration' => new \DateInterval('P0D'),
            'Work' => new \DateInterval('P0D'),
            'ActualWork' => new \DateInterval('P0D'),
            'Start' => '9999-00-00 00:00:00',
            'Finish' => '0000-00-00 00:00:00',
            'ActualStart' => '9999-00-00 00:00:00',
            'ActualFinish' => '0000-00-00 00:00:00',
            'ActualDuration' => new \DateInterval('P0D'),
            'ActualOvertimeWork' => new \DateInterval('P0D'),
            'RegularWork' => new \DateInterval('P0D'),
            'EarlyStart' => '0000-00-00 00:00:00',
            'LateFinish' => '0000-00-00 00:00:00',
            'PercentComplete' => 0,
            'PercentWorkComplete' => 0,
            'OvertimeWork' => new \DateInterval('P0D'),
            'RemainingDuration' => new \DateInterval('P0D'),
            'RemainingWork' => new \DateInterval('P0D')
        );
    }
    
    
    /**
     * Add one task to the stats
     * @param Task $task
     */
    private function addTask(Task $task)
    {
        $this->addAverage('PercentComplete', $task->PercentComplete);
        $this->addAverage('PercentWorkComplete', $task->PercentWorkComplete);
        
        $this->addDuration('Duration', $task->Duration);
        $this->addDuration('Work', $task->Work);
        $this->addDuration('ActualWork', $task->ActualWork);
        $this->addDuration('ActualDuration', $task->ActualDuration);
        $this->addDuration('ActualOvertimeWork', $task->ActualOvertimeWork);
        $this->addDuration('RegularWork', $task->RegularWork);
        $this->addDuration('OvertimeWork', $task->OvertimeWork);
        $this->addDuration('RemainingDuration', $task->RemainingDuration);
        $this->addDuration('RemainingWork', $task->RemainingWork);
        
        if ($task->Start < $this->tasksStatistics['Start']) {
            $this->tasksStatistics['Start'] = $task->Start;
        }
        
        if ($task->Finish > $this->tasksStatistics['Finish']) {
            $this->tasksStatistics['Finish'] = $task->Finish;
        }
        
        if ($task->ActualStart < $this->tasksStatistics['ActualStart']) {
            $this->tasksStatistics['ActualStart'] = $task->ActualStart;
        }
        
        if ($task->ActualFinish > $this->tasksStatistics['ActualFinish']) {
            $this->tasksStatistics['ActualFinish'] = $task->ActualFinish;
        }
        
        if ($task->EarlyStart < $this->tasksStatistics['EarlyStart']) {
            $this->tasksStatistics['EarlyStart'] = $task->EarlyStart;
        }
        
        if ($task->LateFinish > $this->tasksStatistics['LateFinish']) {
            $this->tasksStatistics['LateFinish'] = $task->LateFinish;
        }
    }
    
    /**
     * Compute statistics from the list of task
     * 
     * @return array
     */
    private function getTasksStatistics()
    {
        if (!isset($this->tasksStatistics)) {
            
            $this->tasksStatistics = $this->getDefault();
            
            
            $tasks = $this->project->selectActiveTasks();
            $this->average = array();
            $this->duration = array();
            
            foreach ($tasks as $task) {
                $this->addTask($task);
            }
            
            
            if ($tasks->count() > 0) {
                if ('9999-00-00 00:00:00' === $this->tasksStatistics['ActualStart']) {
                    throw new Exception('Invalid actual start date');
                }
                
                $this->tasksStatistics = array_merge(
                    $this->tasksStatistics,
                    $this->getAverages($tasks->count())
                );
                
                $this->tasksStatistics = array_merge(
                    $this->tasksStatistics,
                    $this->getDurations($this->tasksStatistics['ActualStart'])
                );
            }
        }
        
        return $this->tasksStatistics;
    }
    
    /**
     * Sum a value into average array
     * @param string $name
     * @param float $value
     */
    private function addAverage($name, $value)
    {
        if (!isset($this->average[$name])) {
            
            $this->average[$name] = 0;
            
        }
        
        if (is_numeric($value)) {
            $this->average[$name] += $value;
        } else {
            
        }
    }
    
    /**
     * Sum a value into average array
     * @param string $name
     * @param string $value      A iso-8601 duration
     */
    private function addDuration($name, $value)
    {
        if (empty($value)) {
            return;
        }
        
        if (!isset($this->duration[$name])) {
            $this->duration[$name] = array();
        }
        
        $this->duration[$name][] = new \DateInterval($value);
    }
    
    
    
    /**
     * get the list of averages values
     * 
     * @param int $count            total number of tasks involved
     * 
     * @return array
     */
    private function getAverages($count)
    {
        $arr = array();
        foreach ($this->average as $name => $all) {
            $arr[$name] = ($all/$count);
        }
        
        return $arr;
    }
    
    
    /**
     * get the list of duration sums values
     *
     * @param string $startDate     Start date used to compute the duration sum
     *
     * @return array
     */
    private function getDurations($startDate)
    {
        $arr = array();
        
        $startInterval = new \DateTime($startDate);
        
        foreach ($this->duration as $name => $all) {

            $endInterval = new \DateTime($startDate);
    
            foreach ($all as $duration) {
                $endInterval->add($duration);
            }
    
            $arr[$name] = $startInterval->diff($endInterval);
        }
        
        return $arr;
    }
    
    
    /**
     * Get sum of planned duration on active tasks
     * @return \DateInterval
     */
    public function getDuration()
    {
        $arr = $this->getTasksStatistics();
        return $arr['Duration'];
    }
    
    
    /**
     * Get sum of scheduled work on active tasks
     * @return \DateInterval
     */
    public function getWork()
    {
        $arr = $this->getTasksStatistics();
        return $arr['Work'];
    }
    
    
    /**
     * Get sum of actual work on active tasks
     * @return \DateInterval
     */
    public function getActualWork()
    {
        $arr = $this->getTasksStatistics();
        return $arr['ActualWork'];
    }
    
    
    /**
     * Get sum of task actual durations
     * @return \DateInterval
     */
    public function getActualDuration()
    {
        $arr = $this->getTasksStatistics();
        return $arr['ActualDuration'];
    }
    
    /**
     * Get sum of task actual overtime work durations
     * @return \DateInterval
     */
    public function getActualOvertimeWork()
    {
        $arr = $this->getTasksStatistics();
        return $arr['ActualOvertimeWork'];
    }
    
    
    /**
     * Get the amount of non-overtime work scheduled on all active tasks
     * @return \DateInterval
     */
    public function getRegularWork()
    {
        $arr = $this->getTasksStatistics();
        return $arr['RegularWork'];
    }
    
    
    /**
     * Get sum of task planned overtime work durations
     * @return \DateInterval
     */
    public function getOvertimeWork()
    {
        $arr = $this->getTasksStatistics();
        return $arr['OvertimeWork'];
    }
    
    
    /**
     * Get sum of task remaining durations
     * @return \DateInterval
     */
    public function getRemainingDuration()
    {
        $arr = $this->getTasksStatistics();
        return $arr['RemainingDuration'];
    }
    
    
    /**
     * Get sum of task remaining work durations
     * @return \DateInterval
     */
    public function getRemainingWork()
    {
        $arr = $this->getTasksStatistics();
        return $arr['RemainingWork'];
    }
    
    
    
    /**
     * Return the first start date from the list of task
     * @return string
     */
    public function getStart()
    {
        $arr = $this->getTasksStatistics();
        return $arr['Start'];
    }
    
    
    /**
     * Return the last finish date from the list of task
     * @return string
     */
    public function getFinish()
    {
        $arr = $this->getTasksStatistics();
        return $arr['Finish'];
    }
    
    
    /**
     * Return the first actual start date from the list of task
     * @return string
     */
    public function getActualStart()
    {
        $arr = $this->getTasksStatistics();
        return $arr['ActualStart'];
    }
    
    
    /**
     * Return the last actual finish date from the list of task
     * @return string
     */
    public function getActualFinish()
    {
        $arr = $this->getTasksStatistics();
        return $arr['ActualFinish'];
    }
    
    
    /**
     * Return the first early start date from the list of task
     * @return string
     */
    public function getEarlyStart()
    {
        $arr = $this->getTasksStatistics();
        return $arr['EarlyStart'];
    }
    
    
    /**
     * Return the last late finish date from the list of task
     * @return string
     */
    public function getLateFinish()
    {
        $arr = $this->getTasksStatistics();
        return $arr['LateFinish'];
    }
    
    /**
     * Get percent complete average
     * @return float
     */
    public function getPercentComplete()
    {
        $arr = $this->getTasksStatistics();
        return $arr['PercentComplete'];
    }
    
    /**
     * Get percent work complete average
     * @return float
     */
    public function getPercentWorkComplete()
    {
        $arr = $this->getTasksStatistics();
        return $arr['PercentWorkComplete'];
    }
}
