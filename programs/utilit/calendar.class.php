<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */

namespace Ovidentia\LibProject;


/**
 * Object used to get personal calendar informations from ovidentia
 *
 */
class OvidentiaCalendar
{
    private $user;

    /**
     * 
     * @param int $id_user        Ovidentia ID user
     */
    public function __construct($id_user)
    {
        $this->user = $id_user;
    }

    /**
     * Select periods from the calendar API
     *
     * @param    string             $collectionClassName        bab_NonWorkingDaysCollection
     *                                                      | bab_WorkingPeriodCollection
     *                                                      | bab_VacationPeriodCollection
     * @param    \BAB_DateTime    $begin
     * @param    \BAB_DateTime    $end
     *
     * @return Iterator
     */
    private function selectUserPeriods($collectionClassName, \BAB_DateTime $begin, \BAB_DateTime $end)
    {
        include_once $GLOBALS['babInstallPath']."utilit/utilit.php";
        include_once $GLOBALS['babInstallPath']."utilit/workinghoursincl.php";
        include_once $GLOBALS['babInstallPath']."utilit/calincl.php";

        $obj = new bab_UserPeriods(
            $begin,
            $end
        );

        $factory = \bab_getInstance('bab_PeriodCriteriaFactory');
        /* @var $factory bab_PeriodCriteriaFactory */

        $criteria = $factory->Collection(
            array($collectionClassName)
        );

        $icalendars = \bab_getICalendars($this->user);

        $calendar = $icalendars->getPersonalCalendar();

        if (!isset($calendar)) {
            // the user personal calendar is not accessible
            // create an instance

            $calendar = \bab_functionality::get('CalendarBackend')->PersonalCalendar($this->user);
        }

        $criteria = $criteria->_AND_($factory->Calendar($calendar));

        $obj->createPeriods($criteria);
        $obj->orderBoundaries();

        return $obj;
    }
    
    /**
     * @property Project $project
     * @return Array
     */
    private function getBoundaries(Project $project)
    {
        if ('0000-00-00 00:00:00' === $project->StartDate || '0000-00-00 00:00:00' === $project->FinishDate) {
            throw new ProjectBoundariesException(
                'Failed to select the non working days of a user because of unknown project boundaries'
            );
        }
        
        $dateb = \BAB_DateTime::fromIsoDateTime($project->StartDate);
        $datee = \BAB_DateTime::fromIsoDateTime($project->FinishDate);
        
        return array($dateb, $datee);
    }
    

    /**
     * selectionner les jours feries sur la periode du projet
     * 
     * @property Project $project
     */
    public function selectNonWorkingDays(Project $project)
    {
        list($dateb, $datee) = $this->getBoundaries($project);
        return $this->selectUserPeriods('bab_NonWorkingDaysCollection', $dateb, $datee);
    }

    /**
     * selectionner les jours de conges sur la periode du projet
     * 
     * @property Project $project
     */
    public function selectVacationPeriods(Project $project)
    {
        list($dateb, $datee) = $this->getBoundaries($project);
        return $this->selectUserPeriods('bab_VacationPeriodCollection', $dateb, $datee);
    }

    /**
     * selectionner les periodes de travail sur une semaine
     */
    public function selectWorkingPeriods()
    {
        $dateb = \BAB_DateTime::fromTimeStamp(strtotime('Monday'));
        $datee = clone $dateb;
        $datee->add(7, BAB_DATETIME_DAY);

        return $this->selectUserPeriods('bab_WorkingPeriodCollection', $dateb, $datee);
    }
}
