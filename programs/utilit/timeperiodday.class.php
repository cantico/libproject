<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * 
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
namespace Ovidentia\LibProject;


/**
 * This object represent a day with timeperiod exceptions (non-working day or vacations)
 *
 */
class TimePeriodDay
{
    /**
     * 
     * @var Calendar
     */
    protected $calendar;
    
    
    /**
     * date used as index by the getExceptionDays() method
     * @see Calendar::getExceptionDays()
     * @var string     0000-00-00
     */
    public $day;
    
    /**
     * list of timeperiods found in this day 
     * timeperiods must be added in a chronological order (by start date)
     * 
     * @var array
     */
    public $timeperiod = array();
    
    /**
     * @var bool
     */
    private $worked;
    
    /**
     * Periods uncovered by the exceptions, cache
     * @var array
     */
    private $uncoveredPeriods;
    
    
    /**
     * 
     * @param Calendar $calendar
     * @param string $day            0000-00-00
     */
    public function __construct(Calendar $calendar, $day)
    {
        $this->calendar = $calendar;
        $this->day = $day;
    }
    
    
    /**
     * Search for periods uncovered by the exception in the day
     * @return array
     */
    public function getUncoveredPeriods()
    {
        
        if (!isset($this->uncoveredPeriods)) {
        
            $this->uncoveredPeriods = array();
            $loop = new \DateTime($this->day.' 00:00:00');
            $end = clone $loop;
            $end->add(new \DateInterval('P1D'));
            
            foreach ($this->timeperiod as $timeperiod) {
                
                /*@var $timeperiod TimePeriod */
                $fromdate = new \DateTime($timeperiod->FromDate);
                $todate = new \DateTime($timeperiod->ToDate);
    
                if ($fromdate > $loop) {
                    $this->uncoveredPeriods[] = array($loop, $fromdate);
                }
                
                if ($todate > $loop) {
                    $loop = $todate;
                }
            }
            
            if ($loop < $end) {
                $this->uncoveredPeriods[] = array($loop, $end);
            }
        
        }
        
        return $this->uncoveredPeriods;
    }
    
    
    
    /**
     * Test if the day is worked or not
     * the day will not be worked if the timeperiod exceptions covers all the workingtimes period that day
     * 
     * @return boolean
     */
    public function isWorked()
    {
        if (!isset($this->worked)) {
            $this->worked = false;
            
            $uncovered = $this->getUncoveredPeriods();
            
            if (0 !== count($uncovered)) {
                
                $workingTimes = $this->getWorkingTimeDates();
                
                if (0 === count($workingTimes)) {
                    return $this->worked;
                }
                
                
                
                if ($this->isWorkingTimesOverlapUncovered($workingTimes, $uncovered)) {
                    $this->worked = true;
                }
            }
        }
        
        
        return $this->worked;
    }
    
    
    /**
     * Get the list of schedulable periods 
     * the working times periods intersected with the periods uncovered by the exceptions
     * 
     * @return array
     */
    public function getSchedulablePeriods()
    {
        $return = array();
        
        $uncovered = $this->getUncoveredPeriods();
        $workingTimes = $this->getWorkingTimeDates();
        
        foreach ($workingTimes as $WTDate) {
            /* @var $WTDate WorkingTimeDate */
        
            foreach ($uncovered as $uncoverdPeriod) {
        
                $intersection = $WTDate->getIntersection($uncoverdPeriod[0], $uncoverdPeriod[1]);
                
                if (isset($intersection)) {
                    $period = new SchedulablePeriod();
                    $period->fromDate = $intersection[0];
                    $period->toDate = $intersection[1];
                    
                    $return[] = $period;
                }
            }
        }
        
        return $return;
    }
    
    
    
    /**
     * Get working times of this day
     * @return array
     */
    public function getWorkingTimeDates()
    {
        $return = array();
        $workingTimes = new WorkingTimes($this->calendar, new \DateTime($this->day.' 00:00:00'));
        
        foreach ($workingTimes as $WTDate) {
            /* @var $WTDate WorkingTimeDate */
            
            if ($this->day !== $WTDate->dateTime->format('Y-m-d')) {
                return $return;
            }
            
            $return[] = $WTDate;
        }
        
        return $return;
    }
    
    
    /**
     * Test if the working times schedule overlap the periods uncoverd by the timeperiod exception
     * 
     * @param array $workingTimes
     * @param array $uncovered
     * @return boolean
     */
    private function isWorkingTimesOverlapUncovered(Array $workingTimes, Array $uncovered)
    {
        foreach ($workingTimes as $WTDate) {
            /* @var $WTDate WorkingTimeDate */
            
            foreach ($uncovered as $uncoverdPeriod) {
        
                if ($WTDate->isOverlapedBy($uncoverdPeriod[0], $uncoverdPeriod[1])) {
                    return true;
                }
            }
        }
        
        return false;
    }
}
