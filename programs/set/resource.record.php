<?php
// -------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
// -------------------------------------------------------------------------
/**
 *
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
namespace Ovidentia\LibProject;

/**
 *
 * @property int $UID
 * @property int $ID
 * @property string $Name
 * @property int $Type
 * @property int $IsNull
 * @property string $Initials
 * @property string $MaterialLabel
 * @property string $Group
 * @property int $WorkGroup
 * @property string $EmailAddress
 * @property string $Notes
 * @property string $CreationDate
 * @property int $user
 * @property Project $project
 * @property Calendar $calendar
 * 
 * @method ResourceSet getParentSet()
 */
class Resource extends Record
{
    
    const MATERIAL = 0;
    const WORK = 1;
    
    const WG_DEFAULT = 0;
    const WG_NONE = 1;
    const WG_EMAIL = 2;
    const WG_WEB = 3;
    

    /**
     * Update ressource fields using informations of the linked ovidentia user
     * 
     * @throws Exception
     *
     * @return \Ovidentia\LibProject\Resource
     */
    protected function updateUser()
    {
        if (! $this->user) {
            throw new Exception('Missing user id');
        }
        
        $this->Name = bab_getUserName($this->user);
        $this->Type = 1;
        $this->IsNull = empty($this->Name) ? 1 : 0;
        $this->Initials = bab_abbr($this->Name, BAB_ABBR_INITIAL, 10);
        $this->EmailAddress = bab_getUserEmail($this->user);
        
        return $this;
    }

    /**
     * Delete availability periods of the resource
     */
    protected function deleteAvailability()
    {
        require_once dirname(__FILE__) . '/resource_availability.class.php';
        $set = new Ovidentia\LibProject\AvailabilityPeriodSet();
        $set->delete($set->resource->is($this->UID));
    }
    
    /**
     * Delete calendar associated to project
     */
    protected function deleteCalendar()
    {
        require_once dirname(__FILE__) . '/calendar.class.php';
        $set = new CalendarSet();
        $set->delete($set->resource->is($this->UID));
    }

    /**
     * Get OvidentiaCalendar associated to the user logged in
     * The object is used to query ovidentia personal calendar information
     * @throws Exception
     * 
     * @return OvidentiaCalendar
     */
    protected function getUserCalendar()
    {
        if (! $this->user) {
            throw new Exception('Missing user id');
        }
        
        require_once dirname(__FILE__) . '/../utilit/calendar.class.php';
        
        return new OvidentiaCalendar($this->user);
    }

    /**
     * Create a calendar associated to the personnal resource
     */
    protected function insertUserCalendar()
    {
        $calendarSet = Storage()->CalendarSet();
        
        $calendar = $calendarSet->newRecord();
        $calendar->save();
        
        $this->calendar = $calendar->id;
        
        /*@var $calendar Calendar */
        
        $ovical = $this->getUserCalendar();
        
        $calendar->user()->insertWorkingTimes($ovical);
        $calendar->user()->insertNonWorkingDays($ovical);
        $calendar->user()->insertVacationPeriods($ovical);
    }

    /**
     * Reset the user availability periods according to the ovidentia user options
     *
     * Pas utile pour une ressource utilisateur ???
     * Les periodes de dispo doivent etre parametres manuellement pour une ressource materielle
     *
     * @todo voir si on peut faire un lien avec l'outil de gestion des ressources
     *      
     * @throws Exception
     */
    public function resetUserAvailability()
    {
        if (! $this->user) {
            throw new Exception('Missing user id');
        }
        
        $this->deleteAvailability();
        // nothing to insert for a user
    }

    /**
     * Reset the calendar associated to the ressource and the resource informations
     * with the parameter of the linked user
     *
     * @return bool
     */
    public function resetUserResource()
    {
        if (! $this->user) {
            throw new Exception('Missing user id');
        }
        
        $this->updateUser();
        $this->deleteCalendar();
        $this->insertUserCalendar();
        
        $this->save();
        
        return true;
    }

    /**
     * Receive the parent record
     * use this method to update links in record
     */
    public function updateParentByXml(Record $parent)
    {
        $this->project = $parent->uuid;
    }
}
