<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
namespace Ovidentia\LibProject;


/**
 * Test for tasks and task predecessor links
 *
 */
class TaskTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @beforeClass
     */
    public static function loadMockObjects()
    {
        require_once dirname(__FILE__).'/../mockObjects.php';
    }

    /**
     * @expectedException \Ovidentia\LibProject\MandatoryFieldException
     */
    public function testTaskMandatoryName()
    {
        $storage = Storage();
        $task = $storage->TaskSet()->newRecord();
        $task->Name = '';
        $task->project = bab_uuid();
        $task->save();
    }
    
    
    public function testTaskMockLinks()
    {
        list($task1, $task2) = MockTwoRelatedTasks();
        /*@var $task1 Task */
        /*@var $task2 Task */
        
        $successors = $task1->selectSuccessors();
        $this->assertEquals($successors->count(), 1);
        
        $predecessors = $task2->selectPredecessors();
        $this->assertEquals($predecessors->count(), 1);
    }
    
    
    public function testTaskPredecessorDefaultProperties()
    {
        list($task1, $task2) = MockTwoRelatedTasks();
        /*@var $task1 Task */
        /*@var $task2 Task */
    
        foreach ($task2->selectPredecessors() as $predecessor) {
            /*@var $predecessor PredecessorLink */
    
            $this->assertEquals($predecessor->Type, (string) PredecessorLink::FS);
            $this->assertEquals($task1, $predecessor->PredecessorUID);
        }
    }
    
    
    
    public function testTaskSuccessorDefaultProperties()
    {
        list($task1, $task2) = MockTwoRelatedTasks();
        /*@var $task1 Task */
        /*@var $task2 Task */

        foreach ($task1->selectSuccessors() as $successor) {
            /*@var $successor PredecessorLink */
    
            $this->assertEquals($successor->Type, (string) PredecessorLink::FS);
            $this->assertEquals($task2, $successor->task);
        }
    }
    
    
    
    /**
     * @expectedException \Ovidentia\LibProject\MandatoryFieldException
     */
    public function testTaskScheduledConstraintFailed()
    {
        $storage = Storage();
        $task = $storage->TaskSet()->newRecord();
        $task->Name = 'New task';
        $task->project = bab_uuid();
        $task->Start = '2014-01-01 00:00:00';
        $task->Finish = '2014-01-02 00:00:00';
        $task->ConstraintType = Task::START_ON;
        $task->save();
        $task->isConstraintSatisfied(Task::SCHEDULED);
    }
    
    
    /**
     * 
     */
    public function testTaskScheduledConstraintSuccess()
    {
        $storage = Storage();
        $task = $storage->TaskSet()->newRecord();
        $task->Name = 'New task';
        $task->project = bab_uuid();
        $task->Start = '2014-01-01 00:00:00';
        $task->Finish = '2014-01-02 00:00:00';
        $task->save();
        
        $task->ConstraintType = Task::START_ON;
        $task->ConstraintDate = '2014-01-01 00:00:00';
        $this->assertTrue($task->isConstraintSatisfied(Task::SCHEDULED));
        
        $task->ConstraintType = Task::START_NO_EARLIER_THAN;
        $task->ConstraintDate = '2013-12-25 23:00:00';
        $this->assertTrue($task->isConstraintSatisfied(Task::SCHEDULED));
        
        $task->ConstraintType = Task::START_NO_LATER_THAN;
        $task->ConstraintDate = '2014-01-01 23:00:00';
        $this->assertTrue($task->isConstraintSatisfied(Task::SCHEDULED));
        
        $task->ConstraintType = Task::FINISH_ON;
        $task->ConstraintDate = '2014-01-02 00:00:00';
        $this->assertTrue($task->isConstraintSatisfied(Task::SCHEDULED));
        
        $task->ConstraintType = Task::FINISH_NO_EARLIER_THAN;
        $task->ConstraintDate = '2014-01-01 23:00:00';
        $this->assertTrue($task->isConstraintSatisfied(Task::SCHEDULED));
        
        $task->ConstraintType = Task::FINISH_NO_LATER_THAN;
        $task->ConstraintDate = '2014-01-02 23:00:00';
        $this->assertTrue($task->isConstraintSatisfied(Task::SCHEDULED));
    }
}
