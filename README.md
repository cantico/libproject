# LibProject #

[![Build Status](https://drone.io/bitbucket.org/cantico/libproject/status.png)](https://drone.io/bitbucket.org/cantico/libproject/latest)
[![Scrutinizer Code Quality](https://scrutinizer-ci.com/b/cantico/libproject/badges/quality-score.png?b=master)](https://scrutinizer-ci.com/b/cantico/libproject/?branch=master)
[![Code Coverage](https://scrutinizer-ci.com/b/cantico/libproject/badges/coverage.png?b=master)](https://scrutinizer-ci.com/b/cantico/libproject/?branch=master)

## objets ORM ##

### Project ###

contient les champs d'un projet tel que décrits dans le format MSPDI
le champ uuid a été ajouté en plus pour stocker un idenfiant unique pour le projet

### Task ###

### Resource ###

contient les champs d'une resource tel que décrits dans le format MSPDI
le champ user a été ajouté en plus pour faire le lien avec un utilisateur ovidentia

### Assignment ###

Liens entre les ressources et les taches

### Calendar ###

Définition des heures travaillées et des congés