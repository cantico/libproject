<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
namespace Ovidentia\LibProject;

class Record extends \ORM_MySqlRecord
{

    /**
     * Update properties by XML node
     * @param \DOMNode $node
     */
    public function updateByXml(\DOMNode $node)
    {
        // index node as an array
        $arr = array();
        $loopNode = $node->firstChild;
        /* @var $loopNode \DOMNode */
        do {
            if ($loopNode && $loopNode->hasChildNodes()
               && $loopNode->firstChild->nodeType === XML_TEXT_NODE
               && !hasElementNodes($loopNode)) {
                $arr[$loopNode->nodeName] = $loopNode->firstChild->nodeValue;
            }

        } while ($loopNode = $loopNode->nextSibling);

        $set = $this->getParentSet();
        foreach ($set->getFields() as $field) {
            $name = $field->getName();

            if (isset($arr[$name])) {

                switch (true) {
                    case $field instanceof \ORM_DateTimeField:
                        
                        if ('1970-01-01T00:00:00' === $arr[$name]) {
                            // bug openProj!
                            $this->$name = '0000-00-00 00:00:00';
                            break;
                        }
                        
                        $this->$name = date('Y-m-d H:i:s', strtotime($arr[$name]));  // ISO8601
                        break;
                    default:
                        $this->$name = bab_getStringAccordingToDataBase($arr[$name], 'UTF-8');
                        break;
                }
            }

        }
    }

    /**
     * Receive the parent record
     * use this method to update links in record
     */
    public function updateParentByXml(Record $parent)
    {

    }

    /**
     * Append record to XML under the $node
     * @param \DOMDocument $xml
     * @param \DOMElement $node
     */
    protected function appendXml(\DOMDocument $xml, \DOMElement $node)
    {
        // default action
        $this->appendXmlFields($xml, $node);
    }

    /**
     * Append fields to a DOM node
     * @param    \DOMDocument    $xml
     * @param    \DOMElement    $node
     */
    protected function appendXmlFields(\DOMDocument $xml, \DOMElement $node)
    {
        $set = $this->getParentSet();
        foreach ($set->getFields() as $field) {
            $name = $field->getName();

            if (!ctype_upper(substr($name, 0, 1))) {
                // Ignore fields without first letter uppercase
                // they are internal fields for ORM
                continue;
            }

            $value = $this->getXmlFieldValue($field, $name);

            if (isset($value)) {
                $node->appendChild($xml->createElement($name, $value));
            }
        }
    }

    /**
     *
     * @param \ORM_Field $field
     * @param string $name
     * @return string | NULL
     */
    protected function getXmlFieldValue(\ORM_Field $field, $name)
    {
        $value = null; // Do not set null value

        switch (true) {
            case $field instanceof \ORM_DateTimeField:
                if ('0000-00-00 00:00:00' !== $this->$name) {
                    $value = date('c', bab_mktime($this->$name));  // ISO8601
                }
                break;
            case $this->$name instanceof Record:
                $record = $this->$name;
                $pkey = $record->getParentSet()->getPrimaryKey();
                $value = $record->$pkey;
                break;
            case $field instanceof \ORM_StringField:
            case $field instanceof \ORM_TextField:
                if ('' !== $this->$name) {
                    $value = $this->$name;
                }
                break;
            default:
                $value = $this->$name;
                break;
        }

        return $value;
    }

    /**
     * Append records from the iterator to the XML under the $node
     * 
     * @param   \DOMDocument    $xml
     * @param   \DOMElement     $node
     * @param   \ORM_Iterator   $res
     * 
     */
    protected function appendXmlIterator(\DOMDocument $xml, \DOMElement $node, \ORM_Iterator $res)
    {
        foreach ($res as $record) {
            $name = get_class($record);
            $name = join('', array_slice(explode('\\', $name), -1));
            
            $RecordNode = $xml->createElement($name);
            $node->appendChild($RecordNode);

            /*@var $record Record */
            $record->appendXml($xml, $RecordNode);
        }
    }
    
    /**
     * Specific method used by the import program to save the record
     * special processing can be done in this method for initialization
     * 
     * @return bool
     */
    public function importSave()
    {
        return parent::save();
    }
}
