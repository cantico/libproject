<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
namespace Ovidentia\LibProject;


/**
 * Import project from an XML file in mspdi format
 */
class MspdiImport
{
    /**
     * 
     * @var \DOMDocument
     */
    private $xml;
    
    /**
     * 
     * @var \Ovidentia\LibProject\Project
     */
    private $projectRecord;
    
    /**
     * 
     * @param \DOMDocument $xml
     * @param Project $projectRecord
     */
    public function __construct(\DOMDocument $xml, Project $projectRecord)
    {
        $this->xml = $xml;
        $this->projectRecord = $projectRecord;
        
        $this->xml->normalize();
    }

    
    /**
     * Process the import into database
     */
    public function importProject()
    {
        //if (!$this->xml->schemaValidate(dirname(__FILE__).'/MSPDI.XSD'))
        //{
        //    throw new Exception('Failed to validate the xml file');
        //}
        
        $Project = $this->xml->firstChild;
        
        $this->projectRecord->updateByXml($Project);
        $this->projectRecord->importSave();
        
        
        $this->processSubTreeNodes($Project, $this->projectRecord);
    }
    
    /**
     * Process childnodes
     * 
     * @param    \DomNode             $node        the parent node
     * @param    \Ovidentia\LibProject\Record   $record      the record associated to the parent node
     */
    protected function processSubTreeNodes(\DomNode $node, Record $record)
    {
        $storage = Storage();
        $loop = $node->firstChild;
        
        if (!$loop) {
            return array();
        }
        
        do {
            if ($loop->hasChildNodes()) {
                // test if a corresponding select method exists
                
                $methodName = 'select'.$loop->nodeName;
                if (method_exists($record, $methodName)) {
                    // remove the "s" to get set name
                    $setName = substr($loop->nodeName, 0, -1).'Set';
                    
                    if (method_exists($storage, $setName)) {
                        $set = $storage->$setName();
                        $this->importSet($set, $loop, $record);
                    }
                }
            }
        } while ($loop = $loop->nextSibling);
    }
    
    
    
    /**
     *
     * @param    \Ovidentia\LibProject\RecordSet    $set            ex: TaskSet
     * @param    \DomNode                 $node           ex: <Tasks>
     * @param    \Ovidentia\LibProject\Record       $parent         ex: Project
     */
    protected function importSet(RecordSet $set, \DomNode $node, Record $parent)
    {

        foreach ($node->childNodes as $Item) {
            
            if (!hasElementNodes($Item)) {
                continue;
            }
            
            $record = $set->newRecord();
            /*@var $record Record */
            
            $record->updateByXml($Item);
            $record->updateParentByXml($parent);
            $record->importSave();
            
            //echo substr(get_class($set), 11, -3)."\n";
            
            $this->processSubTreeNodes($Item, $record);
        }
    }
}
