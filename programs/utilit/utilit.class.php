<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
namespace Ovidentia\LibProject;


class Utilit
{

    /**
     * formating the interval like ISO 8601 (PnYnMnDTnHnMnS)
     *
     * @return string
     */
    public function getIntervalSpec(\DateInterval $interval)
    {
        $return = 'P';
    
        if ($interval->y) {
            $return .= $interval->y . 'Y';
        }
    
        if ($interval->m) {
            $return .= $interval->m . 'M';
        }
    
        if ($interval->d) {
            $return .= $interval->d . 'D';
        }
    
        if ($interval->h || $interval->i || $interval->s) {
            $return .= 'T';
    
            if ($interval->h) {
                $return .= $interval->h . 'H';
            }
    
            if ($interval->i) {
                $return .= $interval->i . 'M';
            }
    
            if ($interval->s) {
                $return .= $interval->s . 'S';
            }
        }
    
        return $return;
    }
}
